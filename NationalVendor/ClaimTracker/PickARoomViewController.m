//
//  PickARoomViewController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/29/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "PickARoomViewController.h"
#import "Inventory.h"
#import "CTNavigationController.h"

@interface PickARoomViewController ()

@property NSArray *rooms;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
- (IBAction)cmdCancel:(UIButton *)sender;
- (IBAction)cmdSave:(UIButton *)sender;

@end

@implementation PickARoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    //CTNavigationController *parent = (CTNavigationController *)self.parentViewController;
    //self.claim = parent.selectedClaim;
    self.rooms = [self.claim getRoomList];

    self.picker.dataSource = self;
    self.picker.delegate = self;
    
    for (int i = 0; i < self.rooms.count; i++)
    {
        if ([self.rooms[i] isEqualToString:self.textField.text])
        {
            [self.picker selectRow:i inComponent:0 animated:true];
            break;
        }
    }
}

- (IBAction)cmdCancel:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)cmdSave:(UIBarButtonItem *)sender {
    NSInteger row = [self.picker selectedRowInComponent:0];
    if (row)
    {
        self.textField.text = [[self.picker delegate] pickerView:self.picker titleForRow:row forComponent:0];
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.rooms.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return self.rooms[row];
}

@end
