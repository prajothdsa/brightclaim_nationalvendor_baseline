//
//  ClaimNoteController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/8/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "ClaimNoteController.h"
#import "CTNavigationController.h"

@interface ClaimNoteController ()

@property (weak, nonatomic) IBOutlet UILabel *lblEntered;
@property (weak, nonatomic) IBOutlet UILabel *lblNote;

@end

@implementation ClaimNoteController

- (void)viewDidLoad {
    [super viewDidLoad];
    CTNavigationController * source = (CTNavigationController *)self.parentViewController;
    self.claimNote = source.selectedClaimNote;
    if (self.claimNote){
        self.lblNote.text = self.claimNote.note;

        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MM/dd/yyyy"];
        self.lblEntered.text = [NSString stringWithFormat:@"Entered %@ By %@ for status %@", [df stringFromDate:self.claimNote.date], self.claimNote.repName, self.claimNote.status];
    }
}

@end
