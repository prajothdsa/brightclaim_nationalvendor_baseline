//
//  MyInformationController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 6/2/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import <Photos/Photos.h>
#import "MyInformationController.h"
#import "UserData.h"
#import "LoginController.h"
#import "AppDelegate.h"
#import "DataSource.h"

@interface MyInformationController ()

@property (weak, nonatomic) IBOutlet UILabel *lblLogin;
@property (weak, nonatomic) IBOutlet UILabel *lblRepName;
@property (weak, nonatomic) IBOutlet UIButton *lblRepPhone;
@property (weak, nonatomic) IBOutlet UIButton *lblRepEMail;
- (IBAction)cmdEMail:(UIButton *)sender;
- (IBAction)cmdPhone:(UIButton *)sender;
- (IBAction)cmdLogout:(UIBarButtonItem *)sender;

@end

@implementation MyInformationController

- (void)doRefresh{
    [[UserData data] refreshWithCompletion:^(void){
        [self refreshData];
        if (self.refreshControl) [self.refreshControl endRefreshing];
    }];
}

- (void)refreshData
{
    @try {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL testMode = [defaults boolForKey:@"testMode"];

        if (testMode)
            self.lblLogin.text = [NSString stringWithFormat:@"Logged in as %@: Test Mode", [UserData data].fullName];
        else
            self.lblLogin.text = [NSString stringWithFormat:@"Logged in as %@", [UserData data].fullName];
        self.lblRepName.text = [NSString stringWithFormat:@"Name: %@", [UserData data].repName];
        [self.lblRepPhone setTitle:[NSString stringWithFormat:@"Phone: %@", [UserData data].repPhone] forState:UIControlStateNormal];
        [self.lblRepEMail setTitle:[NSString stringWithFormat:@"E-Mail: %@", [UserData data].repEMail] forState:UIControlStateNormal];
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (void)viewDidLoad {
    @try {
        [super viewDidLoad];

        DataSource * d = [DataSource new];
        if (d.isConnected)
        {
            [[UserData data] refreshConditions];
            [self refreshData];
            [self doRefresh];
        }
        else
        {
            [[UserData data] loadConditions];
            [self refreshData];
        }
        
        self.refreshControl = [[UIRefreshControl alloc] init];
        self.refreshControl.backgroundColor = [UIColor blueColor];
        self.refreshControl.tintColor = [UIColor whiteColor];
        [self.refreshControl addTarget:self action:@selector(doRefresh) forControlEvents:UIControlEventValueChanged];
        
        UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Launch 750 x 1334.png"]];
        img.contentMode = UIViewContentModeScaleAspectFill;
        
        self.tableView.backgroundView = img;

        // Uncomment the following line to preserve selection between presentations.
        // self.clearsSelectionOnViewWillAppear = NO;
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cmdEMail:(UIButton *)sender {
    @try{
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            mailer.mailComposeDelegate = self;
            mailer.modalPresentationStyle = UIModalPresentationPageSheet;
            [mailer setToRecipients:[NSArray arrayWithObjects:[UserData data].repEMail, nil]];
            [mailer setMessageBody:@"" isHTML:NO];
            [self presentViewController:mailer animated:YES completion:NULL];
        }
    }
    @catch (NSException *e){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:e.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Canceled");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Failed");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Savd");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Sent");
            break;
        default:
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)cmdPhone:(UIButton *)sender {
    @try{
        NSString *p = [UserData data].repPhone;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", p]]];
    }
    @catch (NSException *e){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:e.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)cmdLogout:(UIBarButtonItem *)sender {
    @try {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm Logout" message:@"Are you sure you wish to logout?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
        {
            // No action
        }];
        
        UIAlertAction *logoutAction = [UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
        {
            [UserData data].isAuthenticated = false;
            [[UserData data] saveClaims];
            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            UIViewController *rootController = [[UIStoryboard storyboardWithName:@"Login" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"LoginScreen"];
            UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:rootController];
            appDelegate.window.rootViewController = navigation;
        }];
        
        [alert addAction:cancelAction];
        [alert addAction:logoutAction];
        [self presentViewController:alert animated:TRUE completion:nil];
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

#pragma mark - Table view data source

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
