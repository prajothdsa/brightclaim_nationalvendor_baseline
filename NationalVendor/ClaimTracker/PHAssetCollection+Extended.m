//
//  PHAssetCollection+Extended.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 6/23/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "PHAssetCollection+Extended.h"
#import "Constants.h"

@implementation PHAssetCollection (Extended)

+ (void)createCTAlbumForClaim:(NSString *)claim WithCompletion:(void(^)())handler {
    @try {
        __block PHObjectPlaceholder *assetCollectionPlaceholder;
        
        PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
        fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title == %@", claim];
        
        PHFetchResult *collection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:fetchOptions];
        if (collection.count == 0) {
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                if (status == PHAuthorizationStatusAuthorized){
                    [[PHPhotoLibrary sharedPhotoLibrary]
                     performChanges:^(){
                         PHAssetCollectionChangeRequest *createAlbumRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:[NSString stringWithFormat:@"%@", claim]];
                         assetCollectionPlaceholder = createAlbumRequest.placeholderForCreatedAssetCollection;
                     }
                     completionHandler:^(BOOL success, NSError *error) {
                         if (!success) {
                             dispatch_async (dispatch_get_main_queue(), ^{
//                                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@ Photo Album", claim] message:[NSString stringWithFormat:@"%@", error.description] delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//                                 [alert show];
                                 UIWindow *keyWindow = [[UIApplication sharedApplication]keyWindow];
                                 UIViewController *mainController = [keyWindow rootViewController];
                                 UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@ Photo Album", claim] message:[NSString stringWithFormat:@"%@", error.description] preferredStyle:UIAlertControllerStyleAlert];
                                 UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                                     
                                 }];
                                 [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
                                 [alert addAction:cancelAction];
                                 [mainController presentViewController:alert animated:TRUE completion:nil];
                             });
                         }
                         if (handler){
                             void (^_completionHandler)();
                             _completionHandler = [handler copy];
                             _completionHandler();
                         }
                     }];
                }
                else {
                    dispatch_async (dispatch_get_main_queue(), ^{
//                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Not authorized to create Claim photo albums.  Please give ClaimTracker access in Settings." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//                        [alert show];
                        UIWindow *keyWindow = [[UIApplication sharedApplication]keyWindow];
                        UIViewController *mainController = [keyWindow rootViewController];
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:kError message:@"Not authorized to create Claim photo albums.  Please give ClaimTracker access in Settings." preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                            
                        }];
                        [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
                        [alert addAction:cancelAction];
                        [mainController presentViewController:alert animated:TRUE completion:nil];
                        //                alert.view.tintColor = kBCOrangeColor;
                    });
                }
            }];
        }
    } @catch (NSException *exception) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//        [alert show];
        UIWindow *keyWindow = [[UIApplication sharedApplication]keyWindow];
        UIViewController *mainController = [keyWindow rootViewController];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:kError message:exception.reason preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            
        }];
        [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
        [alert addAction:cancelAction];
        [mainController presentViewController:alert animated:TRUE completion:nil];    } @finally {
    }
}

+ (PHAssetCollection *)getCTAlbumForClaim:(NSString *)claim {
    @try {
        PHAssetCollection *assetCollection = nil;
        
        PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
        fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title == %@", claim];
        
        PHFetchResult *collection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:fetchOptions];
        if (collection.firstObject) {
            assetCollection = (PHAssetCollection *)collection.firstObject;
        }
        else {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"Could not find the %@ Photo album", claim] delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//            [alert show];
            UIWindow *keyWindow = [[UIApplication sharedApplication]keyWindow];
            UIViewController *mainController = [keyWindow rootViewController];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:kError message:[NSString stringWithFormat:@"Could not find the %@ Photo album", claim] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                
            }];
            [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
            [alert addAction:cancelAction];
            [mainController presentViewController:alert animated:TRUE completion:nil];
        }
        return assetCollection;
    } @catch (NSException *exception) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//        [alert show];
        UIWindow *keyWindow = [[UIApplication sharedApplication]keyWindow];
        UIViewController *mainController = [keyWindow rootViewController];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:kError message:exception.reason preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            
        }];
        [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
        [alert addAction:cancelAction];
        [mainController presentViewController:alert animated:TRUE completion:nil];    } @finally {
    }
}

@end
