//
//  ConditionList.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 8/2/16.
//  Copyright © 2016 com.nationalvendor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Condition.h"

@interface ConditionList : NSObject

- (NSUInteger)count;
- (Condition *)getConditionByIndex:(NSUInteger) index;
- (Condition *)getConditionByID:(NSUInteger) id;
- (Condition *)getConditionByName:(NSString*) name;
- (void)addCondition:(Condition *) condition;
- (void)removeCondition:(Condition *)condition;
- (void)clear;

- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)decoder;

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id *)buffer count:(NSUInteger)len;

@end
