//
//  Photo.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/22/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "Photo.h"

@implementation Photo

- (void) encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.claimID] forKey:@"claimID"];
    [encoder encodeObject:self.claimName forKey:@"claimName"];
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.index] forKey:@"inventoryIndex"];
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.inventoryID] forKey:@"inventoryID"];
    [encoder encodeObject:self.inventoryOrdinal forKey:@"inventoryOrdinal"];
    [encoder encodeObject:self.image forKey:@"image"];
    [encoder encodeObject:self.imageName forKey:@"imageName"];
    [encoder encodeObject:self.localURL forKey:@"localURL"];
    
    [encoder encodeBool:self.added forKey:@"added"];
}

- (id) initWithCoder:(NSCoder *)decoder{
    self = [super init];
    if (self){
        self.claimID = [[decoder decodeObjectForKey:@"claimID"] unsignedIntegerValue];
        self.claimName = [decoder decodeObjectForKey:@"claimName"];
        self.index = [[decoder decodeObjectForKey:@"index"] unsignedIntegerValue];
        self.inventoryID = [[decoder decodeObjectForKey:@"inventoryID"] unsignedIntegerValue];
        self.inventoryOrdinal = [decoder decodeObjectForKey:@"inventoryOrdinal"];
        self.image = [decoder decodeObjectForKey:@"image"];
        self.imageName = [decoder decodeObjectForKey:@"imageName"];
        self.localURL = [decoder decodeObjectForKey:@"localURL"];
        
        self.added = [decoder decodeBoolForKey:@"added"];
    }
    return self;
}

@end
