//
//  CollectionViewCell.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/26/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "CTCollectionViewCell.h"

@implementation CTCollectionViewCell

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    self.photo.imageName = textField.text;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)cmdDelete:(UIButton *)sender {
    @try {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm Delete" message:@"Are you sure you want to delete this photo?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL];
        
        UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self.inventory.photoList removePhoto:self.photo];
            [self.collectionView reloadData];
        }];
        
        [alert addAction:cancelAction];
        [alert addAction:deleteAction];
        [self.presenterViewController presentViewController:alert animated:TRUE completion:nil];
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}
@end
