//
//  Inventory.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/18/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoList.h"

@interface Inventory : NSObject

@property NSUInteger claimID;
@property NSString *claimName;
@property NSUInteger index;
@property NSUInteger inventoryID;
@property NSDecimalNumber *ordinal;
@property NSUInteger quantity;
@property NSString *originalDescription;
@property NSString *room;
@property NSUInteger ageY;
@property NSUInteger ageM;
@property NSUInteger conditionID;
@property NSString *purchaseLocation;
@property NSDecimalNumber *purchasePrice;
@property PhotoList *photoList;

@property BOOL edited;
@property BOOL added;

- (void) encodeWithCoder:(NSCoder *)encoder;
- (id) initWithCoder:(NSCoder *)decoder;

@end
