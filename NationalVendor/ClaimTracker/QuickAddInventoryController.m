//
//  QuickAddInventoryController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 6/24/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "QuickAddInventoryController.h"
#import "CTNavigationController.h"
#import "CTCollectionView.h"
#import "CTCollectionViewCell.h"
#import "UserData.h"
#import "PickARoomViewController.h"
#import "ClaimInformationController.h"
#import "CTTabBarController.h"
#import "PHAsset+Extended.h"
#import "PHAssetCollection+Extended.h"
#import "PhotoViewController.h"
#import "Photo.h"

@interface QuickAddInventoryController ()

@property Inventory *inventory;
@property Claim *claim;
@property (weak, nonatomic) IBOutlet CTCollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITextField *txtQuantity;
@property (weak, nonatomic) IBOutlet UITextField *txtRoom;
@property (weak, nonatomic) IBOutlet UITextField *txtDescription;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cmdDone;
- (IBAction)cmdAddAnother:(UIButton *)sender;
- (IBAction)cmdPickARoom:(UIButton *)sender;
- (IBAction)cmdPhotoLibrary:(UIButton *)sender;
@property (nonatomic) UIImagePickerController *imagePickerController;
@end

@implementation QuickAddInventoryController

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.inventory.photoList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        CTCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCell" forIndexPath:indexPath];
        Photo *p = [self.inventory.photoList getPhotoByIndex:indexPath.row];
        
        if (p.localURL) {
            PHAsset *asset = [PHAsset fetchAssetWithALAssetURL:p.localURL];
            if (asset){
                PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
                options.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
                options.synchronous = YES;
                options.networkAccessAllowed = NO;
                PHImageManager *manager = [PHImageManager defaultManager];
                [manager requestImageForAsset:asset targetSize:CGSizeMake(145,138) contentMode:PHImageContentModeDefault options:options resultHandler:^(UIImage *resultImage, NSDictionary *info)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         cell.imageView.image = resultImage;
                     });
                 }];
            }
        }
        
        cell.imageTitle.delegate = cell;
        cell.photo = p;
        cell.inventory = self.inventory;
        cell.imageTitle.text = p.imageName;
        return cell;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

-
(void)viewDidLoad {
    @try {
        [super viewDidLoad];
        
        CTNavigationController *source = (CTNavigationController *)self.parentViewController;
        self.claim = source.selectedClaim;
        self.inventory = [self addInventory];

        self.collectionView.claim = self.claim;
        self.collectionView.inventory = self.inventory;
        self.collectionView.presenterViewController = self.navigationController;
        self.collectionView.delegate = self.collectionView;
        self.collectionView.dataSource = self.collectionView;

        self.txtQuantity.delegate = self;
        self.txtRoom.delegate = self;
        self.txtDescription.delegate = self;
        
        // Uncomment the following line to preserve selection between presentations.
        // self.clearsSelectionOnViewWillAppear = NO;
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (Inventory *)addInventory{
    @try {
        Inventory *inventory = [[Inventory alloc] init];
        inventory.claimID = self.claim.claimID;
        inventory.claimName = self.claim.claimName;
        inventory.quantity = 1;
        inventory.conditionID = 1; // Average
        inventory.edited = true;
        inventory.originalDescription = @"";
        if (self.claim.inventoryList.count > 0)
        {
            Inventory * lastInv = [self.claim.inventoryList getInventoryByIndex:self.claim.inventoryList.count - 1];
            inventory.ordinal = [lastInv.ordinal decimalNumberByAdding:[NSDecimalNumber decimalNumberWithString:@"1"]];
            if ([self.txtRoom.text isEqualToString:@""]){
                self.txtRoom.text = inventory.room = lastInv.room;
            }
            else
                inventory.room = self.txtRoom.text;
            inventory.index = lastInv.index + 1;
        }
        else
        {
            inventory.ordinal = [NSDecimalNumber decimalNumberWithString:@"1"];
            inventory.index = 0;
        }
        [self.claim.inventoryList addInventory:inventory];
        
        self.txtQuantity.text = [NSString stringWithFormat:@"%lu", (unsigned long)inventory.quantity];
        self.txtDescription.text = inventory.originalDescription;
        
        return inventory;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (void)saveInventory{
    @try {
        self.inventory.quantity = [self.txtQuantity.text integerValue];
        self.inventory.room = self.txtRoom.text;
        self.inventory.originalDescription = self.txtDescription.text;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (IBAction)cmdAddAnother:(UIButton *)sender {
    @try {
        [self saveInventory];
        if (self.inventory.originalDescription.length == 0){
            if (self.inventory.photoList.count == 0)
                [self.claim.inventoryList removeInventory:self.inventory];
            else
                self.inventory.originalDescription = @"NULL";
        }
        self.inventory = [self addInventory];
        self.collectionView.inventory = self.inventory;
        [self.collectionView reloadData];
        [self flashScreen];
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (IBAction)cmdPickARoom:(UIButton *)sender {
    @try {
        if ([self.claim getRoomList].count == 0){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"There are no rooms defined.  Please type the room into the text box" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [alert show];
        }
        else {
            PickARoomViewController *pickARoomViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"PickARoomViewController"];
            
            pickARoomViewController.claim = self.claim;
            pickARoomViewController.textField = self.txtRoom;
            [self presentViewController:pickARoomViewController animated:YES completion:NULL];
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (IBAction)cmdPhotoLibrary:(UIButton *)sender{
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if (sender == self.cmdDone)
        return [self validateData];
    else
        return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    @try {
        if (sender == self.cmdDone)
        {
            [self saveInventory];
            if (self.inventory.originalDescription.length == 0){
                if (self.inventory.photoList.count == 0)
                    [self.claim.inventoryList removeInventory:self.inventory];
                else
                    self.inventory.originalDescription = @"NULL";
            }
            [[UserData data] saveClaims];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadInventoryList" object:self];
            ClaimInformationController *claimInfo = (ClaimInformationController *)segue.destinationViewController;
            CTTabBarController *tab = (CTTabBarController *)claimInfo.parentViewController.parentViewController;
            [tab setSelectedIndex:2];
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (BOOL)validateData{
    @try {
        if (self.txtDescription.text.length > 0)
        {
            if (self.txtQuantity.text.length == 0){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter a quantity" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                [alert show];
                return NO;
            }
            
            if ([self.txtQuantity.text integerValue] <= 0){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter a quantity greater than zero" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                [alert show];
                return NO;
            }
        }
        return YES;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

-(void) flashScreen {
    UIWindow* wnd = [UIApplication sharedApplication].keyWindow;
    UIView* v = [[UIView alloc] initWithFrame: CGRectMake(0, 0, wnd.frame.size.width, wnd.frame.size.height)];
    [wnd addSubview: v];
    v.backgroundColor = [UIColor grayColor];
    [UIView beginAnimations: nil context: nil];
    [UIView setAnimationDuration: 0.25];
    v.alpha = 0.0f;
    [UIView commitAnimations];

    [[UserData data] saveClaims];
}

#pragma ImagePicker

- (IBAction)showImagePickerForCamera:(id)sender
{
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
}

- (IBAction)showImagePickerForPhotoPicker:(id)sender
{
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType {
    @try {
        [self.txtDescription resignFirstResponder];
        [self.txtQuantity resignFirstResponder];
        [self.txtRoom resignFirstResponder];

        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
        imagePickerController.sourceType = sourceType;
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = TRUE;
        
        if (sourceType == UIImagePickerControllerSourceTypeCamera)
        {
            AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if (status == AVAuthorizationStatusAuthorized) { // authorized
                dispatch_async(dispatch_get_main_queue(), ^{
                    imagePickerController.showsCameraControls = YES;
                    self.imagePickerController = imagePickerController;
                    [self.collectionView.presenterViewController presentViewController:self.imagePickerController animated:YES completion:NULL];
                });
            }
            else if((status == AVAuthorizationStatusDenied) || (status == AVAuthorizationStatusRestricted)){ // denied
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Not authorized to use the camera.  Please give ClaimTracker access in Settings." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                [alert show];
            }
            else if(status == AVAuthorizationStatusNotDetermined){ // not determined
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    if(granted){ // Access has been granted ..do something
                        dispatch_async(dispatch_get_main_queue(), ^{
                            imagePickerController.showsCameraControls = YES;
                            self.imagePickerController = imagePickerController;
                            [self.collectionView.presenterViewController presentViewController:self.imagePickerController animated:YES completion:NULL];
                        });
                    } else { // Access denied ..do something
                        dispatch_async (dispatch_get_main_queue(), ^{
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Not authorized to use the camera.  Please give ClaimTracker access in Settings." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                            [alert show];
                        });
                    }
                }];
            }
        }
        else{
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                if (status == PHAuthorizationStatusAuthorized){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.imagePickerController = imagePickerController;
                        [self.collectionView.presenterViewController presentViewController:self.imagePickerController animated:YES completion:NULL];
                    });
                }
                else {
                    dispatch_async (dispatch_get_main_queue(), ^{
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Not authorized to access photo albums.  Please give ClaimTracker access in Settings." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                        [alert show];
                    });
                }
            }];
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}


#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    @try {
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera){
            UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
            UIView *spinner = [[UserData data] makeSpinner];
            
            [PHAsset addImageToCTAlbum:image ForClaim:self.claim.claimName withCompletion:^{
                
                PHAssetCollection *ctAlbum = [PHAssetCollection getCTAlbumForClaim:self.claim.claimName];
                PHFetchResult *fetchResult = [PHAsset fetchAssetsInAssetCollection:ctAlbum options:nil];
                PHAsset *asset = fetchResult.lastObject;
                
                NSRange range = NSMakeRange(0, 36);
                NSString *url = [NSString stringWithFormat:@"assets-library://asset/asset.JPG?id=%@&ext=JPG",[asset.localIdentifier substringWithRange:range]];
                
                Photo *p = [[Photo alloc] init];
                p.imageName = [NSString stringWithFormat:@"Photo %@", self.inventory.ordinal];
                p.claimID = self.claim.claimID;
                p.claimName = self.claim.claimName;
                p.inventoryID = self.inventory.inventoryID;
                p.inventoryOrdinal = self.inventory.ordinal;
                p.added = TRUE;
                [self.inventory.photoList addPhoto:p];
                p.localURL = [NSURL URLWithString:url];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    @try {
                        if (self.imagePickerController)
                        {
                            [self.collectionView.presenterViewController dismissViewControllerAnimated:YES completion:NULL];
                        }
                        self.imagePickerController = nil;
                        [self.collectionView reloadData];
                    } @catch (NSException *exception) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                        [alert show];
                    } @finally {
                        [[UserData data] removeSpinner:spinner];
                    }
                });
            }];
        }
        else {
            NSURL *url = [info valueForKey:UIImagePickerControllerReferenceURL];
            
            Photo *p = [[Photo alloc] init];
            p.localURL = url;
            p.imageName = [NSString stringWithFormat:@"Photo %@", self.inventory.ordinal];
            p.claimID = self.claim.claimID;
            p.claimName = self.claim.claimName;
            p.inventoryID = self.inventory.inventoryID;
            p.inventoryOrdinal = self.inventory.ordinal;
            p.added = TRUE;
            [self.inventory.photoList addPhoto:p];
            [self.collectionView reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.imagePickerController)
                {
                    [self.collectionView.presenterViewController dismissViewControllerAnimated:YES completion:NULL];
                }
                self.imagePickerController = nil;
            });
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

#pragma mark - Table view data source
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}*/

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
