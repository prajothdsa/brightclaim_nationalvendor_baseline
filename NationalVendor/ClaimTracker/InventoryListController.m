//
//  InventoryListController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/18/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "InventoryListController.h"
#import "CTTabBarController.h"
#import "UserData.h"
#import "CTTabBarController.h"
#import "ClaimNoteController.h"
#import "Constants.h"

@interface InventoryListController() {
    NSInteger syncCount;
}

@end

@implementation InventoryListController
- (IBAction)mDoneButton:(UIBarButtonItem *)sender {
}
- (IBAction)goBack:(id)sender {
    //    [self.tabBarController dismissViewControllerAnimated:YES completion:nil];
    //    [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
//    [[self.navigationController topViewController]dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)claimNote:(id)sender {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:kClaimNotesStoryboard bundle: nil];
        ClaimNoteController *claimNotesVC = (ClaimNoteController*)[mainStoryboard instantiateViewControllerWithIdentifier:kClaimNotesListVC];
        [self.navigationController pushViewController:claimNotesVC animated:YES];
}


- (void)doRefresh{
    @try {
        syncCount = [self.claim.inventoryList needToSyncCount];

        if (syncCount > 0)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:[NSString stringWithFormat:@"You have %ld item(s) to be uploaded. You would lose these changes if you refresh your data.  Please sync first.", (long)syncCount] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                if (self.refreshControl) [self.refreshControl endRefreshing];
            }];
            
            /*UIAlertAction *refreshAction = [UIAlertAction actionWithTitle:@"Refresh" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self.claim refreshInventoryListWithActivityIndex:0 withCompletion:^(int i){
                    [self.tableView reloadData];
                    //[[UserData data] saveClaims];
                    if (self.refreshControl) [self.refreshControl endRefreshing];
                }];
            }];*/
            
            [alert addAction:cancelAction];
            //[alert addAction:refreshAction];
            [self presentViewController:alert animated:TRUE completion:nil];
        }
        else
        {
            [self.claim refreshInventoryListWithActivityIndex:0 withCompletion:^(int i){
                [self.tableView reloadData];
                //[[UserData data] saveClaims];
                if (self.refreshControl) [self.refreshControl endRefreshing];
            }];
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (void)viewDidLoad {

    @try {
        [super viewDidLoad];


        syncCount = [self.claim.inventoryList needToSyncCount];
        
        if (syncCount>0) {

            [self.tableView reloadData];
        }
        
        else {
            UIView *spinner = [[UserData data] makeSpinner];
            [self.claim refreshInventoryListWithActivityIndex:0 withCompletion:^(int i){
                [self.tableView reloadData];
                [[UserData data] removeSpinner:spinner];



            }];
        }
//        CTTabBarController *source = (CTTabBarController *)self.parentViewController.parentViewController;
//        self.claim = source.selectedClaim;

        self.refreshControl = [[UIRefreshControl alloc] init];
        self.refreshControl.backgroundColor = [UIColor blueColor];
        self.refreshControl.tintColor = [UIColor whiteColor];
        [self.refreshControl addTarget:self action:@selector(doRefresh) forControlEvents:UIControlEventValueChanged];
        
        self.tableView.scrollsToTop = YES;

        if (self.claim.inventoryList.count > 0){
            NSIndexPath* ipath = [NSIndexPath indexPathForRow: self.claim.inventoryList.count-1 inSection: 0];
            [self.tableView scrollToRowAtIndexPath:ipath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reloadInventoryList"
                                                      object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(checkReloadNotification:) name:@"reloadInventoryList" object:nil];
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
        
    }
    
}


-(void)checkReloadNotification:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@"reloadInventoryList"])
    {
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    @try {
        if ([segue.identifier isEqualToString:@"segueInventoryListToEditInventory"]){
            NSIndexPath *indexPath = (NSIndexPath *)sender; //[self.tableView indexPathForSelectedRow];
            CTTabBarController *destination = segue.destinationViewController;
            
            self.selectedInventory = [self.claim.inventoryList getInventoryByIndex:indexPath.row];
            destination.selectedClaim = self.claim;
            destination.selectedInventory = [self.claim.inventoryList getInventoryByIndex:indexPath.row];
            destination.editMode = true;
        }
        else if ([segue.identifier isEqualToString:@"segueInventoryListToAddInventory"])
        {
            Inventory *inventory = [[Inventory alloc] init];
            inventory.claimID = self.claim.claimID;
            inventory.claimName = self.claim.claimName;
            inventory.quantity = 1;
            inventory.edited = true;
            if (self.claim.inventoryList.count > 0)
            {
                Inventory * lastInv = [self.claim.inventoryList getInventoryByIndex:self.claim.inventoryList.count - 1];
                inventory.ordinal = [lastInv.ordinal decimalNumberByAdding:[NSDecimalNumber decimalNumberWithString:@"1"]];
                inventory.room = lastInv.room;
                inventory.conditionID = 1; // Average
                inventory.index = lastInv.index + 1;
            }
            else
            {
                inventory.ordinal = [NSDecimalNumber decimalNumberWithString:@"1"];
                inventory.conditionID = 1; // Average
                inventory.index = 0;
            }
            [self.claim.inventoryList addInventory:inventory];
            
            CTTabBarController *destination = segue.destinationViewController;
            destination.selectedClaim = self.claim;
            destination.editMode = false;
            destination.selectedInventory = inventory;
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (IBAction)unwindToInventoryList:(UIStoryboardSegue *)segue {
    [self.tableView reloadData];
}

#pragma TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    @try {
        // Return the number of sections.
        if ([self.claim.inventoryList count] == 0){
            UILabel *lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(0,0,self.view.bounds.size.width, self.view.bounds.size.height)];
            lblMessage.text = kNoDataMsg;
            lblMessage.textColor = [UIColor blackColor];
            lblMessage.numberOfLines = 0;
            lblMessage.textAlignment = NSTextAlignmentCenter;
            lblMessage.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
            [lblMessage sizeToFit];
            self.tableView.backgroundView = lblMessage;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 0;
        }
        else {
            self.tableView.backgroundView = nil;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            return 1;
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.claim.inventoryList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PrototypeCell" forIndexPath:indexPath];
        
        Inventory *inventory = [self.claim.inventoryList getInventoryByIndex:indexPath.row];
        
        UILabel *label;
        label = (UILabel *)[cell viewWithTag:1];
        label.text = [NSString stringWithFormat:@"#%@ Qty %lu: %@", inventory.ordinal.stringValue, (unsigned long)inventory.quantity, inventory.originalDescription];

        if (inventory.inventoryID == 0)
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        //[cell setEditing:YES animated:YES];
        //UILabel *labelQuantity;
        //labelQuantity = (UILabel *)[cell viewWithTag:2];
        //labelQuantity.text = [NSString stringWithFormat:@"Qty %ld", inventory.quantity];

        //UILabel *labelDescription;
        //labelDescription = (UILabel *)[cell viewWithTag:3];
        //labelDescription.text = [NSString stringWithFormat:@"%@", inventory.originalDescription];

        return cell;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        Inventory *inventory = [self.claim.inventoryList getInventoryByIndex:indexPath.row];

        //UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Edit" handler:
        //                                    ^(UITableViewRowAction *action, NSIndexPath *indexPath){
        //                                        [self performSegueWithIdentifier:@"segueInventoryListToEditInventory" sender:indexPath];
        //                                    }
        //                                    ];
        //editAction.backgroundColor = [UIColor blueColor];;
        
        UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete"  handler:
                                              ^(UITableViewRowAction *action, NSIndexPath *indexPath){
                                                  Inventory *inventory = [self.claim.inventoryList getInventoryByIndex:indexPath.row];
                                                  [self.claim.inventoryList removeInventory:inventory];
                                                  [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                                                  [tableView reloadData];
                                              }
                                              ];
        deleteAction.backgroundColor = kBCOrangeColor;

        if (inventory.inventoryID == 0)
            return @[deleteAction];
        else
            return nil;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    Inventory *inventory = [self.claim.inventoryList getInventoryByIndex:indexPath.row];
    return (inventory.inventoryID == 0);
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"segueInventoryListToEditInventory" sender:indexPath];
}

@end
