//
//  Photo.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/22/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Photo : NSObject

@property NSUInteger claimID;
@property NSUInteger inventoryID;
@property NSDecimalNumber *inventoryOrdinal;
@property NSString *claimName;
@property NSUInteger index;
@property UIImage *image;
@property NSString *imageName;
@property NSURL *localURL;

@property BOOL added;

- (void) encodeWithCoder:(NSCoder *)encoder;
- (id) initWithCoder:(NSCoder *)decoder;

@end
