//
//  ClaimList.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/7/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Claim.h"

@interface ClaimList : NSObject

@property BOOL edited;

- (NSUInteger)count;
- (Claim *)getClaimByID:(NSUInteger) claimID;
- (Claim *)getClaimByIndex:(NSUInteger) index;
- (void)addClaim:(Claim *) claim;
- (void)clear;

- (void) encodeWithCoder:(NSCoder *)encoder;
- (id) initWithCoder:(NSCoder *)decoder;

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id *)buffer count:(NSUInteger)len;

@end
