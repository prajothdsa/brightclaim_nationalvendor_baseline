//
//  DataSource.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/4/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <Photos/Photos.h>
#import "PHAsset+Extended.h"
#import "DataSource.h"
#import "UserData.h"
#import "SSKeyChain.h"
#import "Constants.h"

@interface DataSource()

@end

@implementation DataSource

NSString *baseURL;

- (id)init {
    self = [super init];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL testMode = [defaults boolForKey:@"testMode"];

    if (testMode)
        baseURL = @"https://rest1-stage.claimtracker.com/CT1RestData.svc";
    else
        baseURL = @"https://rest1.claimtracker.com/CT1RestData.svc";

    return self;
}

- (NSString *) base64EncodeString:(NSString *)str {
    NSData *nsdata = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    return base64Encoded;
}

- (NSString *) getPingURL{
    return [NSString stringWithFormat:@"%@/Ping", baseURL];
};

- (NSString *) getAuthenticateURL{
    return [NSString stringWithFormat:@"%@/Authenticate", baseURL];
};

- (NSString *) getUserDataURL{
    return [NSString stringWithFormat:@"%@/User2", baseURL];
};

- (NSString *) getConditionsURL{
    return [NSString stringWithFormat:@"%@/Conditions2", baseURL];
};

- (NSString *) getClaimsURL{
    return [NSString stringWithFormat:@"%@/Claims2", baseURL];
};

- (NSString *) getClaimURL:(Claim *)claim{
    return [NSString stringWithFormat:@"%@/Claim2/%lu", baseURL, (unsigned long)claim.claimID];
};

- (NSString *) getClaimNotesURLForClaimID:(NSUInteger)claimID{
    return [NSString stringWithFormat:@"%@/ClaimNotes2/%lu", baseURL, (unsigned long)claimID];
};

- (NSString *) getAddNoteURLForClaim:(NSUInteger)claimID{
    return [NSString stringWithFormat:@"%@/AddNote/%lu", baseURL, (unsigned long)claimID];
};

- (NSString *) getSentEmailURLForClaim:(Claim *)claim{
    return [NSString stringWithFormat:@"%@/SendLink/%lu", baseURL, (unsigned long)claim.claimID];
};

- (NSString *) getInventoryURLForClaim:(Claim *)claim{
    return [NSString stringWithFormat:@"%@/Inventory2/%lu", baseURL, (unsigned long)claim.claimID];
};

- (NSString *) getInventoryURL{
    return [NSString stringWithFormat:@"%@/Inventory2", baseURL];
};

/*- (NSString *) getPhotoURLForInventory:(Inventory *)inventory{
    return [NSString stringWithFormat:@"https://rest1.claimtracker.com/CT1RestData.svc/Photos/%@/%@/%lu",
            [UserData data].lastLogin,
            [SSKeychain passwordForService:KCService account:KCAccount],
            (unsigned long)inventory.inventoryID];
};*/

- (NSString *) getPhotoURL{
    return [NSString stringWithFormat:@"%@/Photo2", baseURL];
};

- (NSDate *)deserializeJsonDateString: (NSString *)jsonDateString{
    NSInteger offset = [[NSTimeZone defaultTimeZone] secondsFromGMT]; //get number of seconds to add or subtract according to the client default time zone
    
    NSInteger startPosition = [jsonDateString rangeOfString:@"("].location + 1; //start of the date value
    
    // WCF will send 13 digit-long value for the time interval since 1970 (millisecond precision)
    // whereas iOS works with 10 digit-long values (second precision), hence the divide by 1000
    NSTimeInterval unixTime = [[jsonDateString substringWithRange:NSMakeRange(startPosition, 13)] doubleValue] / 1000;
    
    return [[NSDate dateWithTimeIntervalSince1970:unixTime] dateByAddingTimeInterval:offset];
}

- (NSMutableURLRequest *) makeRequestForURL:(NSURL *) url {
    return [self makeRequestForURL:url andUser:[UserData data].lastLogin andPassword:[SSKeychain passwordForService:KCService account:KCAccount] andMethod:@"GET"];
}

- (NSMutableURLRequest *) makeRequestForURL:(NSURL *) url andMethod:(NSString *) method {
    return [self makeRequestForURL:url andUser:[UserData data].lastLogin andPassword:[SSKeychain passwordForService:KCService account:KCAccount] andMethod:method];
}

- (NSMutableURLRequest *) makeRequestForURL:(NSURL *) url andUser:(NSString *) userName andPassword:(NSString *) password andMethod:(NSString *) method {
    @try {
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        NSString *auth = [NSString stringWithFormat:@"%@:%@",userName, password];
        NSString *authHeader = [NSString stringWithFormat:@"Basic %@",[self base64EncodeString:auth]];
        //[request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        [request setValue:authHeader forHTTPHeaderField:@"CTAuthorization"];
        [request setHTTPMethod:method];
        return request;
        
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (bool)isConnected {
    @try {
        NSURL *url = [NSURL URLWithString:[self getPingURL]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response;
        NSError *connectionError;
        
        [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&connectionError];

         if (connectionError == nil)
         {
             return YES;
         }
         else
         {
             return NO;
         }
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
        
    } @finally {
    }
}

- (bool)ctAuthenticateUser:(NSString *)userName andPassword:(NSString *)password {
    @try {
        NSURL *url = [NSURL URLWithString:[self getAuthenticateURL]];
        NSMutableURLRequest *request = [self makeRequestForURL: url andUser:userName andPassword:password andMethod:@"GET"];
        //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        //NSString *auth = [NSString stringWithFormat:@"%@:%@",userName, password];
        //NSString *authHeader = [NSString stringWithFormat:@"Basic %@",[self base64EncodeString:auth]];
        //[request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        //[request setHTTPMethod:@"GET"];
        NSHTTPURLResponse *response;
        NSError *connectionError;

        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&connectionError];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                
        if ([response statusCode] != 200) { // OK
            [self showAlertControllerWithTitle:@"Connection Error" andMessage:@"Error connecting to the server" andBtnTitle:kClose];
            return NO;
        }
        else if (connectionError == nil) {
            if ([dict objectForKey:@"ErrorMessage"]){
                [self showAlertControllerWithTitle:@"Connection Error" andMessage:@"Either Username or Password is incorrect. Please retry" andBtnTitle:kClose];
                return NO;
            }
            else
                return YES;
        }
        else
        {
            [self showAlertControllerWithTitle:@"Connection Error" andMessage:@"Error connecting to the server" andBtnTitle:kClose];
            return NO;
        }
    } @catch (NSException *exception) {
         [self showAlertControllerWithTitle:@"Error" andMessage:@"Error connecting to the server" andBtnTitle:kClose];
    } @finally {
    }
}

- (void)ctGetUserDataWithCompletion:(void(^)())handler {
    @try {
        NSURL *url = [NSURL URLWithString:[self getUserDataURL]];
        //NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSMutableURLRequest *request = [self makeRequestForURL: url];

        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
             [UserData data].isAuthenticated = FALSE;
             if ((data.length > 0) && (connectionError == nil)) {
                 NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                 if (!dict)
                 {

                     [self showAlertControllerWithTitle:@"Error" andMessage:@"Error getting data" andBtnTitle:kClose];
                     
                 }
                 else if ([dict objectForKey:@"ErrorMessage"])
                 {

                     [self showAlertControllerWithTitle:@"Error" andMessage:[dict objectForKey:@"ErrorMessage"] andBtnTitle:kClose];
                 }
                 else
                 {
                     [UserData data].fullName = [dict valueForKey:@"FullName"];
                     [UserData data].repName = [dict valueForKey:@"RepName"];
                     [UserData data].repPhone = [dict valueForKey:@"RepPhone"];
                     [UserData data].repEMail = [dict valueForKey:@"RepEMail"];
                     [UserData data].isAuthenticated = TRUE;
                 }
             }
//             else if (connectionError != nil){
//
//                 [self showAlertControllerWithTitle:@"Connection Error" andMessage:[NSString stringWithFormat:@"%@", connectionError.localizedDescription] andBtnTitle:kClose];
//             }
             _completionHandler = [handler copy];
             _completionHandler();
         }];
    } @catch (NSException *exception) {

        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)ctGetClaimsWithCompletion:(void(^)())handler {
    @try {
        NSURL *url = [NSURL URLWithString:[self getClaimsURL]];
        //NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSMutableURLRequest *request = [self makeRequestForURL: url];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
             if ((data.length > 0) && (connectionError == nil)) {
                 NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                 if (([dict respondsToSelector:@selector(objectForKey:)]) && ([dict objectForKey:@"ErrorMessage"])){

                     [self showAlertControllerWithTitle:@"Error" andMessage:[dict objectForKey:@"ErrorMessage"] andBtnTitle:kClose];
                 } else {
                     NSArray *arr = (NSArray *)dict;
                     [[UserData data].claims clear];
                     [UserData data].claims = [[ClaimList alloc] init];
                     for (int i = 0; i < arr.count; i++){
                         Claim *claim = [[Claim alloc] init];
                         dict = [arr objectAtIndex:i];
                         claim.edited = FALSE;
                         claim.added = FALSE;
                         claim.index = i;
                         claim.claimID = [[dict objectForKey:@"SubClaimID"] unsignedIntegerValue];
                         claim.claimName = (NSString *)[dict objectForKey:@"ClaimName"];
                         claim.insuredFullAddress = (NSString *)[dict objectForKey:@"InsuredFullAddress"];
                         claim.insuredAddress1 = (NSString *)[dict objectForKey:@"InsuredAddress1"];
                         claim.insuredAddress2 = (NSString *)[dict objectForKey:@"InsuredAddress2"];
                         claim.insuredCity = (NSString *)[dict objectForKey:@"InsuredCity"];
                         claim.insuredState = (NSString *)[dict objectForKey:@"InsuredState"];
                         claim.insuredZip = (NSString *)[dict objectForKey:@"InsuredZip"];
                         claim.insuredEMail = (NSString *)[dict objectForKey:@"InsuredEMail"];
                         claim.insuredPhone = (NSString *)[dict objectForKey:@"InsuredPhone"];
                         claim.lossType = (NSString *)[dict objectForKey:@"LossType"];
                         claim.coverageType = (NSString *)[dict objectForKey:@"CoverageType"];
                         claim.deductible = [[NSDecimalNumber alloc] initWithDecimal: [[dict objectForKey:@"Deductible"] decimalValue]];
                         claim.overallLimit = [[NSDecimalNumber alloc] initWithDecimal: [[dict objectForKey:@"OverallLimit"] decimalValue]];
                         [[UserData data].claims addClaim:claim];
                     }
                 }
             }
//             else if (connectionError != nil){
//                 [self showAlertControllerWithTitle:@"Connection Error" andMessage:[NSString stringWithFormat:@"%@", connectionError.localizedDescription] andBtnTitle:kClose];
//             }
             _completionHandler = [handler copy];
             _completionHandler();
         }];
    } @catch (NSException *exception) {

        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)ctGetConditionsWithCompletion:(void(^)())handler {
    @try {
        NSURL *url = [NSURL URLWithString:[self getConditionsURL]];
        //NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSMutableURLRequest *request = [self makeRequestForURL: url];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
             if ((data.length > 0) && (connectionError == nil)) {
                 NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                 if (([dict respondsToSelector:@selector(objectForKey:)]) && ([dict objectForKey:@"ErrorMessage"])){

                     [self showAlertControllerWithTitle:@"Error" andMessage:[dict objectForKey:@"ErrorMessage"] andBtnTitle:kClose];
                 } else {
                     NSArray *arr = (NSArray *)dict;
                     [[UserData data].conditions clear];
                     [UserData data].conditions = [[ConditionList alloc] init];
                     for (int i = 0; i < arr.count; i++){
                         Condition *condition = [[Condition alloc] init];
                         dict = [arr objectAtIndex:i];
                         condition.added = FALSE;
                         condition.index = i;
                         condition.conditionID = [[dict objectForKey:@"ConditionID"] unsignedIntegerValue];
                         condition.conditionName = (NSString *)[dict objectForKey:@"ConditionName"];
                         [[UserData data].conditions addCondition:condition];
                     }
                 }
             }
             else if (connectionError != nil){

                 [self showAlertControllerWithTitle:@"Error" andMessage:kNetworkConnectionErrorMsg andBtnTitle:kClose];
             }
             _completionHandler = [handler copy];
             _completionHandler();
         }];
    } @catch (NSException *exception) {

        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)ctGetClaim:(Claim *)claim withCompletion:(void(^)())handler {
    @try {
        NSURL *url = [NSURL URLWithString:[self getClaimURL:claim]];
        //NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSMutableURLRequest *request = [self makeRequestForURL: url];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
             if ((data.length > 0) && (connectionError == nil)) {
                 NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                 if ([dict objectForKey:@"ErrorMessage"]){
                     [self showAlertControllerWithTitle:kError andMessage:[dict objectForKey:@"Error loading data. Please try again."] andBtnTitle:kClose];
                 } else {
                     claim.claimName = [dict valueForKey:@"ClaimName"];
                     claim.insuredFullAddress = [dict valueForKey:@"InsuredFullAddress"];
                     claim.insuredAddress1 = [dict valueForKey:@"InsuredAddress1"];
                     claim.insuredAddress2 = [dict valueForKey:@"InsuredAddress2"];
                     claim.insuredCity = [dict valueForKey:@"InsuredCity"];
                     claim.insuredState = [dict valueForKey:@"InsuredState"];
                     claim.insuredZip = [dict valueForKey:@"InsuredZip"];
                     claim.insuredEMail = [dict valueForKey:@"InsuredEMail"];
                     claim.insuredPhone = [dict valueForKey:@"InsuredPhone"];
                     claim.lossType = [dict valueForKey:@"LossType"];
                     claim.coverageType = [dict valueForKey:@"CoverageType"];
                     claim.deductible = [dict valueForKey:@"Deductible"];
                     claim.overallLimit = [dict valueForKey:@"OverallLimit"];
                 }
             }
             else if (connectionError != nil){
                 [self showAlertControllerWithTitle:kError andMessage:[NSString stringWithFormat:@"%@", connectionError.localizedDescription] andBtnTitle:kClose];
             }
             _completionHandler = [handler copy];
             _completionHandler();
         }];
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:kError andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)ctGetClaimNotesForClaim:(Claim *)claim withCompletion:(void(^)())handler {
    @try {
        NSURL *url = [NSURL URLWithString:[self getClaimNotesURLForClaimID:claim.claimID]];
        //NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSMutableURLRequest *request = [self makeRequestForURL: url];

        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
             if ((data.length > 0) && (connectionError == nil)) {
                 NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                 if (([dict respondsToSelector:@selector(objectForKey:)]) && ([dict objectForKey:@"ErrorMessage"])){
                     
                     [self showAlertControllerWithTitle:@"Error" andMessage:@"Error loading data. Please try again" andBtnTitle:kClose];
                     
                 } else {
                     NSArray *arr = (NSArray *)dict;
                     if (claim.claimNoteList){
                         [claim.claimNoteList clear];
                     } else {
                         claim.claimNoteList = [[ClaimNoteList alloc] init];
                     }
                     for (int i = 0; i < arr.count; i++){
                         ClaimNote *claimNote = [[ClaimNote alloc] init];
                         dict = [arr objectAtIndex:i];
                         claimNote.index = i;
                         claimNote.claimID = claim.claimID;
                         claimNote.claimNoteID = [[dict objectForKey:@"ClaimNoteID"] unsignedIntegerValue];
                         claimNote.status = (NSString *)[dict objectForKey:@"Status"];
                         claimNote.note = (NSString *)[dict objectForKey:@"Note"];
                         claimNote.repName = (NSString *)[dict objectForKey:@"RepName"];
                         claimNote.date = (NSDate *)[self deserializeJsonDateString:(NSString *)[dict objectForKey:@"Date"]];
                         [claim.claimNoteList addClaimNote:claimNote];
                     }
                     //[[UserData data] saveClaims];
                 }
             }
//             else if (connectionError != nil){
//                 //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kError message:kNetworkConnectionErrorMsg delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//                 //alert.tintColor = kBCOrangeColor;
//                 //[alert show];
//                 [self showAlertControllerWithTitle:@"Error" andMessage:kNetworkConnectionErrorMsg andBtnTitle:kClose];
//             }
             _completionHandler = [handler copy];
             _completionHandler();
         }];
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)ctGetInventoryForClaim:(Claim *)claim withActivityIndex:(int)i withCompletion:(void(^)(int))handler {
    @try {
        NSURL *url = [NSURL URLWithString:[self getInventoryURLForClaim:claim]];
        //NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSMutableURLRequest *request = [self makeRequestForURL: url];

        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
             if ((data.length > 0) && (connectionError == nil)) {
                 NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                 if (([dict respondsToSelector:@selector(objectForKey:)]) && ([dict objectForKey:@"ErrorMessage"])){
                     [self showAlertControllerWithTitle:kError andMessage:@"Error loading data. Try again." andBtnTitle:kOK];
                 } else {
                     NSArray *arr = (NSArray *)dict;
                     if (claim.inventoryList){
                         [claim.inventoryList clear];
                     } else {
                         claim.inventoryList = [[InventoryList alloc] init];
                     }
                     for (int i = 0; i < arr.count; i++){
                         Inventory *inventory = [[Inventory alloc] init];
                         dict = [arr objectAtIndex:i];
                         inventory.edited = FALSE;
                         inventory.index = i;
                         inventory.claimID = claim.claimID;
                         inventory.claimName = claim.claimName;
                         inventory.inventoryID = [[dict objectForKey:@"InventoryID"] unsignedIntegerValue];
                         inventory.ordinal = [[NSDecimalNumber alloc] initWithDecimal: [[dict objectForKey:@"Ordinal"] decimalValue]];
                         inventory.quantity = [[dict objectForKey:@"Quantity"] unsignedIntegerValue];
                         inventory.originalDescription = (NSString *)([[dict objectForKey:@"Description"] isKindOfClass:[NSNull class]] ? @"" : [dict objectForKey:@"Description"]);
                         inventory.room = (NSString *)([[dict objectForKey:@"Room"] isKindOfClass:[NSNull class]] ? @"" : [dict objectForKey:@"Room"]);
                         inventory.ageY = [[dict objectForKey:@"AgeY"] unsignedIntegerValue];
                         inventory.ageM = [[dict objectForKey:@"AgeM"] unsignedIntegerValue];
                         inventory.conditionID = [[dict objectForKey:@"ConditionID"] unsignedIntegerValue];
                         inventory.purchasePrice = [[NSDecimalNumber alloc] initWithDecimal: [[dict objectForKey:@"PurchasePrice"] decimalValue]];
                         inventory.purchaseLocation = (NSString *)([[dict objectForKey:@"PurchaseLocation"] isKindOfClass:[NSNull class]] ? @"" : [dict objectForKey:@"PurchaseLocation"]);
                         
                         if (inventory.photoList){
                             [inventory.photoList clear];
                         } else {
                             inventory.photoList = [[PhotoList alloc] init];
                         }
                         NSArray *photos = (NSArray *)[dict objectForKey:@"Photos"];
                         for (int p = 0; p < photos.count; p++){
                             Photo *photo = [[Photo alloc] init];
                             NSDictionary *dictPhoto = [photos objectAtIndex:p];
                             photo.index = p;
                             photo.added = NO;
                             photo.claimID = claim.claimID;
                             photo.claimName = claim.claimName;
                             photo.inventoryID = inventory.inventoryID;
                             photo.inventoryOrdinal = inventory.ordinal;
                             photo.imageName = (NSString *)([[dictPhoto objectForKey:@"Name"] isKindOfClass:[NSNull class]] ? @"" : [dictPhoto objectForKey:@"Name"]);
                             photo.localURL = [NSURL URLWithString:(NSString *)([[dictPhoto objectForKey:@"PhysicalLocation"] isKindOfClass:[NSNull class]] ? @"" : [dictPhoto objectForKey:@"PhysicalLocation"])];

                             [inventory.photoList addPhoto:photo];
                         }
                         
                         if (inventory.conditionID == 0) inventory.conditionID = 1;
                         [claim.inventoryList addInventory:inventory];
                     }
                 }
             }
             else if (connectionError != nil){
                 [self showAlertControllerWithTitle:@"Error" andMessage:kNetworkConnectionErrorMsg andBtnTitle:kOK];
             }
             _completionHandler = [handler copy];
             _completionHandler(i);
         }];
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:kError andMessage:exception.reason andBtnTitle:kOK];
    } @finally {
    }
}

- (NSMutableURLRequest *)getRequestForAddNotes:(ClaimNote*)claimNote {
    NSError *e = nil;
    NSString *noteString = [NSString stringWithFormat:@"\"%@\"",claimNote.note];
    NSData *jsonData = [noteString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *s = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"JSON: %@", s);
    if (!jsonData){
        NSLog(@"Error: %@", e);
    }
    NSURL *url = [NSURL URLWithString:[self getAddNoteURLForClaim:claimNote.claimID]];
    NSMutableURLRequest *request = [self makeRequestForURL: url andMethod:@"POST"];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:jsonData];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    return request;
}

- (NSMutableURLRequest *)getRequestForUploadInventory:(Inventory *)inventory addIt:(bool)doAdd {
    @try {
        NSDictionary * dict = [[NSDictionary dictionaryWithObjectsAndKeys:
                                [NSNumber numberWithUnsignedInteger:inventory.claimID], @"ClaimID",
                                [NSNumber numberWithUnsignedInteger:inventory.inventoryID], @"InventoryID",
                                [NSNumber numberWithDouble:[inventory.ordinal doubleValue]], @"Ordinal",
                                [NSNumber numberWithUnsignedInteger:inventory.quantity], @"Quantity",
                                inventory.originalDescription, @"Description",
                                inventory.room, @"Room",
                                [NSNumber numberWithUnsignedInteger:inventory.conditionID], @"ConditionID",
                                [NSNumber numberWithUnsignedInteger:inventory.ageY], @"AgeY",
                                [NSNumber numberWithUnsignedInteger:inventory.ageM], @"AgeM",
                                ([NSNumber numberWithDouble:[inventory.purchasePrice isEqualToNumber:[NSDecimalNumber notANumber]] ? 0 : [inventory.purchasePrice doubleValue]]), @"PurchasePrice",
                                inventory.purchaseLocation, @"PurchaseLocation",
                                nil] init];
        
        NSError *e = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&e];
        
        NSString *s = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON: %@", s);
        if (!jsonData){
            NSLog(@"Error: %@", e);
        }
        
        //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        //[request setHTTPMethod:doAdd ? @"POST" : @"PUT"];

        NSURL *url = [NSURL URLWithString:[self getInventoryURL]];
        NSMutableURLRequest *request = [self makeRequestForURL: url andMethod:doAdd ? @"POST" : @"PUT"];
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:jsonData];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];

        return request;
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:kError andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)ctUploadNotes:(ClaimNote*)claimNote async:(bool)async addIt:(bool)doAdd withCompletion:(void(^)())handler{
    @try {
        NSMutableURLRequest *request = [self getRequestForAddNotes:claimNote];
        if (async) {
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             {
                 [self handleUploadNotes:claimNote withResponse:(NSHTTPURLResponse *)response andData:data andConnectionError:connectionError];
                 _completionHandler = [handler copy];
                 _completionHandler();
             }];
        }
        else {
            NSHTTPURLResponse *response;
            NSError *connectionError;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&connectionError];
            [self handleUploadNotes:claimNote withResponse:response andData:data andConnectionError:connectionError];
            _completionHandler = [handler copy];
            _completionHandler();
        }
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:kError andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)ctSendEmailToInsured:(Claim*)claim async:(bool)async addIt:(bool)doAdd withCompletion:(void(^)())handler{
    @try {
        NSURL *url = [NSURL URLWithString:[self getSentEmailURLForClaim:claim]];
        NSMutableURLRequest *request = [self makeRequestForURL:url andMethod:@"GET"];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
             NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
             NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
             if ((data.length > 0) && (connectionError == nil)) {
                 NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                 if (([dict respondsToSelector:@selector(objectForKey:)]) && ([dict objectForKey:@"ErrorMessage"])){
                     
                     [self showAlertControllerWithTitle:@"Error" andMessage:[dict objectForKey:@"ErrorMessage"] andBtnTitle:kClose];
                 } else if ([httpResponse statusCode] == 200) {
                     [self showAlertControllerWithTitle:@"Claim Tracker" andMessage:@"Email successfully sent" andBtnTitle:kClose];
                 }
             }
             else if (connectionError != nil){
                 
//                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:[NSString stringWithFormat:@"%@", connectionError.localizedDescription] delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//                 alert.tintColor = kBCOrangeColor;
//                 [alert show];
                 [self showAlertControllerWithTitle:kError andMessage:kNetworkConnectionErrorMsg andBtnTitle:kOK];
             }
             _completionHandler = [handler copy];
             _completionHandler();
         }];
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:kError andMessage:exception.reason andBtnTitle:kOK];
    } @finally {
    }
}

- (void)ctUploadInventory:(Inventory *)inventory async:(bool)async addIt:(bool)doAdd withCompletion:(void(^)(int))handler{
    @try {
        NSMutableURLRequest *request = [self getRequestForUploadInventory:inventory addIt:doAdd];

        if (async) {
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
            {
                [self handleUploadInventory:inventory withResponse:(NSHTTPURLResponse *)response andData:data andConnectionError:connectionError];

                _completionHandler = [handler copy];
                _completionHandler();
            }];
        }
        else {
            NSHTTPURLResponse *response;
            NSError *connectionError;
            
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&connectionError];
            
            [self handleUploadInventory:inventory withResponse:response andData:data andConnectionError:connectionError];
            
            _completionHandler = [handler copy];
            if (connectionError != nil)
                _completionHandler(333);
            else
                _completionHandler();
        }
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:kError andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void) handleUploadInventory:(Inventory *)inventory withResponse:(NSHTTPURLResponse *)response andData:(NSData *)data andConnectionError:(NSError *)connectionError {
    @try {
        if ((data.length > 0) && (connectionError == nil)) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
            if (dict == nil){
                [self showAlertControllerWithTitle:@"Error" andMessage:[NSString stringWithFormat:@"Response was Null: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]] andBtnTitle:kClose];
            }
            else if (([dict respondsToSelector:@selector(objectForKey:)]) && ([dict objectForKey:@"ErrorMessage"])) {

                [self showAlertControllerWithTitle:@"Error" andMessage:[dict objectForKey:@"ErrorMessage"] andBtnTitle:kClose];

            }
            else {
                inventory.inventoryID = [[dict objectForKey:@"InventoryID"] unsignedIntegerValue];
                inventory.ordinal = [[NSDecimalNumber alloc] initWithDecimal: [[dict objectForKey:@"Ordinal"] decimalValue]];
                inventory.quantity = [[dict objectForKey:@"Quantity"] unsignedIntegerValue];
                inventory.originalDescription = (NSString *)([[dict objectForKey:@"Description"] isKindOfClass:[NSNull class]] ? @"" : [dict objectForKey:@"Description"]);
                inventory.room = (NSString *)([[dict objectForKey:@"Room"] isKindOfClass:[NSNull class]] ? @"" : [dict objectForKey:@"Room"]);
                inventory.ageY = [[dict objectForKey:@"AgeY"] unsignedIntegerValue];
                inventory.ageM = [[dict objectForKey:@"AgeM"] unsignedIntegerValue];
                inventory.conditionID = [[dict objectForKey:@"ConditionID"] unsignedIntegerValue];
                inventory.purchasePrice = [[NSDecimalNumber alloc] initWithDecimal: [[dict objectForKey:@"PurchasePrice"] decimalValue]];
                inventory.purchaseLocation = (NSString *)([[dict objectForKey:@"PurchaseLocation"] isKindOfClass:[NSNull class]] ? @"" : [dict objectForKey:@"PurchaseLocation"]);
                inventory.edited = FALSE;

                for (Photo *photo in inventory.photoList) { photo.inventoryID = inventory.inventoryID; }
            }
        }
        else if (connectionError != nil) {
            [self showAlertControllerWithTitle:@"Error" andMessage:kNetworkConnectionErrorMsg andBtnTitle:kOK];
        }
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void) handleUploadNotes:(ClaimNote*)claimNote withResponse:(NSHTTPURLResponse *)response andData:(NSData *)data andConnectionError:(NSError *)connectionError {
    @try {
        if ((data.length > 0) && (connectionError == nil)) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
            if (dict == nil){
                [self showAlertControllerWithTitle:@"Error" andMessage:[NSString stringWithFormat:@"Response was Null: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]] andBtnTitle:kClose];
            }
            else if (([dict respondsToSelector:@selector(objectForKey:)]) && ([dict objectForKey:@"ErrorMessage"])) {
                [self showAlertControllerWithTitle:@"Error" andMessage:[dict objectForKey:@"ErrorMessage"] andBtnTitle:kClose];            }
            else {
//                inventory.inventoryID = [[dict objectForKey:@"InventoryID"] unsignedIntegerValue];
//                inventory.ordinal = [[NSDecimalNumber alloc] initWithDecimal: [[dict objectForKey:@"Ordinal"] decimalValue]];
//                inventory.quantity = [[dict objectForKey:@"Quantity"] unsignedIntegerValue];
//                inventory.originalDescription = (NSString *)([[dict objectForKey:@"Description"] isKindOfClass:[NSNull class]] ? @"" : [dict objectForKey:@"Description"]);
//                inventory.room = (NSString *)([[dict objectForKey:@"Room"] isKindOfClass:[NSNull class]] ? @"" : [dict objectForKey:@"Room"]);
//                inventory.ageY = [[dict objectForKey:@"AgeY"] unsignedIntegerValue];
//                inventory.ageM = [[dict objectForKey:@"AgeM"] unsignedIntegerValue];
//                inventory.conditionID = [[dict objectForKey:@"ConditionID"] unsignedIntegerValue];
//                inventory.purchasePrice = [[NSDecimalNumber alloc] initWithDecimal: [[dict objectForKey:@"PurchasePrice"] decimalValue]];
//                inventory.purchaseLocation = (NSString *)([[dict objectForKey:@"PurchaseLocation"] isKindOfClass:[NSNull class]] ? @"" : [dict objectForKey:@"PurchaseLocation"]);
//                inventory.edited = FALSE;
                
//                for (Photo *photo in inventory.photoList) { photo.inventoryID = inventory.inventoryID; }
                //                Claim *claimObj = [[[UserData data]claims]getClaimByID:inventory.claimID];
                //                [claimObj.inventoryList removeInventory:[claimObj.inventoryList getInventoryByID:inventory.inventoryID]];
                //                [claimObj.inventoryList addInventory:inventory];
                claimNote.edited = NO;
            }
        }
        else if (connectionError != nil) {
            [self showAlertControllerWithTitle:@"Error" andMessage:kNetworkConnectionErrorMsg andBtnTitle:kOK];
        }
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:@"Error" andMessage:@"Error connecting to the server" andBtnTitle:kClose];
    } @finally {
    }
}

- (void)getRequestForUploadPhoto:(Photo *)photo withCompletion:(void(^)(NSMutableURLRequest *))handler {
    @try {
        PHImageManager *manager = [PHImageManager defaultManager];
        PHAsset *asset = [PHAsset fetchAssetWithALAssetURL:photo.localURL];
        if (asset){
            @autoreleasepool {
                PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
                options.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
                options.synchronous = YES;
                options.networkAccessAllowed = NO;
                [manager requestImageForAsset:asset targetSize:CGSizeMake(1024.0, 1024.0) contentMode:PHImageContentModeDefault options:options resultHandler:^(UIImage *resultImage, NSDictionary *info)
                 {
                     // Resize
                     float actualHeight = resultImage.size.height;
                     float actualWidth = resultImage.size.width;
                     float imgRatio = actualWidth/actualHeight;
                     float maxRatio = 1024.0/1024.0; //320.0/480.0;
                     
                     if(imgRatio!=maxRatio){
                         if(imgRatio < maxRatio){
                             imgRatio = 1024.0 / actualHeight;
                             actualWidth = imgRatio * actualWidth;
                             actualHeight = 1024.0;
                         }
                         else{
                             imgRatio = 1024.0 / actualWidth;
                             actualHeight = imgRatio * actualHeight;
                             actualWidth = 1024.0;
                         }
                     }
                     CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
                     UIGraphicsBeginImageContext(rect.size);
                     [resultImage drawInRect:rect];
                     UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
                     UIGraphicsEndImageContext();
                     // End Resize
                     
                     NSData *img = UIImageJPEGRepresentation(smallImage, 0.4);
                     NSString *name = [NSString stringWithFormat:@"%@.jpg",
                                       [[photo.imageName capitalizedString]
                                        stringByReplacingOccurrencesOfString:@" " withString:@""]];
                     NSString *str = [img base64EncodedStringWithOptions:0];
                     NSDictionary *dict = [[NSDictionary dictionaryWithObjectsAndKeys:
                                            [NSNumber numberWithUnsignedInteger:photo.inventoryID], @"InventoryID",
                                            [NSNumber numberWithUnsignedInteger:photo.claimID], @"ClaimID",
                                            name, @"Name",
                                            [photo.localURL absoluteString], @"PhysicalLocation",
                                            str, @"Base64Image",
                                            nil] init];
                     NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];

                     NSURL *url = [NSURL URLWithString:[self getPhotoURL]];
                     //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                     NSMutableURLRequest *request = [self makeRequestForURL: url andMethod:@"POST"];
                     //[request setHTTPMethod:@"POST"];
                     [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                     [request setHTTPBody:jsonData];
                     
                     if (handler){
                         _completionHandler = [handler copy];
                         _completionHandler(request);
                     }
                 }];
            }
        }
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:kError andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)ctUploadPhoto:(Photo *)photo async:(bool)async withCompletion:(void(^)(int))handler{
    @try {
        [self getRequestForUploadPhoto:photo withCompletion:^(NSMutableURLRequest *request){
            if (async) {
                [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
                 {
                     [self handleUploadPhoto:photo withResponse:response andData:data andConnectionError:connectionError];
                     
                     _completionHandler = [handler copy];
                     _completionHandler();
                 }];
            }
            else {
                NSURLResponse *response;
                NSError *connectionError;
                NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&connectionError];
                
                [self handleUploadPhoto:photo withResponse:response andData:data andConnectionError:connectionError];
                
                _completionHandler = [handler copy];
                if (connectionError != nil)
                    _completionHandler(333);
                else
                    _completionHandler();
            }
        }];
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:kError andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)handleUploadPhoto:(Photo *) photo withResponse:(NSURLResponse *)response andData:(NSData *)data andConnectionError:(NSError *)connectionError {
    @try {
        if ((data.length > 0) && (connectionError == nil)) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
            if (([dict respondsToSelector:@selector(objectForKey:)]) && ([dict objectForKey:@"ErrorMessage"])){
                [self showAlertControllerWithTitle:@"Error" andMessage:[dict objectForKey:@"ErrorMessage"] andBtnTitle:kClose];
            } else {
                photo.added = FALSE;
            }
        }
        else if (connectionError != nil){
            [self showAlertControllerWithTitle:@"Error" andMessage:kNetworkConnectionErrorMsg andBtnTitle:kOK];
        }
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)showAlertControllerWithTitle:(NSString*)alertTitle andMessage:(NSString*)alertMsg andBtnTitle:(NSString*)btnTitle {
    UIWindow *keyWindow = [[UIApplication sharedApplication]keyWindow];
    UIViewController *mainController = [keyWindow rootViewController];
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:alertTitle message:alertMsg preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *closeButton = [UIAlertAction actionWithTitle:btnTitle style:UIAlertActionStyleCancel handler: ^(UIAlertAction * action) {
        
    }];
    [closeButton setValue:kBCOrangeColor forKey:@"titleTextColor"];
    [alertView addAction:closeButton];
    [mainController presentViewController: alertView animated:YES completion:nil];
}


/*- (void)ctGetPhotosForInventory:(Inventory *)inventory withActivityIndex:(int)i withCompletion:(void(^)(int))handler{
 NSURL *url = [NSURL URLWithString:[self getPhotoURLForInventory:inventory]];
 NSURLRequest *request = [NSURLRequest requestWithURL:url];
 
 [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
 {
 if ((data.length > 0) && (connectionError == nil)) {
 NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
 if (([dict respondsToSelector:@selector(objectForKey:)]) && ([dict objectForKey:@"ErrorMessage"])){
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[dict objectForKey:@"ErrorMessage"] delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
 [alert show];
 } else {
 NSArray *arr = (NSArray *)dict;
 if (inventory.photoList){
 [inventory.photoList clear];
 } else {
 inventory.photoList = [[PhotoList alloc] init];
 }
 for (int i = 0; i < arr.count; i++){
 Photo *photo = [[Photo alloc] init];
 dict = [arr objectAtIndex:i];
 photo.added = false;
 photo.index = i;
 photo.inventoryID = inventory.inventoryID;
 photo.claimID = inventory.claimID;
 photo.claimName = inventory.claimName;
 photo.localURL = [NSURL URLWithString:(NSString *)([[dict objectForKey:@"PhysicalLocation"] isKindOfClass:[NSNull class]] ? @"" : [dict objectForKey:@"PhysicalLocation"])];
 photo.imageName = (NSString *)([[dict objectForKey:@"Name"] isKindOfClass:[NSNull class]] ? @"" : [dict objectForKey:@"Name"]);
 [inventory.photoList addPhoto:photo];
 }
 }
 }
 else if (connectionError != nil){
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:[NSString stringWithFormat:@"%@", connectionError.localizedDescription] delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
 [alert show];
 }
 _completionHandler = [handler copy];
 _completionHandler(i);
 }];
 }*/

@end
