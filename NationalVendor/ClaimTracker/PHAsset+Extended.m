//
//  PHAsset+Extended.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 6/22/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "PHAsset+Extended.h"
#import "PHAssetCollection+Extended.h"
#import "Constants.h"

@implementation PHAsset (Extended)

+ (PHAsset *)fetchAssetWithALAssetURL:(NSURL *)url {
    @try {
        //PHImageManager *manager = [PHImageManager defaultManager];

        PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[url] options:nil];
        if (result.count > 0){
            return result[0];
        }
        else {
            PHAsset *asset;
            NSString *str = [url absoluteString];
            NSUInteger startOfString = [str rangeOfString:@"="].location + 1;
            //NSUInteger endOfString = startOfString + 36;
            NSRange range = NSMakeRange(startOfString, 36);
            NSString *localIDFragment = [str substringWithRange:range];
            PHFetchResult *fetchResultForPhotoStream = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumMyPhotoStream options: nil];
            if (fetchResultForPhotoStream.count > 0) {
                PHAssetCollection *photostream = fetchResultForPhotoStream[0];
                PHFetchResult *fetchResultForPhotoStreamAssets = [PHAsset fetchAssetsInAssetCollection:photostream options: nil];
                if (fetchResultForPhotoStreamAssets.count > 0) {
                    bool stop = false;
                    NSRange range2 = NSMakeRange(0, 36);
                    for (int i = 0; i < (fetchResultForPhotoStreamAssets.count) && (!stop); i++) {
                        PHAsset *phAssetBeingCompared = fetchResultForPhotoStreamAssets[i];
                        if ([[[phAssetBeingCompared localIdentifier] substringWithRange:range2] isEqualToString:localIDFragment]){
                            asset = phAssetBeingCompared;
                            stop = true;
                        }
                    }
                    return asset;
                }
            }
            return nil;
        }
        
        //PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
        //options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        //options.synchronous = YES;
        //options.networkAccessAllowed = NO;

        //[manager requestImageForAsset:asset targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeDefault options:options resultHandler:^(UIImage *resultImage, NSDictionary *info)
         //{
         //    cell.imageView.image = resultImage;
         //}];
    } @catch (NSException *exception) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//        [alert show];
        [self showAlertControllerWithTitle:kError andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

+ (void)addImageToCTAlbum:(UIImage *)image ForClaim:(NSString *)claim withCompletion:(void(^)())handler {
    @try {
        PHAssetCollection *ctAlbum = [PHAssetCollection getCTAlbumForClaim:claim];
        
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetChangeRequest *changeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
            PHObjectPlaceholder *placeholder = changeRequest.placeholderForCreatedAsset;
            PHAssetCollectionChangeRequest *albumRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:ctAlbum];
            [albumRequest addAssets:@[placeholder]];
            
        } completionHandler:^(BOOL success, NSError *error) {
            if (!success) {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error Saving Photo" message:[NSString stringWithFormat:@"%@", error.description] delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//                [alert show];
                [self showAlertControllerWithTitle:@"Error Saving Photo" andMessage:[NSString stringWithFormat:@"%@", error.description] andBtnTitle:kClose];
            }
            if (handler){
                void (^_completionHandler)();
                _completionHandler = [handler copy];
                _completionHandler();
            }
        }];
    } @catch (NSException *exception) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//        [alert show];
        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}
+ (void)showAlertControllerWithTitle:(NSString*)alertTitle andMessage:(NSString*)alertMsg andBtnTitle:(NSString*)btnTitle {
    UIWindow *keyWindow = [[UIApplication sharedApplication]keyWindow];
    UIViewController *mainController = [keyWindow rootViewController];
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:alertTitle message:alertMsg preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *closeButton = [UIAlertAction actionWithTitle:btnTitle style:UIAlertActionStyleCancel handler: ^(UIAlertAction * action) {
        
    }];
    [closeButton setValue:kBCOrangeColor forKey:@"titleTextColor"];
    [alertView addAction:closeButton];
    [mainController presentViewController: alertView animated:YES completion:nil];
//    alertView.view.tintColor = kBCOrangeColor;
}

@end
