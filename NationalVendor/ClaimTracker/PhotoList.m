//
//  PhotoList.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/22/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "PhotoList.h"
#import "DataSource.h"

@interface PhotoList()

@property NSMutableArray *photoArray;

@end

@implementation PhotoList

- (PhotoList *) init
{
    self = [super init];
    self.photoArray = [[NSMutableArray alloc] init];
    return self;
}

- (NSUInteger)count
{
    return [self.photoArray count];
}

- (Photo *)getPhotoByIndex:(NSUInteger)index
{
    return [self.photoArray objectAtIndex:index];
}

- (void)addPhoto:(Photo *) photo
{
    [self.photoArray addObject:photo];
}

- (void)removePhoto:(Photo *) photo
{
    [self.photoArray removeObject:photo];
}

- (void)clear
{
    [self.photoArray removeAllObjects];
}

- (void) encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.photoArray forKey:@"photoArray"];
}

- (id) initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.photoArray = [decoder decodeObjectForKey:@"photoArray"];
        if (!self.photoArray) {
            self.photoArray = [[NSMutableArray alloc] init];
        }
    }
    return self;
}

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id *)buffer count:(NSUInteger)len
{
    return [self.photoArray countByEnumeratingWithState:state objects:buffer count:len];
}

@end
