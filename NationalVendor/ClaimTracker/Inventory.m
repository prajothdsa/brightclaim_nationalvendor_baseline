//
//  Inventory.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/18/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "Inventory.h"

@implementation Inventory

- (void) encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.claimID] forKey:@"claimID"];
    [encoder encodeObject:self.claimName forKey:@"claimName"];
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.index] forKey:@"inventoryIndex"];
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.inventoryID] forKey:@"inventoryID"];
    [encoder encodeObject:self.ordinal forKey:@"ordinal"];
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.quantity] forKey:@"quantity"];
    [encoder encodeObject:self.originalDescription forKey:@"originalDescription"];
    [encoder encodeObject:self.room forKey:@"room"];
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.conditionID] forKey:@"conditionID"];
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.ageY] forKey:@"ageY"];
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.ageM] forKey:@"ageM"];
    [encoder encodeObject:self.purchaseLocation forKey:@"purchaseLocation"];
    [encoder encodeObject:self.purchasePrice forKey:@"purchasePrice"];

    [encoder encodeObject:self.photoList forKey:@"photoList"];

    [encoder encodeBool:self.edited forKey:@"edited"];
    //[encoder encodeBool:self.added forKey:@"added"];
}

- (id) initWithCoder:(NSCoder *)decoder{
    self = [super init];
    if (self){
        self.claimID = [[decoder decodeObjectForKey:@"claimID"] unsignedIntegerValue];
        self.claimName = [decoder decodeObjectForKey:@"claimName"];
        self.index = [[decoder decodeObjectForKey:@"inventoryIndex"] unsignedIntegerValue];
        self.inventoryID = [[decoder decodeObjectForKey:@"inventoryID"] unsignedIntegerValue];
        self.ordinal = [decoder decodeObjectForKey:@"ordinal"];
        self.quantity = [[decoder decodeObjectForKey:@"quantity"] unsignedIntegerValue];
        self.originalDescription = [decoder decodeObjectForKey:@"originalDescription"];
        self.room = [decoder decodeObjectForKey:@"room"];
        self.ageY = [[decoder decodeObjectForKey:@"ageY"] unsignedIntegerValue];
        self.ageM = [[decoder decodeObjectForKey:@"ageM"] unsignedIntegerValue];
        self.conditionID = [[decoder decodeObjectForKey:@"conditionID"] unsignedIntegerValue];
        self.purchaseLocation = [decoder decodeObjectForKey:@"purchaseLocation"];
        self.purchasePrice = [decoder decodeObjectForKey:@"purchasePrice"];
        
        self.photoList = [decoder decodeObjectForKey:@"photoList"];
        if (!self.photoList) self.photoList = [[PhotoList alloc] init];
        
        self.edited = [decoder decodeBoolForKey:@"edited"];
}
    return self;
}

- (id) init
{
    self = [super init];
    self.photoList = [[PhotoList alloc] init];
    return self;
}

@end
