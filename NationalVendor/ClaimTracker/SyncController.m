//
//  SyncController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/20/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "SyncController.h"
#import "UserData.h"
#import "DataSource.h"

@interface SyncController()

- (IBAction)syncAll:(UIBarButtonItem *)sender;

@end

@implementation SyncController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UserData data] saveClaims];
}

- (IBAction)syncAll:(UIBarButtonItem *)sender {
    @try {
        DataSource * d = [DataSource new];
        if (!d.isConnected)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You are not connected." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            NSMutableArray *arr = [[UserData data] getSyncItems];
            [[UserData data] syncToServerWithTableView:self.tableView andArray:arr];
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[UserData data] needToSyncCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PrototypeCell" forIndexPath:indexPath];
        cell.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        cell.clipsToBounds = YES;
        cell.textLabel.numberOfLines = 0;
        
        NSMutableArray *arr = [[UserData data] getSyncItems];
        
        if ([[arr objectAtIndex:indexPath.row] isKindOfClass:[Claim class]]){
            Claim *claim = (Claim *)[arr objectAtIndex:indexPath.row];
            cell.textLabel.text = claim.claimName;
        }
        else if ([[arr objectAtIndex:indexPath.row] isKindOfClass:[Inventory class]]){
            Inventory *inventory = (Inventory *)[arr objectAtIndex:indexPath.row];
            cell.textLabel.text = [NSString stringWithFormat:@"%@\nInventory #%@", inventory.claimName, inventory.ordinal.stringValue];
        }
        else if ([[arr objectAtIndex:indexPath.row] isKindOfClass:[Photo class]]){
            Photo *photo = (Photo *)[arr objectAtIndex:indexPath.row];
            cell.textLabel.text = [NSString stringWithFormat:@"%@\n#%@ Photo", photo.claimName, photo.inventoryOrdinal.stringValue];
        }
        //UILabel *label;
        //label = (UILabel *)[cell viewWithTag:1];
        //label.text = [NSString stringWithFormat:@"%@", claim.claimName];
        
        //label = (UILabel *)[cell viewWithTag:2];
        //label.text = [NSString stringWithFormat:@"%@", claim.insuredAddress];
        
        return cell;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

@end
