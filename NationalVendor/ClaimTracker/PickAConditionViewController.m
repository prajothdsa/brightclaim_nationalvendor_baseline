//
//  PickAConditionViewController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 8/2/16.
//  Copyright © 2016 com.nationalvendor. All rights reserved.
//

#import "PickAConditionViewController.h"
#import "UserData.h"
#import "CTNavigationController.h"

@interface PickAConditionViewController ()

@property (weak, nonatomic) IBOutlet UIPickerView *picker;
- (IBAction)cmdCancel:(UIButton *)sender;
- (IBAction)cmdSave:(UIButton *)sender;

@end

@implementation PickAConditionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.picker.dataSource = self;
    self.picker.delegate = self;
    
    [self.picker selectRow:[[UserData data].conditions getConditionByName:self.textField.text].index inComponent:0 animated:true];
}

- (IBAction)cmdCancel:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)cmdSave:(UIBarButtonItem *)sender {
    NSInteger row = [self.picker selectedRowInComponent:0];
    if (row)
    {
        self.textField.text = [[self.picker delegate] pickerView:self.picker titleForRow:row forComponent:0];
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [UserData data].conditions.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [[UserData data].conditions getConditionByIndex:row].conditionName;
}

@end
