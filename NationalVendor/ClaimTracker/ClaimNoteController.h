//
//  ClaimNoteController.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/8/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClaimNote.h"

@interface ClaimNoteController : UIViewController

@property ClaimNote * claimNote;

@end
