//
//  InventoryPhotosController.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/22/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Claim.h"
#import "Inventory.h"

@interface InventoryPhotosController : UICollectionViewController

@property Claim *claim;
@property Inventory *inventory;

@end
