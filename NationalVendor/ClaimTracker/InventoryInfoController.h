//
//  InventoryInfoController.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/19/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Claim.h"
#import "Inventory.h"
#import "UITextField+Extended.h"

@interface InventoryInfoController : UITableViewController<UITextFieldDelegate>

@property Claim *claim;
@property Inventory *inventory;
@property bool editMode;
- (IBAction)btnCancelAction:(id)sender;
- (IBAction)btnSaveAction:(id)sender;

@end
