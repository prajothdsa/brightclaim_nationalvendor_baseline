//
//  UITextFieldExt.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 6/4/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "UITextField+Extended.h"
#import <objc/runtime.h>

static char defaultHashKey;

@implementation UITextField (Extended)

- (UITextField *) nextTextField
{
    return objc_getAssociatedObject(self, &defaultHashKey);
}

- (void)setNextTextField:(UITextField *)nextTextField
{
    objc_setAssociatedObject(self, &defaultHashKey, nextTextField, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
