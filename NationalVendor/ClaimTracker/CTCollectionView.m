//
//  CTCollectionView.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 6/24/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>
#import "CTCollectionView.h"
#import "PHAsset+Extended.h"
#import "PHAssetCollection+Extended.h"
#import "PhotoViewController.h"
#import "Photo.h"
#import "InventoryPhotosController.h"
#import "CTTabBarController.h"
#import "CTCollectionViewCell.h"
#import "UserData.h"

@interface CTCollectionView ()

@property (nonatomic) UIImagePickerController *imagePickerController;

@end

@implementation CTCollectionView

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.inventory.photoList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        CTCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCell" forIndexPath:indexPath];
        cell.presenterViewController = self.presenterViewController;
        cell.collectionView = self;
        Photo *p = [self.inventory.photoList getPhotoByIndex:indexPath.row];
        
        if (p.localURL) {
            PHAsset *asset = [PHAsset fetchAssetWithALAssetURL:p.localURL];
            if (asset){
                PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
                options.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
                options.synchronous = YES;
                options.networkAccessAllowed = NO;
                PHImageManager *manager = [PHImageManager defaultManager];
                [manager requestImageForAsset:asset targetSize:CGSizeMake(145,138) contentMode:PHImageContentModeDefault options:options resultHandler:^(UIImage *resultImage, NSDictionary *info)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         cell.imageView.image = resultImage;
                     });
                 }];
            }
        }
        
        cell.imageTitle.delegate = cell;
        cell.photo = p;
        cell.inventory = self.inventory;
        cell.imageTitle.text = p.imageName;
        return cell;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

#pragma ImagePicker

- (IBAction)showImagePickerForCamera:(id)sender
{
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
}

- (IBAction)showImagePickerForPhotoPicker:(id)sender
{
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    @try {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
        imagePickerController.sourceType = sourceType;
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = TRUE;
        
        if (sourceType == UIImagePickerControllerSourceTypeCamera)
        {
            AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if (status == AVAuthorizationStatusAuthorized) { // authorized
                dispatch_async(dispatch_get_main_queue(), ^{
                    imagePickerController.showsCameraControls = YES;
                    self.imagePickerController = imagePickerController;
                    [self.presenterViewController presentViewController:self.imagePickerController animated:YES completion:NULL];
                });
            }
            else if((status == AVAuthorizationStatusDenied) || (status == AVAuthorizationStatusRestricted)){ // denied
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Not authorized to use the camera.  Please give ClaimTracker access in Settings." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                [alert show];
            }
            else if(status == AVAuthorizationStatusNotDetermined) { // not determined
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    if (granted) { // Access has been granted ..do something
                        dispatch_async(dispatch_get_main_queue(), ^{
                            imagePickerController.showsCameraControls = YES;
                            self.imagePickerController = imagePickerController;
                            [self.presenterViewController presentViewController:self.imagePickerController animated:YES completion:NULL];
                        });
                    } else { // Access denied ..do something
                        dispatch_async (dispatch_get_main_queue(), ^{
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Not authorized to use the camera.  Please give ClaimTracker access in Settings." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                            [alert show];
                        });
                    }
                }];
            }
        }
        else {
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                if (status == PHAuthorizationStatusAuthorized){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.imagePickerController = imagePickerController;
                        [self.presenterViewController presentViewController:self.imagePickerController animated:YES completion:NULL];
                    });
                }
                else {
                    dispatch_async (dispatch_get_main_queue(), ^{
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Not authorized to access photo albums.  Please give ClaimTracker access in Settings." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                        [alert show];
                    });
                }
            }];
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}


#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    @try {
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera){
            UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
            UIView *spinner = [[UserData data] makeSpinner];
            
            [PHAsset addImageToCTAlbum:image ForClaim:self.claim.claimName withCompletion:^{
                @try {
                    PHAssetCollection *ctAlbum = [PHAssetCollection getCTAlbumForClaim:self.claim.claimName];
                    PHFetchResult *fetchResult = [PHAsset fetchAssetsInAssetCollection:ctAlbum options:nil];
                    PHAsset *asset = fetchResult.lastObject;
                    
                    NSRange range = NSMakeRange(0, 36);
                    NSString *url = [NSString stringWithFormat:@"assets-library://asset/asset.JPG?id=%@&ext=JPG",[asset.localIdentifier substringWithRange:range]];
                    
                    Photo *p = [[Photo alloc] init];
                    p.imageName = [NSString stringWithFormat:@"Photo %@", self.inventory.ordinal];
                    p.claimID = self.claim.claimID;
                    p.claimName = self.claim.claimName;
                    p.inventoryID = self.inventory.inventoryID;
                    p.inventoryOrdinal = self.inventory.ordinal;
                    p.added = TRUE;
                    [self.inventory.photoList addPhoto:p];
                    p.localURL = [NSURL URLWithString:url];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        @try {
                            if (self.imagePickerController)
                            {
                                [self.presenterViewController dismissViewControllerAnimated:YES completion:NULL];
                            }
                            self.imagePickerController = nil;
                            [self reloadData];
                        } @catch (NSException *exception) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                            [alert show];
                        } @finally {
                            [[UserData data] removeSpinner:spinner];
                        }
                    });
                } @catch (NSException *exception) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                    [alert show];
                } @finally {
                    [[UserData data] removeSpinner:spinner];
                }
            }];
        }
        else {
            NSURL *url = [info valueForKey:UIImagePickerControllerReferenceURL];
            
            Photo *p = [[Photo alloc] init];
            p.localURL = url;
            p.imageName = [NSString stringWithFormat:@"Photo %@", self.inventory.ordinal];
            p.claimID = self.claim.claimID;
            p.claimName = self.claim.claimName;
            p.inventoryID = self.inventory.inventoryID;
            p.inventoryOrdinal = self.inventory.ordinal;
            p.added = TRUE;
            [self.inventory.photoList addPhoto:p];
            [self reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.imagePickerController)
                {
                    [self.presenterViewController dismissViewControllerAnimated:YES completion:NULL];
                }
                self.imagePickerController = nil;
            });
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

/*- (void)image:(UIImage *)image hasBeenSavedInPhotoAlbumWithError:(NSError *)error contextInfo:(void *)contextInfo {
 if (error) {
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error Saving Photo" message:[NSString stringWithFormat:@"%@", error.description] delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
 [alert show];
 } else {
 // .... do anything you want here to handle
 // .... when the image has been saved in the photo album
 }
 }*/

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self.presenterViewController dismissViewControllerAnimated:YES completion:NULL];
    self.imagePickerController = nil;
}

@end
