//
//  PHAssetCollection+Extended.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 6/23/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <Photos/Photos.h>

@interface PHAssetCollection (Extended)

+ (void) createCTAlbumForClaim:(NSString *)claim WithCompletion:(void(^)())handler;
+ (PHAssetCollection *)getCTAlbumForClaim:(NSString *)claim;

@end
