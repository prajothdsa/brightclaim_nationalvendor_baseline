//
//  ClaimNotesController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/6/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "ClaimNotesController.h"
#import "CTTabBarController.h"
#import "ClaimNoteController.h"
#import "UserData.h"
#import "CTNavigationController.h"

@interface ClaimNotesController ()

@end

@implementation ClaimNotesController

- (void)doRefresh{
    @try {
        [self.claim refreshClaimNotesWithCompletion:^(void){
            [self.tableView reloadData];
            //[[UserData data] saveClaims];
            if (self.refreshControl) [self.refreshControl endRefreshing];
        }];
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (void)viewDidLoad {
    @try {
        [super viewDidLoad];
        CTTabBarController *source = (CTTabBarController *)self.parentViewController.parentViewController;
        self.claim = source.selectedClaim;

        self.refreshControl = [[UIRefreshControl alloc] init];
        self.refreshControl.backgroundColor = [UIColor blueColor];
        self.refreshControl.tintColor = [UIColor whiteColor];
        [self.refreshControl addTarget:self action:@selector(doRefresh) forControlEvents:UIControlEventValueChanged];
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    @try {
        if ([segue.identifier isEqualToString:@"segueClaimNoteListToClaimNote"]){
            NSInteger row = [self.tableView indexPathForSelectedRow].row;
            CTNavigationController *destination = segue.destinationViewController;
            
            self.selectedClaimNote = [self.claim.claimNoteList getClaimNoteByIndex:row];
            destination.selectedClaimNote = self.selectedClaimNote;
        }
        //else if ([segue.identifier isEqualToString:@"segueToClaimList"]) {
        //    UITabBarController *destination = segue.destinationViewController;
        //    [destination setSelectedIndex:1];
        //}
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}


- (IBAction)unwindToClaimNotes:(UIStoryboardSegue *)segue {
    //ClaimNoteController *source = [segue sourceViewController];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    @try {
        // Return the number of sections.
        if ([self.claim.claimNoteList count] == 0){
            UILabel *lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(0,0,self.view.bounds.size.width, self.view.bounds.size.height)];
            lblMessage.text = @"No data is currently available.  Please pull down to refresh.";
            lblMessage.textColor = [UIColor blackColor];
            lblMessage.numberOfLines = 0;
            lblMessage.textAlignment = NSTextAlignmentCenter;
            lblMessage.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
            [lblMessage sizeToFit];
            self.tableView.backgroundView = lblMessage;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 0;
        }
        else {
            self.tableView.backgroundView = nil;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            return 1;
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.claim.claimNoteList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PrototypeCell" forIndexPath:indexPath];
        
        ClaimNote *claimNote = [self.claim.claimNoteList getClaimNoteByIndex:indexPath.row];
        
        UILabel *label;
        label = (UILabel *)[cell viewWithTag:1];
        label.text = [NSString stringWithFormat:@"%@", claimNote.note];

        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MM/dd/yyyy"];
        label = (UILabel *)[cell viewWithTag:2];
        label.text = [NSString stringWithFormat:@"Entered %@ By %@ for status %@", [df stringFromDate:claimNote.date], claimNote.repName, claimNote.status];

        return cell;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

@end
