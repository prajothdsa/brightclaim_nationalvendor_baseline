//
//  AppDelegate.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 4/29/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSKeyChain.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
/*
- (void)showLoginScreen:(BOOL)animated;
- (void)logout;
*/
@end

