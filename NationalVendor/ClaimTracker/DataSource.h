//
//  DataSource.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/4/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Claim.h"

@interface DataSource : NSObject
{
    void (^_completionHandler)();
}

- (bool)isConnected;

- (bool)ctAuthenticateUser:(NSString *)userName andPassword:(NSString *)password;

- (void)ctGetUserDataWithCompletion:(void(^)())handler;

- (void)ctGetClaimsWithCompletion:(void(^)())handler;

- (void)ctGetConditionsWithCompletion:(void(^)())handler;

- (void)ctGetClaim:(Claim *)claim withCompletion:(void(^)())handler;

- (void)ctGetClaimNotesForClaim:(Claim *)claim withCompletion:(void(^)())handler;

- (void)ctGetInventoryForClaim:(Claim *)claim withActivityIndex:(int)i withCompletion:(void(^)(int))handler;

- (void)ctUploadInventory:(Inventory *)inventory async:(bool)async addIt:(bool)doAdd withCompletion:(void(^)(int))handler;

- (void)ctUploadPhoto:(Photo *)photo async:(bool)async withCompletion:(void(^)(int))handler;

- (void)ctUploadNotes:(ClaimNote*)claimNote async:(bool)async addIt:(bool)doAdd withCompletion:(void(^)())handler;

- (void)ctSendEmailToInsured:(Claim*)claim async:(bool)async addIt:(bool)doAdd withCompletion:(void(^)())handler;
@end
