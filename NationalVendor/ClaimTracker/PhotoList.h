//
//  PhotoList.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/22/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Photo.h"

@interface PhotoList : NSObject

- (NSUInteger)count;
//- (Photo *)getPhotoByID:(NSUInteger) photoID;
- (Photo *)getPhotoByIndex:(NSUInteger) index;
- (void)addPhoto:(Photo *) photo;
- (void)removePhoto:(Photo *)photo;
- (void)clear;

- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)decoder;

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id *)buffer count:(NSUInteger)len;

@end
