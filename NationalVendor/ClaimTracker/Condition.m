//
//  Condition.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 8/2/16.
//  Copyright © 2016 com.nationalvendor. All rights reserved.
//

#import "Condition.h"

@implementation Condition

- (void) encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.conditionID] forKey:@"conditionID"];
    [encoder encodeObject:self.conditionName forKey:@"conditionName"];
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.index] forKey:@"inventoryIndex"];

    [encoder encodeBool:self.added forKey:@"added"];
}

- (id) initWithCoder:(NSCoder *)decoder{
    self = [super init];
    if (self){
        self.conditionID = [[decoder decodeObjectForKey:@"conditionID"] unsignedIntegerValue];
        self.conditionName = [decoder decodeObjectForKey:@"conditionName"];
        self.index = [[decoder decodeObjectForKey:@"index"] unsignedIntegerValue];

        self.added = [decoder decodeBoolForKey:@"added"];
    }
    return self;
}

@end
