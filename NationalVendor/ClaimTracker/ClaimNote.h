//
//  ClaimNote.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/7/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClaimNote : NSObject

@property NSUInteger claimID;
@property NSUInteger index;
@property NSUInteger claimNoteID;
@property NSString *note;
@property NSString *status;
@property NSString *repName;
@property NSDate *date;

- (void) encodeWithCoder:(NSCoder *)encoder;
- (id) initWithCoder:(NSCoder *)decoder;
@property BOOL edited;

@end
