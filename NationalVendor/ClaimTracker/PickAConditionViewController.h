//
//  PickAConditionViewController.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 8/2/16.
//  Copyright © 2016 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickAConditionViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>

@property UITextField *textField;

@end
