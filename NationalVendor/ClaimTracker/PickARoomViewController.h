//
//  PickARoomViewController.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/29/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Claim.h"

@interface PickARoomViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>

@property Claim *claim;
@property UITextField *textField;

@end
