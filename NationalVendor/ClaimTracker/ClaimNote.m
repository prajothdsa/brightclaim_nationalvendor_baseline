//
//  ClaimNote.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/7/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "ClaimNote.h"

@implementation ClaimNote

- (void) encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.claimID] forKey:@"claimID"];
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.index] forKey:@"noteIndex"];
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.claimNoteID] forKey:@"claimNoteID"];
    [encoder encodeObject:self.status forKey:@"status"];
    [encoder encodeObject:self.note forKey:@"note"];
    [encoder encodeObject:self.repName forKey:@"repName"];
    [encoder encodeObject:self.date forKey:@"date"];
}

- (id) initWithCoder:(NSCoder *)decoder{
    self = [super init];
    if (self){
        self.claimID = [[decoder decodeObjectForKey:@"claimID"] unsignedIntegerValue];
        self.index = [[decoder decodeObjectForKey:@"noteIndex"] unsignedIntegerValue];
        self.claimNoteID = [[decoder decodeObjectForKey:@"claimNoteID"] unsignedIntegerValue];
        self.status = [decoder decodeObjectForKey:@"status"];
        self.note = [decoder decodeObjectForKey:@"note"];
        self.repName = [decoder decodeObjectForKey:@"repName"];
        self.date = [decoder decodeObjectForKey:@"date"];
    }
    return self;
}

@end
