//
//  QuickAddInventoryController.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 6/24/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuickAddInventoryController : UITableViewController<UITextFieldDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

//@property UIViewController *presenterViewController;
- (IBAction)showImagePickerForCamera:(id)sender;
- (IBAction)showImagePickerForPhotoPicker:(id)sender;

@end
