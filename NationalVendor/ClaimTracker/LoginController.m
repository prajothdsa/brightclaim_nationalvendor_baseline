//
//  LoginViewController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 4/30/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "LoginController.h"
#import "UserData.h"
#import "AppDelegate.h"

@interface LoginController ()

@property (weak, nonatomic) IBOutlet UITextField *txtEMail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
- (IBAction)cmdLogin:(UIButton *)sender;

@end

@implementation LoginController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.txtEMail.delegate = self;
    self.txtPassword.delegate = self;

    self.txtEMail.nextTextField = self.txtPassword;
    self.txtPassword.nextTextField = nil;

    if ([UserData data].lastLogin) self.txtEMail.text = [UserData data].lastLogin;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField
{
    UITextField *next = theTextField.nextTextField;
    if (next)
    {
        [next becomeFirstResponder];
    }
    else
    {
        [theTextField resignFirstResponder];
        [self login];
    }
    
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cmdLogin:(UIButton *)sender {
    [self login];
}

- (void)login
{
    if (self.txtEMail.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter your login name" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if (self.txtPassword.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter your password" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    NSArray *arrIllegalChars = [NSArray arrayWithObjects:@"*", @"<", @">", @"%", @"&", @":", @"\\", @"?", nil];
    for (NSString *c in arrIllegalChars)
    {
        if ([self.txtEMail.text rangeOfString:c].location != NSNotFound)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"E-Mail Address cannot contain the %@ character", c] delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [alert show];
            return;
        }
        if ([self.txtPassword.text rangeOfString:c].location != NSNotFound)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"Password cannot contain the %@ character", c] delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    
    // If logging in as a different user, we need to clear out claims from last user
    if (![[UserData data].lastLogin isEqualToString: self.txtEMail.text]) {
        [[UserData data] clearClaimData];
    }

    if ([[UserData data] authenticateUser:self.txtEMail.text andPassword:self.txtPassword.text]){
        UIView *spinner = [[UserData data] makeSpinner];

        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        appDelegate.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateInitialViewController];

        [[UserData data] refreshWithCompletion:^(void){
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UserData data] removeSpinner:spinner];
                AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                appDelegate.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
            });
        }];
    }
         
    /*if ()
    if (([UserData data].password) && ([self.txtPassword.text isEqualToString:[UserData data].password])) {
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        appDelegate.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
        [UserData data].lastLogin = self.txtEMail.text;
        [UserData data].isAuthenticated = true;
    }
    else if (([UserData data].password) && (![self.txtPassword.text isEqualToString:[UserData data].password])) {
    }
    else {
        UIView *spinner = [[UserData data] makeSpinner];
        
        [UserData data].lastLogin = self.txtEMail.text;
        [UserData data].password = self.txtPassword.text;

        [[UserData data] refreshWithCompletion:^(void){
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UserData data] removeSpinner:spinner];
                if ([UserData data].isAuthenticated)
                {
                    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                    appDelegate.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
                }
                else {
                    [UserData data].lastLogin = nil;
                    [UserData data].password = nil;
                }
            });
        }];
    }
    */
}
@end
