//
//  InventoryListController.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/18/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Claim.h"
#import "Inventory.h"

@interface InventoryListController : UITableViewController

@property Claim *claim;
@property Inventory *selectedInventory;

@end
