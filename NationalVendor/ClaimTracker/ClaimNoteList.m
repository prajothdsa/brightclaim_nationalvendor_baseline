//
//  ClaimNoteList.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/7/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "ClaimNoteList.h"
#import "DataSource.h"

@interface ClaimNoteList()

@property NSMutableArray *claimNoteArray;

@end

@implementation ClaimNoteList

- (ClaimNoteList *) init{
    self = [super init];
    self.claimNoteArray = [[NSMutableArray alloc] init];
    return self;
}

- (NSUInteger)count{
    return [self.claimNoteArray count];
}

- (ClaimNote *)getClaimNoteByIndex:(NSUInteger)index{
    return [self.claimNoteArray objectAtIndex:index];
}

- (ClaimNote *)getClaimNoteByID:(NSUInteger)claimNoteID{
    for (int i = 0; i < self.claimNoteArray.count; i++){
        ClaimNote *claimNote = [self getClaimNoteByIndex:i];
        if (claimNote.claimNoteID == claimNoteID){
            return claimNote;
        }
    }
    return nil;
}

- (void)addClaimNote:(ClaimNote *) claimNote{
    [self.claimNoteArray addObject:claimNote];
}

- (void)addNewClaimNote:(ClaimNote *) claimNote{
    [self.claimNoteArray insertObject:claimNote atIndex:0];
}

- (void)clear{
    [self.claimNoteArray removeAllObjects];
}

- (void) encodeWithCoder:(NSCoder *)encoder{
    //[encoder encodeInt64:self.claimID forKey:@"claimID"];
    [encoder encodeObject:self.claimNoteArray forKey:@"claimNoteArray"];
}

- (id) initWithCoder:(NSCoder *)decoder{
    self = [super init];
    if (self) {
        //self.claimID = [decoder decodeInt64ForKey:@"claimID"];
        self.claimNoteArray = [decoder decodeObjectForKey:@"claimNoteArray"];
        if (!self.claimNoteArray) {
            self.claimNoteArray = [[NSMutableArray alloc] init];
        }
    }
    return self;
}

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id *)buffer count:(NSUInteger)len{
    return [self.claimNoteArray countByEnumeratingWithState:state objects:buffer count:len];
}

- (NSInteger) needToSyncClaimNotesCount{
    NSInteger c = 0;
    for (ClaimNote *claimNote in self.claimNoteArray)
    {
        if (claimNote.edited == TRUE) { c++; }
    }
    return c;
}

@end
