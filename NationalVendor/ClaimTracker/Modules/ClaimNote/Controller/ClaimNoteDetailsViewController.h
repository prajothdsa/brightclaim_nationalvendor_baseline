//
//  ClaimNoteDetailsViewController.h
//  ClaimTracker
//
//  Created by Sadhana Sundaresh on 8/21/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Claim.h"

@interface ClaimNoteDetailsViewController : UIViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *claimDetailTxtView;
@property (weak, nonatomic)NSString *claimNotesDetails;
@property (strong, nonatomic)NSString *claimNotesDateNameStatus;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (nonatomic, assign) BOOL isEdit;
@property Claim *claim;
@property (weak, nonatomic) IBOutlet UILabel *noteLabel;
@property (strong, nonatomic) UIView *spinner;
@end
