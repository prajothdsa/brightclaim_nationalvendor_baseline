//
//  ClaimNoteDetailsViewController.m
//  ClaimTracker
//
//  Created by Sadhana Sundaresh on 8/21/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import "ClaimNoteDetailsViewController.h"
#import "DataSource.h"
#import "UserData.h"
#import "Constants.h"

@interface ClaimNoteDetailsViewController ()

@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
//@property (weak, nonatomic) IBOutlet UITextView *textView;
//@property (weak, nonatomic) IBOutlet UIButton *done;


@end

@implementation ClaimNoteDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _claimDetailTxtView.textContainerInset = UIEdgeInsetsMake(10, 10, 0, 10);
    
    if(_isEdit == YES){
        _doneBtn.hidden = FALSE;
        [_claimDetailTxtView becomeFirstResponder];
        _claimDetailTxtView.text = @"";
        _claimDetailTxtView.editable = YES;
        _noteLabel.text = kNewNote;
    } else{
        _doneBtn.hidden = TRUE;
        _claimDetailTxtView.editable = NO;
        _noteLabel.text = kViewNote;
        
        UIFont *openSansFont = [UIFont fontWithName:@"Helvetica" size:13.0];
        NSDictionary *openSansDict = [NSDictionary dictionaryWithObject: openSansFont forKey:NSFontAttributeName];
        NSMutableAttributedString *noteInfoAttrString = [[NSMutableAttributedString alloc] initWithString:_claimNotesDateNameStatus attributes: openSansDict];
        [noteInfoAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:162.0/255 green:164.0/255 blue:169.0/255 alpha:1.0] range:(NSMakeRange(0, _claimNotesDateNameStatus.length))];
        
        UIFont *openSansSemiboldFont = [UIFont fontWithName:@"Helvetica" size:15.0];
        NSDictionary *verdanaDict = [NSDictionary dictionaryWithObject:openSansSemiboldFont forKey:NSFontAttributeName];
        NSString *claimNoteDetailsStr = [NSString stringWithFormat:@"%@",_claimNotesDetails];
        NSMutableAttributedString *noteDetailAttrString = [[NSMutableAttributedString alloc]initWithString:claimNoteDetailsStr attributes:verdanaDict];
        [noteDetailAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:(47.0/255.0) green:(47.0/255.0) blue:(47.0/255.0) alpha:1.0] range:(NSMakeRange(0, claimNoteDetailsStr.length))];
        
        NSAttributedString *atrStr = [[NSAttributedString alloc]initWithString:@"\n\n" attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:8]}];
        [noteInfoAttrString appendAttributedString:atrStr];
        [noteInfoAttrString appendAttributedString:noteDetailAttrString];
        _claimDetailTxtView.attributedText = noteInfoAttrString;
    }
    [[UserData data] removeSpinner:_spinner];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)doneBtnAction:(id)sender {
    [_claimDetailTxtView resignFirstResponder];
    NSCharacterSet *set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789.?&,$@"]invertedSet];
    UIView *spinner = [[UserData data] makeSpinner];
    NSArray *splitText = [_claimDetailTxtView.text componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    splitText = [splitText filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
	NSString *finalNoteString = [splitText componentsJoinedByString:@" "];
	finalNoteString = [finalNoteString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (finalNoteString.length == 0) {
        [[UserData data] removeSpinner:spinner];
        [self showAlertControllerWithTitle:kError andMessage:kEnterNote andBtnTitle:kClose];
        return;
    }
    
    if(finalNoteString.length > 255){
        [[UserData data] removeSpinner:spinner];
        [self showAlertControllerWithTitle:kError andMessage:kExceedLimit andBtnTitle:kClose];
    }
    
    if([finalNoteString rangeOfCharacterFromSet:set].location != NSNotFound){
        [[UserData data] removeSpinner:spinner];
        [self showAlertControllerWithTitle:kError andMessage:@"Please remove special characters from the note." andBtnTitle:kClose];
    }
    else{
//        DataSource * d = [DataSource new];
//        if (!d.isConnected)
//        {
//            [[UserData data] removeSpinner:spinner];
//            [self showAlertControllerWithTitle:kError andMessage:kNetworkConnectionErrorMsg andBtnTitle:kOK];
//        } else{
//            [_claimDetailTxtView resignFirstResponder];
//            [d ctUploadNotes:_claim andNoteText:finalNoteString async:YES addIt:nil withCompletion:^()
//             {
//                 dispatch_async(dispatch_get_main_queue(), ^{
//                     
//                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//                     [defaults setBool:YES forKey:@"isNoteAdded"];
//                     [[UserData data] removeSpinner:spinner];
//                     [self.navigationController popViewControllerAnimated:YES];
//                 });
//             }];
//        }
        ClaimNote *claimNote = [[ClaimNote alloc] init];
        claimNote.note = finalNoteString;
        claimNote.claimID = _claim.claimID;
        claimNote.repName = [UserData data].repName;
        claimNote.date = [NSDate date];
        claimNote.status = @"New";
        claimNote.edited = YES;
        [_claim.claimNoteList addNewClaimNote:claimNote];
        [[UserData data] removeSpinner:spinner];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)showAlertControllerWithTitle:(NSString*)alertTitle andMessage:(NSString*)alertMsg andBtnTitle:(NSString*)btnTitle {
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:alertTitle message:alertMsg preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *closeButton = [UIAlertAction actionWithTitle:btnTitle style:UIAlertActionStyleCancel handler: ^(UIAlertAction * action) {
        [_claimDetailTxtView becomeFirstResponder];
    }];
    [closeButton setValue:kBCOrangeColor forKey:@"titleTextColor"];
    [alertView addAction:closeButton];
    [self presentViewController: alertView animated:YES completion:nil];
//    alertView.view.tintColor = kBCOrangeColor;
}

//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
//    if ([text isEqualToString:@"\n"]) {
//        [_claimDetailTxtView resignFirstResponder];
//        return NO;
//    }
//    return YES;
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//- (IBAction)done:(id)sender {
//    _textView.editable = YES;
//    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"error" message:@"msg" preferredStyle: UIAlertControllerStyleAlert];
//    UIAlertAction *closeButton = [UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleCancel handler: ^(UIAlertAction * action) {
//    }];
//    [alertView addAction:closeButton];
//    [self presentViewController: alertView animated:YES completion:nil];
//    
//}


@end
