//
//  ClaimNotesListViewController.h
//  ClaimTracker
//
//  Created by Sadhana Sundaresh on 8/21/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Claim.h"

@interface ClaimNotesListViewController : UIViewController
@property Claim *claim;
@property (weak, nonatomic) IBOutlet UITableView *tblClaimNotesList;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property ClaimNote *selectedClaimNote;
@property (weak, nonatomic) IBOutlet UIButton *addNewNoteBtn;
@property Claim *selectedClaim;


@end
