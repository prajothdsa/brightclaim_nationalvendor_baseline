//
//  ClaimNotesController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/6/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "ClaimNotesController.h"
#import "CTTabBarController.h"
#import "ClaimNoteController.h"
#import "UserData.h"
#import "CTNavigationController.h"
#import "DataSource.h"
#import "Constants.h"

@interface ClaimNotesController (){
UIRefreshControl *refreshController;
    
}
@end

@implementation ClaimNotesController

- (void)doRefresh{
    @try {
        [self.claim refreshClaimNotesWithCompletion:^(void){
            [self.claimNotesTableView reloadData];
        }];
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (void)viewDidLoad {
    @try {
        [super viewDidLoad];
        CTTabBarController *source = (CTTabBarController *)self.parentViewController.parentViewController;
        self.claim = source.selectedClaim;
        [self doRefresh];
        refreshController = [[UIRefreshControl alloc] init];
        [refreshController addTarget:self action:@selector(doRefresh) forControlEvents:UIControlEventValueChanged];
        refreshController.backgroundColor = [UIColor colorWithRed:245/255.0f green:247/255.0f blue:248/255.0f alpha:1.0];
        _claimNotesTableView.backgroundView = refreshController;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

-(void)viewWillAppear:(BOOL)animated {
    UIView *spinner = [[UserData data] makeSpinner];
    DataSource * d = [DataSource new];
    if (!d.isConnected) {
        [self showAlertControllerWithTitle:@"Error" andMessage:kNetworkConnectionErrorMsg andBtnTitle:@"OK"];
    }
    else {
        [[UserData data] refreshClaimsWithCompletion:^(void){
            [self.claimNotesTableView reloadData];
            [[UserData data] removeSpinner:spinner];
            [refreshController endRefreshing];
        }];
    }
    
}

- (void)showAlertControllerWithTitle:(NSString*)alertTitle andMessage:(NSString*)alertMsg andBtnTitle:(NSString*)btnTitle {
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:alertTitle message:alertMsg preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *closeButton = [UIAlertAction actionWithTitle:btnTitle style:UIAlertActionStyleCancel handler: ^(UIAlertAction * action) {
        
    }];
    alertView.view.tintColor = kBCOrangeColor;
    [alertView addAction:closeButton];
    [self presentViewController: alertView animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    @try {
        if ([segue.identifier isEqualToString:@"segueClaimNoteListToClaimNote"]){
            NSInteger row = [self.claimNotesTableView indexPathForSelectedRow].row;
            CTNavigationController *destination = segue.destinationViewController;
            
            self.selectedClaimNote = [self.claim.claimNoteList getClaimNoteByIndex:row];
            destination.selectedClaimNote = self.selectedClaimNote;
        }
        //else if ([segue.identifier isEqualToString:@"segueToClaimList"]) {
        //    UITabBarController *destination = segue.destinationViewController;
        //    [destination setSelectedIndex:1];
        //}
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}
- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)unwindToClaimNotes:(UIStoryboardSegue *)segue {
    //ClaimNoteController *source = [segue sourceViewController];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    @try {
        // Return the number of sections.
        if ([self.claim.claimNoteList count] == 0){
            UILabel *lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(0,0,self.view.bounds.size.width, self.view.bounds.size.height)];
            lblMessage.text = kNoDataMsg;
            lblMessage.textColor = [UIColor blackColor];
            lblMessage.numberOfLines = 0;
            lblMessage.textAlignment = NSTextAlignmentCenter;
            lblMessage.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
            [lblMessage sizeToFit];
            self.claimNotesTableView.backgroundView = lblMessage;
            self.claimNotesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 0;
        }
        else {
            self.claimNotesTableView.backgroundView = nil;
            self.claimNotesTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            return 1;
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.claim.claimNoteList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PrototypeCell" forIndexPath:indexPath];
        
        ClaimNote *claimNote = [self.claim.claimNoteList getClaimNoteByIndex:indexPath.row];
        
        UILabel *label;
        label = (UILabel *)[cell viewWithTag:1];
        label.text = [NSString stringWithFormat:@"%@", claimNote.note];

        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MM/dd/yyyy"];
        label = (UILabel *)[cell viewWithTag:2];
        label.text = [NSString stringWithFormat:@"Entered %@ By %@ for status %@", [df stringFromDate:claimNote.date], claimNote.repName, claimNote.status];

        return cell;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

#pragma mark - Handle Refresh Method
-(void)handleRefresh: (id)sender
{
    @try{
    NSLog (@"Pull To Refresh Method Called");
            [[UserData data] refreshClaimsWithCompletion:^(void){
            [self.claimNotesTableView reloadData];
            [refreshController endRefreshing];
        }];
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
        }
    
}

@end
