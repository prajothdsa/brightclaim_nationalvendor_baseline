//
//  ClaimNotesListViewController.m
//  ClaimTracker
//
//  Created by Sadhana Sundaresh on 8/21/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import "ClaimNotesListViewController.h"
#import "DataSource.h"
#import "Constants.h"
#import "UserData.h"
#import "ClaimNoteDetailsViewController.h"
#import "ClaimNotesTableViewCell.h"

@interface ClaimNotesListViewController () {
    UIRefreshControl *refreshControl;
    UIView *spinner;
    NSInteger unsyncedNotes;

}

@end

@implementation ClaimNotesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    spinner = [[UserData data] makeSpinner];
//    [self doRefresh];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(doRefresh) forControlEvents:UIControlEventValueChanged];
    refreshControl.backgroundColor = [UIColor colorWithRed:245/255.0f green:247/255.0f blue:248/255.0f alpha:1.0];
    [_tblClaimNotesList addSubview: refreshControl];
}

-(void)viewWillAppear:(BOOL)animated {
    _tblClaimNotesList.userInteractionEnabled = YES;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    unsyncedNotes = [[UserData data] unsyncedNotesForClaimID:_claim.claimID];
    if (unsyncedNotes > 0) {
        [[UserData data] removeSpinner:spinner];
        [self.tblClaimNotesList reloadData];
    }
    else {
        [self doRefresh];
    }
//    UIView *loading = [[UserData data] makeSpinner];
//    DataSource * d = [DataSource new];
//    if (!d.isConnected) {
//        [[UserData data] removeSpinner:loading];
//        [self showAlertControllerWithTitle:@"Error" andMessage:kNetworkConnectionErrorMsg andBtnTitle:kOK];
//    }
//    else {
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        if([defaults boolForKey:@"isNoteAdded"] == YES){
//            [self doRefresh];
//            [[UserData data] removeSpinner:loading];
//            [defaults setBool:NO forKey:@"isNoteAdded"];
//        }
//        else{
//             [self.tblClaimNotesList reloadData];
//            [[UserData data] removeSpinner:loading];
//        }
//        
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)doRefresh{
    if (unsyncedNotes>0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:kWarning message:[NSString stringWithFormat:@"You have %ld item(s) to be uploaded. You would lose these changes if you refresh your data. Please sync first", (long)unsyncedNotes] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kOK style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            [refreshControl endRefreshing];
        }];
        [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:TRUE completion:nil];
    }
    else {
        DataSource * d = [DataSource new];
        if (!d.isConnected) {
            [[UserData data] removeSpinner:spinner];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:kError message:kNetworkConnectionErrorMsg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kOK style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                [refreshControl endRefreshing];
                [_tblClaimNotesList reloadData];
            }];
            [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:TRUE completion:nil];
        } else {
            [self.claim refreshClaimNotesWithCompletion:^(void){
                [[UserData data] removeSpinner:spinner];
                _tblClaimNotesList.tag = 1;
                [_tblClaimNotesList reloadData];
                [refreshControl endRefreshing];
            }];
        }
    }
}

- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.claim.claimNoteList count] == 0 && _tblClaimNotesList.tag == 1){
        UILabel *lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(0,0,self.view.bounds.size.width, self.view.bounds.size.height)];
        lblMessage.text = kNoDataMsg;
        lblMessage.textColor = [UIColor blackColor];
        lblMessage.numberOfLines = 0;
        lblMessage.textAlignment = NSTextAlignmentCenter;
        lblMessage.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
        [lblMessage sizeToFit];
        _tblClaimNotesList.backgroundView = lblMessage;
        return 0;
    }
    else {
        _tblClaimNotesList.backgroundView = nil;
        return [self.claim.claimNoteList count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ClaimNotesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PrototypeCell" forIndexPath:indexPath];
    ClaimNote *claimNote = [self.claim.claimNoteList getClaimNoteByIndex:indexPath.row];
    
    if (claimNote.edited) {
        cell.lblAdded.hidden = NO;
    } else {
        cell.lblAdded.hidden = YES;
    }
    
    UILabel *label;
    
    UIView *contentView;
    
    contentView = (UIView *)[cell viewWithTag:4];
    
    contentView.layer.shadowColor = [UIColor colorWithRed:211.0/255.0 green:211.0/255.0 blue:212.0/255.0 alpha:1].CGColor;
    contentView.layer.shadowOffset = CGSizeMake(2, 2);
    contentView.layer.shadowOpacity = 0.5;
    contentView.layer.shadowRadius = 0.1;
    
    label = (UILabel *)[cell viewWithTag:1];
    label.text = [NSString stringWithFormat:@"%@", claimNote.note];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM/dd/yyyy"];
    label = (UILabel *)[cell viewWithTag:2];
    label.text = [NSString stringWithFormat:@"Entered %@ By %@ for status %@", [df stringFromDate:claimNote.date], claimNote.repName, claimNote.status];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.userInteractionEnabled = NO;
    UIView *spinnerView = [[UserData data] makeSpinner];
    UIStoryboard *claimNoteStoryboard = [UIStoryboard storyboardWithName:kClaimNotesStoryboard bundle: nil];
    ClaimNoteDetailsViewController *claimsNotesDetailsVC = (ClaimNoteDetailsViewController*)[claimNoteStoryboard instantiateViewControllerWithIdentifier:kClaimNotesDetailsVC];
    ClaimNote *claimNote = [self.claim.claimNoteList getClaimNoteByIndex:indexPath.row];
    claimsNotesDetailsVC.claimNotesDetails =  claimNote.note;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM/dd/yyyy"];
    NSString *notesLabel = [NSString stringWithFormat:@"Entered %@ By %@ for status %@", [df stringFromDate:claimNote.date], claimNote.repName, claimNote.status];
    claimsNotesDetailsVC.claimNotesDateNameStatus = notesLabel;
    claimsNotesDetailsVC.isEdit = FALSE;
    claimsNotesDetailsVC.spinner = spinnerView;
//    dispatch_async(dispatch_get_main_queue(), ^{
    [self.navigationController pushViewController:claimsNotesDetailsVC animated:YES];
//    });
}

- (IBAction)addNewNoteAction:(id)sender {
    UIView *spinnerView = [[UserData data] makeSpinner];
    UIStoryboard *claimNoteStoryboard = [UIStoryboard storyboardWithName:kClaimNotesStoryboard bundle: nil];
    ClaimNoteDetailsViewController *claimsNotesDetailsVC = (ClaimNoteDetailsViewController*)[claimNoteStoryboard instantiateViewControllerWithIdentifier:/*@"claimNotesDetails"*/kClaimNotesDetailsVC];
    claimsNotesDetailsVC.isEdit = TRUE;
    claimsNotesDetailsVC.claim = _claim;
    claimsNotesDetailsVC.spinner = spinnerView;
    [self.navigationController pushViewController:claimsNotesDetailsVC animated:YES];
}


- (void)showAlertControllerWithTitle:(NSString*)alertTitle andMessage:(NSString*)alertMsg andBtnTitle:(NSString*)btnTitle {
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:alertTitle message:alertMsg preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *closeButton = [UIAlertAction actionWithTitle:btnTitle style:UIAlertActionStyleCancel handler: ^(UIAlertAction * action) {
        
    }];
    [closeButton setValue:kBCOrangeColor forKey:@"titleTextColor"];
    [alertView addAction:closeButton];
    [self presentViewController: alertView animated:YES completion:nil];
}

@end
