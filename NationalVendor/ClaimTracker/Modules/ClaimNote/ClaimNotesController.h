//
//  ClaimNotesController.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/6/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClaimNote.h"
#import "Claim.h"


@interface ClaimNotesController : UIViewController

@property Claim *claim;
@property ClaimNote *selectedClaimNote;
@property (weak, nonatomic) IBOutlet UITableView *claimNotesTableView;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end
