//
//  ClaimsListViewController.m
//  ClaimTracker
//
//  Created by Prajoth Antonio D on 10/08/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import "ClaimsListViewController.h"
#import "LoginController.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "UserData.h"
#import "ClaimInfoViewController.h"
#import "CTTabBarController.h"
#import "DataSource.h"
#import "InventoryListController.h"

@interface ClaimsListViewController (){
    UIRefreshControl *refreshController;
    NSInteger syncCount;
    NSMutableArray *arr;
    NSMutableArray *editedItems;
    UIView *spinner;
}
@property (weak, nonatomic) IBOutlet UIButton *mPhoneLabelButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mTopView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *syncView;
@property (weak, nonatomic) IBOutlet UILabel *mNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mNameValue;
@property (weak, nonatomic) IBOutlet UILabel *mPhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *mEmailLabel;
@property (weak, nonatomic) IBOutlet UIButton *mInfoButton;
@property (weak, nonatomic) IBOutlet UILabel *mVendorDesigntionLabel;
@property (weak, nonatomic) IBOutlet UIButton *mLogoutButton;
@property (weak, nonatomic) IBOutlet UILabel *mMyClaimLabel;
@property (weak, nonatomic) IBOutlet UIButton *mProfileButton;
@property (weak, nonatomic) IBOutlet UIButton *mSyncButton;
@property (weak, nonatomic) IBOutlet UITableView *mTableView;
@property (weak, nonatomic) IBOutlet UIButton *mEmailButton;
@property (weak, nonatomic) IBOutlet UILabel *mEditedLabel;
@property (weak, nonatomic) IBOutlet UIView *mMyClaimsView;
@property (weak, nonatomic) IBOutlet UILabel *lblChangesToSync;
@end

@implementation ClaimsListViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    spinner = [[UserData data] makeSpinner]; 
    [[UserData data] loadClaims];
    refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    refreshController.backgroundColor = [UIColor colorWithRed:245/255.0f green:247/255.0f blue:248/255.0f alpha:1.0];
    [_tblClaims addSubview:refreshController];
    self.navigationController.navigationBar.hidden = YES;
    
    _mMyClaimsView.layer.shadowColor = [UIColor colorWithRed:211.0/255.0 green:211.0/255.0 blue:212.0/255.0 alpha:1].CGColor;
    _mMyClaimsView.layer.shadowOffset = CGSizeMake(2, 2);
    _mMyClaimsView.layer.shadowOpacity = 0.5;
    _mMyClaimsView.layer.shadowRadius = 0.1;
    _mTopView.constant = 80;
    _syncView.constant = 0;
    _lblLastSync.hidden = YES; //NEED_FOR_LAUNCH

    [_mLogoutButton.layer setBorderWidth:1.0];
    _mLogoutButton.layer.cornerRadius = 0.5;
    [_mLogoutButton.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    [_mSyncButton.layer setBorderWidth:1.0];
    _mSyncButton.layer.cornerRadius = 1;
    _mSyncButton.layer.borderColor = [[UIColor colorWithRed:210.0/255 green:62.0/255 blue:2.0/255 alpha:1]CGColor];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(doneSyncing) name:@"SyncToServerDone" object:nil];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    //NEED_FOR_LAUNCH
    NSString *savedLastSyncDateTime = [[NSUserDefaults standardUserDefaults] objectForKey:@"LastSyncedDateTime"];
    NSString *lastSyncDateTimeString = [NSString stringWithFormat:@"Last Sync: %@",savedLastSyncDateTime];
    _lblLastSync.text = lastSyncDateTimeString;

    syncCount = [[UserData data] needToSyncCount];
    if (syncCount > 0) {
        [[UserData data] loadConditions];
        [[UserData data] removeSpinner:spinner];
        _syncView.constant = 103;
        //NEED_FOR_LAUNCH
        if (savedLastSyncDateTime.length) {
            _lblLastSync.hidden = NO;
        } else {
            _lblLastSync.hidden = YES;
        }
        _lblChangesToSync.text = [NSString stringWithFormat:@"You have %ld item(s) to be uploaded",(long)syncCount];
        [self.mTableView reloadData];
        arr = [[UserData data] getSyncItems];
        editedItems = [[NSMutableArray alloc] init];
        for (int i =0; i<[arr count]; i++) {
            if ([[arr objectAtIndex:i] isKindOfClass:[Inventory class]]){
                Inventory *inventory = (Inventory *)[arr objectAtIndex:i];
                [editedItems addObject:[NSNumber numberWithUnsignedInteger:inventory.claimID]];
            }
            else if ([[arr objectAtIndex:i] isKindOfClass:[Photo class]]){
                Photo *photo = (Photo *)[arr objectAtIndex:i];
                [editedItems addObject:[NSNumber numberWithUnsignedInteger:photo.claimID]];
            }
            //NEED_FOR_LAUNCH.
            else if([[arr objectAtIndex:i] isKindOfClass:[ClaimNote class]]){
                ClaimNote *claimNote = (ClaimNote *)[arr objectAtIndex:i];
                [editedItems addObject:[NSNumber numberWithUnsignedInteger:claimNote.claimID]];
            }
        }
    }
    else {
        _syncView.constant = 0;
        _lblLastSync.hidden = YES; //NEED_FOR_LAUNCH
        [editedItems removeAllObjects];
//        spinner = [[UserData data] makeSpinner];
        DataSource * d = [DataSource new];
        if (!d.isConnected) {
            [[UserData data] loadConditions];
            [[UserData data] removeSpinner:spinner];
            if (!_isOfflineLogin) {
                [self showAlertControllerWithTitle:@"Error" andMessage:kNetworkConnectionErrorMsg andBtnTitle:kOK];
            }
        }
        else {
            [[UserData data] refreshConditions];
            [[UserData data] refreshClaimsWithCompletion:^(void){
                [self.mTableView reloadData];
                [[UserData data] removeSpinner:spinner];
                [refreshController endRefreshing];
            }];
        }
    }
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString * versionBuildString = [NSString stringWithFormat:@"v%@ (%@)", appVersionString, appBuildString];
    _lblBuildInfo.text = [NSString stringWithFormat:@"%@",versionBuildString];
}

- (void)refreshData
{
    @try {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL testMode = [defaults boolForKey:@"testMode"];
        if (testMode)
            self.mNameValue.text = [UserData data].repName;
        [self.mEmailButton setTitle:[UserData data].repEMail forState:UIControlStateNormal];
        [self.mPhoneLabelButton setTitle:[UserData data].repPhone forState:UIControlStateNormal];
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:@"Error" andMessage:@"Error loading data" andBtnTitle:@"OK"];
    } @finally {
    }
}
- (IBAction)syncButtonTapped:(id)sender {
    DataSource * d = [DataSource new];
    if (!d.isConnected) {
        [self showAlertControllerWithTitle:@"Error" andMessage:kNetworkConnectionErrorMsg andBtnTitle:kOK];
    }
    else {
//        UIView *spinner = [[UserData data] makeSpinner];
        [[UserData data] syncToServerWithTableView:_tblClaims andArray:arr];
//        [self.mTableView reloadData];
//        [refreshController endRefreshing];
//        [[UserData data] removeSpinner:spinner];
//        [editedItems removeAllObjects];
//        _syncView.constant = 0;
    }
}

- (void)doneSyncing {
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    [[NSUserDefaults standardUserDefaults] setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"LastSyncedDateTime"];
    //    [self.mTableView reloadData];
    [refreshController endRefreshing];
    //    [[UserData data] removeSpinner:spinner];
    [editedItems removeAllObjects];
    _syncView.constant = 0;
    _lblLastSync.hidden = YES; //NEED_FOR_LAUNCH
}

- (IBAction)logOutButton:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm Logout" message:@"Are you sure you wish to logout?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       // No action
                                   }];
    
    UIAlertAction *logoutAction = [UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       [UserData data].isAuthenticated = false;
                                       [[UserData data] saveClaims];
                                       AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                                       UIViewController *rootController = [[UIStoryboard storyboardWithName:@"Login" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"LoginScreen"];
                                       UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:rootController];
                                       appDelegate.window.rootViewController = navigation;
                                       
                                   }];
    [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
    [logoutAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
    [alert addAction:cancelAction];
    [alert addAction:logoutAction];
    [self presentViewController:alert animated:TRUE completion:nil];
//    alert.view.tintColor = kBCOrangeColor;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[UserData data].claims count];
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    if ([[UserData data].claims count] == 0){
//        UILabel *lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(0,0,self.view.bounds.size.width, self.view.bounds.size.height)];
//        lblMessage.text = @"No data available";
//        lblMessage.textColor = [UIColor blackColor];
//        lblMessage.numberOfLines = 0;
//        lblMessage.textAlignment = NSTextAlignmentCenter;
//        lblMessage.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
//        [lblMessage sizeToFit];
//        self.mTableView.backgroundView = lblMessage;
//        self.mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        return 0;
//    }
//    else {
//        self.mTableView.backgroundView = nil;
//        return 1;
//    }
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PrototypeCell" forIndexPath:indexPath];
    Claim *claim = [[UserData data].claims getClaimByIndex:indexPath.row];
    UILabel *label; UIView *contentView;
    
    contentView = (UIView *)[cell viewWithTag:4];
    contentView.layer.shadowColor = [UIColor colorWithRed:211.0/255.0 green:211.0/255.0 blue:212.0/255.0 alpha:1].CGColor;
    contentView.layer.shadowOffset = CGSizeMake(2, 2);
    contentView.layer.shadowOpacity = 0.5;
    contentView.layer.shadowRadius = 0.1;

    label = (UILabel *)[cell viewWithTag:1];
    label.text = [NSString stringWithFormat:@"%@", claim.claimName];
    
    label = (UILabel *)[cell viewWithTag:2];
    NSString *addrLine1 = [NSString stringWithFormat:@"%@ %@", claim.insuredAddress1,claim.insuredAddress2];
    label.text = ([addrLine1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length>0) ? addrLine1 : @"NA";
    
    label = (UILabel *)[cell viewWithTag:5];
    NSString *addrLine2 = [NSString stringWithFormat:@"%@ %@ %@",claim.insuredCity,claim.insuredState,claim.insuredZip];
    label.text = ([addrLine2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length>0) ? addrLine2 : @"NA";
    
    label = (UILabel *)[cell viewWithTag:3];
    label.text = @"";
    if ([editedItems containsObject:[NSNumber numberWithUnsignedInteger:claim.claimID]]) {
        label.text = @"Edited";
        label.textColor = [UIColor colorWithRed:255.0/255.0 green:170.0/255.0 blue:131.0/255.0 alpha:1.0];
    } else {
        label.text = @"";
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:kClaimInfoStoryboard bundle: nil];
    ClaimInfoViewController *claimsInfoVC = (ClaimInfoViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:kClaimInfoVC];
    claimsInfoVC.selectedClaim = [[UserData data].claims getClaimByIndex:indexPath.row];
    [self.navigationController pushViewController:claimsInfoVC animated:YES];
    _mTopView.constant = 80;
    _mNameLabel.hidden = true;
    _mNameValue.hidden = true;
    _mPhoneLabel.hidden = true;
    _mPhoneLabelButton.hidden = true;
    _mEmailLabel.hidden = true;
    _mEmailButton.hidden = true;
    _mInfoButton.hidden = true;
    _mVendorDesigntionLabel.hidden = true;
    _mLogoutButton.hidden = true;
    _mMyClaimLabel.hidden = false;
    _mProfileButton.hidden = false;
    _lblBuildInfo.hidden = true;
}

#pragma mark - Handle Refresh Method
-(void)handleRefresh : (id)sender
{
    NSLog (@"Pull To Refresh Method Called");
    syncCount = [[UserData data] needToSyncCount];
    if (syncCount>0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:kWarning message:[NSString stringWithFormat:@"You have %ld item(s) to be uploaded. You would lose these changes if you refresh your data. Please sync first", (long)syncCount] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kOK style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            [refreshController endRefreshing];
        }];
         [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:TRUE completion:nil];
    }
    else {
        DataSource * d = [DataSource new];
        if (!d.isConnected) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:kError message:kNetworkConnectionErrorMsg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kOK style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                [refreshController endRefreshing];
                [self.mTableView reloadData];
            }];
             [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:TRUE completion:nil];
        } else {
            [[UserData data] refreshClaimsWithCompletion:^(void){
                [self.mTableView reloadData];
                [refreshController endRefreshing];
            }];
        }
    }
}

- (IBAction)mInfoButton:(id)sender {
    _lblBuildInfo.hidden = true;
    _mTopView.constant = 80;
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^ {
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         _mNameLabel.hidden = true;
                         _mNameValue.hidden = true;
                         _mPhoneLabel.hidden = true;
                         _mPhoneLabelButton.hidden = true;
                         _mEmailLabel.hidden = true;
                         _mEmailButton.hidden = true;
                         _mInfoButton.hidden = true;
                         _mVendorDesigntionLabel.hidden = true;
                         _mLogoutButton.hidden = true;
                         _mMyClaimLabel.hidden = false;
                         _mProfileButton.hidden = false;
                     }];
}

- (IBAction)myClainProfileButton:(id)sender {
    _mTopView.constant = 220;
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^ {
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) { }];
    _mNameLabel.hidden = false;
    _mNameValue.hidden = false;
    _mPhoneLabel.hidden = false;
    _mPhoneLabelButton.hidden = false;
    _mEmailLabel.hidden = false;
    _mEmailButton.hidden = false;
    _mInfoButton.hidden = false;
    _mVendorDesigntionLabel.hidden = false;
    _mLogoutButton.hidden = false;
    _mMyClaimLabel.hidden = true;
    _mProfileButton.hidden = true;
    _lblBuildInfo.hidden = false;
    [self refreshData];
}

- (void)showAlertControllerWithTitle:(NSString*)alertTitle andMessage:(NSString*)alertMsg andBtnTitle:(NSString*)btnTitle {
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:alertTitle message:alertMsg preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *closeButton = [UIAlertAction actionWithTitle:btnTitle style:UIAlertActionStyleCancel handler: ^(UIAlertAction * action) {
        
    }];
    [closeButton setValue:kBCOrangeColor forKey:@"titleTextColor"];
    [alertView addAction:closeButton];
    [self presentViewController: alertView animated:YES completion:nil];
}

@end
