//
//  ViewUserDataController.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/1/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSKeyChain.h"
#import "UserData.h"

@interface MyClaimsController : UITableViewController

@end
