//
//  ClaimsListViewController.h
//  ClaimTracker
//
//  Created by Prajoth Antonio D on 10/08/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClaimsListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblClaims;
@property (nonatomic, assign) BOOL isOfflineLogin;
@property (weak, nonatomic) IBOutlet UILabel *lblBuildInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblLastSync;
@end
