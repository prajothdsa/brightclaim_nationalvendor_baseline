//
//  MyClaimsController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/1/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyClaimsController.h"
#import "CTTabBarController.h"
#import "Constants.h"

@interface MyClaimsController ()

- (IBAction)addClaim:(UIBarButtonItem *)sender;
- (IBAction)syncToServer:(UIBarButtonItem *)sender;

@end

@implementation MyClaimsController

- (void)doRefresh{
    @try {
        NSInteger syncCount = [[UserData data] needToSyncCount];
        
        if (syncCount > 0)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:[NSString stringWithFormat:@"You have %ld item(s) to be uploaded. You would lose these changes if you refresh your data.  Please sync first.", (long)syncCount] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                if (self.refreshControl) [self.refreshControl endRefreshing];
            }];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:TRUE completion:nil];
        }
        else
        {
            [[UserData data] refreshClaimsWithCompletion:^(void){
                [self.tableView reloadData];
                if (self.refreshControl) [self.refreshControl endRefreshing];
            }];
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (void)viewDidLoad {
    @try {
        [super viewDidLoad];
        [[UserData data] loadClaims];
        self.refreshControl = [[UIRefreshControl alloc] init];
        self.refreshControl.backgroundColor = [UIColor blueColor];
        self.refreshControl.tintColor = [UIColor whiteColor];
        [self.refreshControl addTarget:self action:@selector(doRefresh) forControlEvents:UIControlEventValueChanged];
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"segueMyClaimDetail"]){
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        CTTabBarController *destination = segue.destinationViewController;
        
        destination.selectedClaim = [[UserData data].claims getClaimByIndex:indexPath.row];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    @try {
        // Return the number of sections.
        if ([[UserData data].claims count] == 0){
            UILabel *lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(0,0,self.view.bounds.size.width, self.view.bounds.size.height)];
            lblMessage.text = kNoDataMsg;
            lblMessage.textColor = [UIColor blackColor];
            lblMessage.numberOfLines = 0;
            lblMessage.textAlignment = NSTextAlignmentCenter;
            lblMessage.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
            [lblMessage sizeToFit];
            self.tableView.backgroundView = lblMessage;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 0;
        }
        else {
            self.tableView.backgroundView = nil;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            return 1;
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[UserData data].claims count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PrototypeCell" forIndexPath:indexPath];

        Claim *claim = [[UserData data].claims getClaimByIndex:indexPath.row];
        
        UILabel *label;
        label = (UILabel *)[cell viewWithTag:1];
        label.text = [NSString stringWithFormat:@"%@", claim.claimName];
        
        label = (UILabel *)[cell viewWithTag:2];
        label.text = [NSString stringWithFormat:@"%@", claim.insuredFullAddress];
        
        return cell;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}


- (IBAction)addClaim:(UIBarButtonItem *)sender {
}

- (IBAction)syncToServer:(UIBarButtonItem *)sender {
    @try {
        UIProgressView *progress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
        
        [self.tableView addSubview:progress];
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (IBAction)unwindToList:(UIStoryboardSegue *)segue {

}
@end
