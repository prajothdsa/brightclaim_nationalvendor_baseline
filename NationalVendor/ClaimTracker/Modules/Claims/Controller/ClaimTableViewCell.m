//
//  ClaimTableViewCell.m
//  ClaimTracker
//
//  Created by Varun Shukla on 8/10/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import "ClaimTableViewCell.h"

@implementation ClaimTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
