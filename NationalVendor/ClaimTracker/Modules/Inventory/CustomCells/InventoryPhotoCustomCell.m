//
//  InventoryPhotoCustomCell.m
//  ClaimTracker
//
//  Created by Prajoth Antonio D on 25/08/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import "InventoryPhotoCustomCell.h"
#import "Constants.h"

@implementation InventoryPhotoCustomCell

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    _txtPhotoTitle.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtPhotoTitle.keyboardType = UIKeyboardTypeASCIICapable;
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    self.photo.imageName = textField.text;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)btnDeleteAction:(UIButton *)sender {
    @try {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm Delete" message:@"Are you sure you want to delete this photo?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL];
        
        UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//            [self.inventory.photoList removePhoto:self.photo];
            [self.delegate deletePhoto:self.photo];
            [self.collectionView reloadData];
        }];
        [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
        [deleteAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
        [alert addAction:cancelAction];
        [alert addAction:deleteAction];
        [self.presenterViewController presentViewController:alert animated:TRUE completion:nil];
    } @catch (NSException *exception) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"exception.reason" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleCancel handler:NULL];
        [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
        [alert addAction:cancelAction];
        [self.presenterViewController presentViewController:alert animated:TRUE completion:nil];
    } @finally {
    }
}

@end
