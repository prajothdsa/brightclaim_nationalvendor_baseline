//
//  InventoryPhotoCustomCell.h
//  ClaimTracker
//
//  Created by Prajoth Antonio D on 25/08/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Photo.h"
#import "Inventory.h"

@protocol PhotoCellDelegate <NSObject>
- (void)deletePhoto:(Photo*)photoObj;
@end

@interface InventoryPhotoCustomCell : UICollectionViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewFirstCell;
@property (weak, nonatomic) IBOutlet UIView *viewOtherCells;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *btnDeletePhoto;
@property (weak, nonatomic) IBOutlet UITextField *txtPhotoTitle;
@property (weak, nonatomic) IBOutlet UIView *viewAddPhoto;
@property Photo *photo;
@property Inventory *inventory;
@property UICollectionView *collectionView;
@property UIViewController *presenterViewController;
@property (nonatomic, weak) id <PhotoCellDelegate> delegate;

- (IBAction)btnDeleteAction:(UIButton *)sender;

@end
