//
//  InventoryViewController.h
//  ClaimTracker
//
//  Created by Prajoth Antonio D on 21/08/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Claim.h"
#import "Inventory.h"
#import "JVFloatLabeledTextField.h"
#import "UserData.h"
#import "InventoryPhotoCustomCell.h"

@interface InventoryViewController : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,PhotoCellDelegate>
@property (weak, nonatomic) IBOutlet UIView *btnViewAddInventory;
@property (weak, nonatomic) IBOutlet UIView *btnViewEditInventory;
@property (weak, nonatomic) IBOutlet UIButton *btnViewMore;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tblInventoryInfo;
@property (weak, nonatomic) IBOutlet UIView *viewMoreView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewMoreViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblItemNo;
@property (weak, nonatomic) IBOutlet UILabel *lblClaimName;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *saveFinishButton;
@property (weak, nonatomic) IBOutlet UIButton *saveAddButton;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtQuantity;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtRoom;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtDescription;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtAgeY;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtAgeM;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtCondition;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtPurchasePrice;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtPurchaseLocation;
@property (assign, nonatomic) int itemNo;
@property (weak, nonatomic) IBOutlet UIButton *btnRoomPicker;
@property (weak, nonatomic) IBOutlet UICollectionView *photoCollectionView;
@property Claim *selectedClaim;
@property Inventory *selectedInventory;
@property BOOL editMode;
@property (weak, nonatomic) IBOutlet UITableView *tblPickerView;
@property (weak, nonatomic) IBOutlet UIButton *btnPickerCancel;
@property (weak, nonatomic) IBOutlet UIView *viewPicker;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

- (IBAction)btnViewMoreAction:(id)sender;
- (IBAction)btnRoomPickerAction:(id)sender;
- (IBAction)btnBackAction:(id)sender;
- (IBAction)btnPickerCancelAction:(id)sender;
@end
