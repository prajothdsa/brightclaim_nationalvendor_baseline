//
//  InventoryViewController.m
//  ClaimTracker
//
//  Created by Prajoth Antonio D on 21/08/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import "InventoryViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "Constants.h"
#import "PHAsset+Extended.h"
#import "PHAssetCollection+Extended.h"
#import "UIButton+Extended.h"
#import <Photos/Photos.h>

@interface InventoryViewController () {
    BOOL viewMore;
    UITextField *selectedTextfield;
    UIButton *btnReturn;
    NSMutableArray *photosArray;
}
@property NSArray *rooms;
@property NSArray *conditions;
@property (nonatomic) UIImagePickerController *imagePickerController;
@end

@implementation InventoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    photosArray = [[NSMutableArray alloc]init];
    if (_editMode) {
        for (Photo *p in _selectedInventory.photoList) {
            [photosArray addObject:p];
        }
    }
    self.rooms = [_selectedClaim getRoomList];
    _rooms = [_rooms filteredArrayUsingPredicate: [NSPredicate predicateWithFormat:@"length > 0"]];

    [self hidePicker:YES];
    _tblPickerView.layer.cornerRadius = 10;
    _btnPickerCancel.layer.cornerRadius = 10;
    [_btnPickerCancel setTitleColor:kBCOrangeColor forState:UIControlStateNormal];
    _viewMoreView.hidden = YES;
    _lblTitle.text = @"Inventory Info";
    
    _btnViewAddInventory.layer.shadowColor = [UIColor colorWithRed:211.0/255.0 green:211.0/255.0 blue:212.0/255.0 alpha:1].CGColor;
    _btnViewAddInventory.layer.shadowOffset = CGSizeMake(2, 2);
    _btnViewAddInventory.layer.shadowOpacity = 0.5;
    _btnViewAddInventory.layer.shadowRadius = 10.0f;
    [_saveAddButton setBackgroundImage:[_saveAddButton imageForButtonWithColor:kBCOrangeSelectedColor] forState:UIControlStateHighlighted];
    [_btnSave setBackgroundImage:[_btnSave imageForButtonWithColor:kBCOrangeSelectedColor] forState:UIControlStateHighlighted];

    _btnViewEditInventory.layer.shadowColor = [UIColor colorWithRed:211.0/255.0 green:211.0/255.0 blue:212.0/255.0 alpha:1].CGColor;
    _btnViewEditInventory.layer.shadowOffset = CGSizeMake(2, 2);
    _btnViewEditInventory.layer.shadowOpacity = 0.5;
    _btnViewEditInventory.layer.shadowRadius = 10.0f;

    [_saveFinishButton.layer setBorderWidth:1.0];
    _saveFinishButton.layer.cornerRadius = 0.5;
    [_saveFinishButton.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    [_saveFinishButton.layer setBorderWidth:1.0];
    _saveFinishButton.layer.cornerRadius = 1;
    _saveFinishButton.layer.borderColor = [[UIColor colorWithRed:210.0/255 green:62.0/255 blue:2.0/255 alpha:1]CGColor];

    _txtQuantity.text = [NSString stringWithFormat:@"%lu",(unsigned long)_selectedInventory.quantity];
    _txtRoom.text = _selectedInventory.room;
    _txtDescription.text = _selectedInventory.originalDescription;
    _txtCondition.text = [[UserData data].conditions getConditionByID:_selectedInventory.conditionID].conditionName;
    _txtPurchasePrice.text = _selectedInventory.purchasePrice.stringValue;
    _txtPurchaseLocation.text = _selectedInventory.purchaseLocation;
    if (_itemNo > 9) {
        _lblItemNo.text = [NSString stringWithFormat:@"Item:#%ld",(long)_itemNo];
    } else {
        _lblItemNo.text = [NSString stringWithFormat:@"Item:#0%ld",(long)_itemNo];
    }
    _lblClaimName.text = [NSString stringWithFormat:@"%@",_selectedInventory.claimName];
    _tblInventoryInfo.sectionHeaderHeight = UITableViewAutomaticDimension;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
     if (_editMode) {
         _txtAgeY.text = [NSString stringWithFormat:@"%lu",(unsigned long)_selectedInventory.ageY];
         _txtAgeM.text = [NSString stringWithFormat:@"%lu",(unsigned long)_selectedInventory.ageM];
        _saveAddButton.enabled = NO;
        _saveAddButton.layer.backgroundColor = [[UIColor grayColor]CGColor];
        _saveAddButton.layer.borderColor = [[UIColor darkGrayColor]CGColor];
        _btnViewAddInventory.hidden = YES;
        _btnViewEditInventory.hidden = NO;
    } else {
        _txtAgeY.text = @"";
        _txtAgeM.text = @"";
        _btnViewAddInventory.hidden = NO;
        _btnViewEditInventory.hidden = YES;
    }
    btnReturn = [[UIButton alloc]init];
    [btnReturn setTitle:@"Done" forState:UIControlStateNormal];
    [btnReturn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnReturn setFrame:CGRectMake(0, 163, 106, 53)];
    btnReturn.adjustsImageWhenHighlighted = NO;
    [btnReturn addTarget:self action:@selector(numberPadDone) forControlEvents:UIControlEventTouchUpInside];
    _txtDescription.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtPurchaseLocation.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtRoom.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtPurchasePrice.autocorrectionType = UITextAutocorrectionTypeNo;
}

//- (void) viewWillAppear:(BOOL)animated {
//    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (IBAction)btnViewMoreAction:(id)sender {
    [selectedTextfield resignFirstResponder];
    if (viewMore) {
        viewMore = NO;
        [UIView animateWithDuration:0.3 animations:^{
            CGRect oldFrame = self.headerView.frame;
            self.headerView.frame = CGRectMake(oldFrame.origin.x, oldFrame.origin.y, oldFrame.size.width, 185);
            [_tblInventoryInfo setTableHeaderView:self.headerView];
            _viewMoreView.hidden = YES;
            _viewMoreViewHeightConstraint.constant = 0;
            [_btnViewMore setTitle:@"View more" forState:UIControlStateNormal];
        }];
    } else {
        viewMore = YES;
        [UIView animateWithDuration:0.3 animations:^{
            CGRect oldFrame = self.headerView.frame;
            self.headerView.frame = CGRectMake(oldFrame.origin.x, oldFrame.origin.y, oldFrame.size.width, 380);
            [_tblInventoryInfo setTableHeaderView:self.headerView];
            _viewMoreView.hidden = NO;
            _viewMoreViewHeightConstraint.constant = 185;
            [_btnViewMore setTitle:@"View less" forState:UIControlStateNormal];
        }];
    }
}

/*PHOTO FIX*/
//- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    if (tableView == _tblInventoryInfo) {
//        return _photoCollectionView;
//    }
//    return nil;
//}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    if (tableView == _tblInventoryInfo) {
//        return _photoCollectionView.contentSize.height;
//    }
//    return 0;
//}
/*PHOTO FIX*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (selectedTextfield == _txtRoom) {
        return _rooms.count;
    }
    else if (selectedTextfield == _txtCondition) {
        return [UserData data].conditions.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pickerCell" forIndexPath:indexPath];
    if (selectedTextfield == _txtRoom) {
        cell.textLabel.text = _rooms[indexPath.row];
    }
    else if (selectedTextfield == _txtCondition) {
        cell.textLabel.text = [[UserData data].conditions getConditionByID:indexPath.row+1].conditionName;
    }
    cell.textLabel.textColor = [UIColor colorWithRed:145.0/255.0 green:145.0/255.0 blue:145.0/255.0 alpha:1];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:20];
    cell.textLabel.textColor = kBCOrangeColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (selectedTextfield == _txtRoom) {
        _txtRoom.text = _rooms[indexPath.row];
    }
    else {
        _txtCondition.text = [[UserData data].conditions getConditionByID:indexPath.row+1].conditionName;
    }
    [self hidePicker:YES];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (_editMode) {
        return 1+photosArray.count;
    }
    return 1+_selectedInventory.photoList.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        UICollectionReusableView *headerView = (UICollectionReusableView*)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        reusableview = headerView;
    }
    return reusableview;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"photoCell";
    InventoryPhotoCustomCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.delegate = self;
    if (indexPath.row == 0) {
        [self drawDashedBorderAroundView:cell.viewAddPhoto];
        cell.viewFirstCell.hidden = NO;
        cell.viewOtherCells.hidden = YES;
    }
    else {
        cell.viewFirstCell.hidden = YES;
        cell.viewOtherCells.hidden = NO;
        Photo *p;
        if (_editMode) {
            p = photosArray[indexPath.row-1];
        }
        else {
            p = [_selectedInventory.photoList getPhotoByIndex:indexPath.row-1];
        }
        if (p.localURL) {
            PHAsset *asset = [PHAsset fetchAssetWithALAssetURL:p.localURL];
            if (asset){
                PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
                options.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
                options.synchronous = YES;
                options.networkAccessAllowed = NO;
                PHImageManager *manager = [PHImageManager defaultManager];
                [manager requestImageForAsset:asset targetSize:CGSizeMake(145,138) contentMode:PHImageContentModeDefault options:options resultHandler:^(UIImage *resultImage, NSDictionary *info)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         cell.imageView.image = resultImage;
                     });
                 }];
            }
        }
        cell.presenterViewController = self;
        cell.collectionView = _photoCollectionView;
        cell.photo = p;
        cell.inventory = _selectedInventory;
        cell.txtPhotoTitle.delegate = cell;
        cell.txtPhotoTitle.text = p.imageName;
    }
    [self layoutPhotoCollectionView];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (!_viewPicker.hidden) {
        _viewPicker.hidden = YES;
    }
    if (indexPath.row == 0) {
        [selectedTextfield resignFirstResponder];
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Add a photo" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }];
        [cancel setValue:kBCOrangeColor forKey:@"titleTextColor"];
        [actionSheet addAction:cancel];
        UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if((status == AVAuthorizationStatusDenied) || (status == AVAuthorizationStatusRestricted)){ // denied
                [self showAlertControllerWithTitle:kError andMessage:@"Not authorized to use the camera.  Please give ClaimTracker access in Settings" andBtnTitle:kClose];
                return;
            }
            if ([[NSString stringWithFormat:@"%@",_selectedClaim.claimName] isEqualToString:@"<null>"]) {
                [self showAlertControllerWithTitle:kError andMessage:@"Invalid Claim! Photo album for the claim not found" andBtnTitle:kClose];
                return;
            }
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                if (status == PHAuthorizationStatusAuthorized){
                    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
                }
                else {
                    dispatch_async (dispatch_get_main_queue(), ^{
                        [self showAlertControllerWithTitle:kError andMessage:@"Not authorized to access photo albums.  Please give ClaimTracker access in Settings" andBtnTitle:kClose];
                    });
                }
            }];
        }];
        [cameraAction setValue:kBCPickerItemColor forKey:@"titleTextColor"];
        [actionSheet addAction:cameraAction];
        
        UIAlertAction *libraryAction = [UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        }];
        [libraryAction setValue:kBCPickerItemColor forKey:@"titleTextColor"];
        [actionSheet addAction:libraryAction];
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
}

- (void)deletePhoto:(Photo *)photoObj {
    if (_editMode) {
        [photosArray removeObject:photoObj];
    }
    else {
        [_selectedInventory.photoList removePhoto:photoObj];
    }
}

#pragma mark Text Field Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (selectedTextfield != textField) {
        [selectedTextfield resignFirstResponder];
    }
    selectedTextfield = textField;
    if (textField == _txtRoom) {
//        if (_rooms.count == 0){
//            [self showAlertControllerWithTitle:kError andMessage:@"There are no rooms defined. Please type the room into the text box" andBtnTitle:kClose];
//            return YES;
//        }
//        else {
//            [self hidePicker:NO];
//            [_pickerView reloadAllComponents];
//            return NO;
//        }
    }
    else if (textField == _txtCondition) {
        if ([UserData data].conditions.count == 0){
            [self showAlertControllerWithTitle:kError andMessage:@"There are no conditions loaded" andBtnTitle:kClose];
            return NO;
        }
        else {
            UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }];
            [cancel setValue:kBCOrangeColor forKey:@"titleTextColor"];
            [actionSheet addAction:cancel];

            for (int j =0 ; j<[UserData data].conditions.count; j++)
            {
                NSString *titleString = [[UserData data].conditions getConditionByID:j+1].conditionName;
                UIAlertAction * action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    _txtCondition.text = [[UserData data].conditions getConditionByID:j+1].conditionName;
                }];
                [action setValue:kBCPickerItemColor forKey:@"titleTextColor"];
                if ([[[UserData data].conditions getConditionByID:j+1].conditionName isEqualToString:_txtCondition.text]) {
                    [action setValue:[UIColor blackColor] forKey:@"titleTextColor"];
                }
                [actionSheet addAction:action];
            }
            [self presentViewController:actionSheet animated:YES completion:nil];
//            [self hidePicker:NO];
//            [_tblPickerView reloadData];
            return NO;
        }
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
}

//NEED_FOR_LAUNCH
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == _txtQuantity) {
        [_txtQuantity resignFirstResponder];
        [_txtRoom becomeFirstResponder];
        return NO;
    }
    else if (textField == _txtRoom) {
        [_txtRoom resignFirstResponder];
        [_txtDescription becomeFirstResponder];
        return NO;
    }
    else if (textField == _txtDescription) {
        if (viewMore) {
            [_txtDescription resignFirstResponder];
            [_txtAgeY becomeFirstResponder];
            return NO;
        }
        [_txtDescription resignFirstResponder];
        return YES;
    }
    else if (textField == _txtAgeY) {
        [_txtAgeY resignFirstResponder];
        [_txtAgeM becomeFirstResponder];
        return NO;
    }
    else if (textField == _txtAgeM) {
        [_txtAgeM resignFirstResponder];
        [_txtCondition becomeFirstResponder];
        return NO;
    }
    else if (textField == _txtCondition) {
        [_txtCondition resignFirstResponder];
        [_txtPurchasePrice becomeFirstResponder];
        return NO;
    }
    else if (textField == _txtPurchasePrice) {
        [_txtPurchasePrice resignFirstResponder];
        [_txtPurchaseLocation becomeFirstResponder];
        return NO;
    }
    else if (textField == _txtPurchaseLocation) {
        [textField resignFirstResponder];
    }
    //    if ([textField canResignFirstResponder]) {
    //        [textField resignFirstResponder];
    //    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}

//NEED_FOR_LAUNCH
- (void)numberPadDone{
    [btnReturn removeFromSuperview];
    btnReturn.hidden = YES;
    //    if (selectedTextfield == _txtQuantity) {
    //        [selectedTextfield resignFirstResponder];
    //        [_txtRoom becomeFirstResponder];
    //    }
    //    else if (selectedTextfield == _txtAgeY) {
    //        [selectedTextfield resignFirstResponder];
    //        [_txtAgeM becomeFirstResponder];
    //    }
    //    else if (selectedTextfield == _txtAgeM) {
    //        [selectedTextfield resignFirstResponder];
    //        [_txtPurchasePrice becomeFirstResponder];
    //    }
    [selectedTextfield resignFirstResponder];
}

#pragma mark Picker View Delegates

//- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
//    return 1;
//}

//- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
//    if (selectedTextfield == _txtRoom) {
//        return _rooms.count;
//    }
//    else if (selectedTextfield == _txtCondition) {
//        return [UserData data].conditions.count;
//    }
//    return 0;
//}

//- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    if (selectedTextfield == _txtRoom) {
//        return _rooms[row];
//    }
//    else if (selectedTextfield == _txtCondition) {
//        return [[UserData data].conditions getConditionByID:row+1].conditionName;
//    }
//    return @"";
//}

- (IBAction)saveFinishButtonTapped:(id)sender {
    [selectedTextfield resignFirstResponder];
    NSString *existingConditionName = [[UserData data].conditions getConditionByID:_selectedInventory.conditionID].conditionName;
    if (_editMode && (_selectedInventory.quantity == _txtQuantity.text.integerValue) && (_selectedInventory.room == _txtRoom.text) && ([_selectedInventory.originalDescription isEqualToString:_txtDescription.text]) && (_selectedInventory.ageY == _txtAgeY.text.integerValue) && (_selectedInventory.ageM == _txtAgeM.text.integerValue) && ([existingConditionName isEqualToString:_txtCondition.text]) && ([_selectedInventory.purchasePrice.stringValue isEqualToString:_txtPurchasePrice.text]) && ([_selectedInventory.purchaseLocation isEqualToString:_txtPurchaseLocation.text]) && !photosArray.count) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    if (_txtDescription.text.length == 0) {
        [self showAlertControllerWithTitle:kError andMessage:@"Please add a description" andBtnTitle:kOK];
        return;
    }
    if ([self validateData]) {
        NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:self.txtPurchasePrice.text];
        _selectedInventory.quantity = [self.txtQuantity.text integerValue];
        _selectedInventory.room = self.txtRoom.text;
        _selectedInventory.originalDescription = self.txtDescription.text;
        _selectedInventory.ageY = [self.txtAgeY.text integerValue];
        _selectedInventory.ageM = [self.txtAgeM.text integerValue];
        _selectedInventory.conditionID = [[UserData data].conditions getConditionByName:self.txtCondition.text].conditionID;
        _selectedInventory.purchasePrice = [price isEqualToNumber:[NSDecimalNumber notANumber]] ? [NSDecimalNumber decimalNumberWithString:@"0"] : price;
        _selectedInventory.purchaseLocation = self.txtPurchaseLocation.text;
        if (_selectedInventory.edited == NO){
            _selectedInventory.edited = YES;
        }
        
        //NEED_FOR_LAUNCH
        if (_editMode) {
            for (Photo *p in photosArray) {
                p.added = TRUE;
                [_selectedInventory.photoList addPhoto:p];
            }
        }
        
        if(!_editMode)
            [_selectedClaim.inventoryList addInventory:_selectedInventory];
        

        [[UserData data] saveClaims];
        if (_editMode) {
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"Inventory %@ edited successfully",_lblItemNo.text] preferredStyle: UIAlertControllerStyleAlert];
            [self presentViewController: alertView animated:YES completion:nil];
            [self performSelector:@selector(dismissAlert:) withObject:alertView afterDelay:0.8];
        }
        else {
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"Inventory %@ added successfully",_lblItemNo.text] preferredStyle: UIAlertControllerStyleAlert];
            [self presentViewController: alertView animated:YES completion:nil];
            [self performSelector:@selector(dismissAlert:) withObject:alertView afterDelay:0.8];
        }
    }
}

- (void)dismissAlert:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [(UIAlertController*)sender dismissViewControllerAnimated:YES completion:nil];
}

- (void)dismissAlertQuickAdd:(id)sender {
    [(UIAlertController*)sender dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveAddButtonTapped:(id)sender {
    [selectedTextfield resignFirstResponder];
    if ([self validateData]) {
        [self saveInventory];
        [selectedTextfield resignFirstResponder];
        if (_selectedInventory.originalDescription.length == 0){
            [_selectedClaim.inventoryList removeInventory:_selectedInventory];
            [self showAlertControllerWithTitle:kError andMessage:@"Please add a description" andBtnTitle:kOK];
        }
        else {
            _selectedInventory = [self addInventory];
            [_photoCollectionView reloadData];
            [self flashScreen];
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"Inventory %@ added successfully",_lblItemNo.text] preferredStyle: UIAlertControllerStyleAlert];
            self.rooms = [_selectedClaim getRoomList];
            [self presentViewController: alertView animated:YES completion:nil];
            _itemNo++;
            if (_itemNo > 9) {
                _lblItemNo.text = [NSString stringWithFormat:@"Item:#%ld",(long)_itemNo];
            } else {
                _lblItemNo.text = [NSString stringWithFormat:@"Item:#0%ld",(long)_itemNo];
            }
            _txtCondition.text = [[UserData data].conditions getConditionByID:1].conditionName;
            _txtAgeM.text = @"0"; _txtAgeY.text = @"0"; _txtDescription.text = @""; _txtPurchasePrice.text = @""; _txtPurchaseLocation.text = @"";
            [self performSelector:@selector(dismissAlertQuickAdd:) withObject:alertView afterDelay:0.5];
        }
    }
}

- (void)saveInventory{
    NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:self.txtPurchasePrice.text];
    _selectedInventory.quantity = [self.txtQuantity.text integerValue];
    _selectedInventory.room = self.txtRoom.text;
    _selectedInventory.originalDescription = self.txtDescription.text;
    _selectedInventory.ageY = [self.txtAgeY.text integerValue];
    _selectedInventory.ageM = [self.txtAgeM.text integerValue];
    _selectedInventory.conditionID = [[UserData data].conditions getConditionByName:self.txtCondition.text].conditionID;
    _selectedInventory.purchasePrice = [price isEqualToNumber:[NSDecimalNumber notANumber]] ? [NSDecimalNumber decimalNumberWithString:@"0"] : price;
    _selectedInventory.purchaseLocation = self.txtPurchaseLocation.text;
    //NEED_FOR_LAUNCH
    if (_editMode) {
        for (Photo *p in photosArray) {
            p.added = TRUE;
            [_selectedInventory.photoList addPhoto:p];
        }
    }
    [_selectedClaim.inventoryList addInventory:_selectedInventory];
}

- (Inventory *)addInventory {
    Inventory *inventory = [[Inventory alloc] init];
    inventory.claimID = _selectedClaim.claimID;
    inventory.claimName = _selectedClaim.claimName;
    inventory.quantity = 1;
    inventory.conditionID = 1; // Average
    inventory.edited = YES;
    inventory.added = YES;
    inventory.originalDescription = @"";
    if (_selectedClaim.inventoryList.count > 0) {
        Inventory * lastInv = [_selectedClaim.inventoryList getInventoryByIndex:_selectedClaim.inventoryList.count - 1];
        inventory.ordinal = [lastInv.ordinal decimalNumberByAdding:[NSDecimalNumber decimalNumberWithString:@"1"]];
        if ([self.txtRoom.text isEqualToString:@""]){
            self.txtRoom.text = inventory.room = lastInv.room;
        }
        else
            inventory.room = self.txtRoom.text;
        inventory.index = lastInv.index + 1;
    }
    else {
        inventory.ordinal = [NSDecimalNumber decimalNumberWithString:@"1"];
        inventory.index = 0;
    }
    //NEED_FOR_LAUNCH
//    [_selectedClaim.inventoryList addInventory:inventory];
    _txtQuantity.text = [NSString stringWithFormat:@"%lu", (unsigned long)inventory.quantity];
    _txtDescription.text = inventory.originalDescription;
    return inventory;
}

-(void) flashScreen {
    UIWindow *wnd = [UIApplication sharedApplication].keyWindow;
    UIView *v = [[UIView alloc] initWithFrame: CGRectMake(0, 0, wnd.frame.size.width, wnd.frame.size.height)];
    [wnd addSubview: v];
    v.backgroundColor = [UIColor grayColor];
    [UIView beginAnimations: nil context: nil];
    [UIView setAnimationDuration: 0.25];
    v.alpha = 0.0f;
    [UIView commitAnimations];
    [[UserData data] saveClaims];
}

- (IBAction)btnBackAction:(id)sender {
    if(!self.editMode) // Canceling an Add
    {
        [_selectedClaim.inventoryList removeInventory:_selectedInventory];
        _selectedInventory = nil;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) layoutPhotoCollectionView {
    [_photoCollectionView setFrame:CGRectMake(_photoCollectionView.frame.origin.x, _photoCollectionView.frame.origin.y, _photoCollectionView.frame.size.width,_photoCollectionView.contentSize.height)];
    [self.view layoutIfNeeded];
    [_tblInventoryInfo reloadData];
}

- (void)drawDashedBorderAroundView:(UIView *)v {
    //border definitions
    CGFloat cornerRadius = 0.0;
    CGFloat borderWidth = 1.0;
    UIColor *lineColor = [UIColor darkGrayColor];
    //drawing
    CGRect frame = v.bounds;
    CAShapeLayer *_shapeLayer = [CAShapeLayer layer];
    //creating a path
    CGMutablePathRef path = CGPathCreateMutable();
    //drawing a border around a view
    CGPathMoveToPoint(path, NULL, 0, frame.size.height - cornerRadius);
    CGPathAddLineToPoint(path, NULL, 0, cornerRadius);
    
    CGPathAddArc(path, NULL, cornerRadius, cornerRadius, cornerRadius, M_PI, -M_PI_2, NO);
    CGPathAddLineToPoint(path, NULL, v.frame.size.width, 0);
    
    CGPathAddArc(path, NULL, v.frame.size.width, cornerRadius, cornerRadius, -M_PI_2, 0, NO);
    CGPathAddLineToPoint(path, NULL, v.frame.size.width, frame.size.height - cornerRadius);
    
    CGPathAddArc(path, NULL, v.frame.size.width, frame.size.height - cornerRadius, cornerRadius, 0, M_PI_2, NO);
    CGPathAddLineToPoint(path, NULL, cornerRadius, frame.size.height);
    
    CGPathAddArc(path, NULL, cornerRadius, frame.size.height - cornerRadius, cornerRadius, M_PI_2, M_PI, NO);
    //path is set as the _shapeLayer object's path
    _shapeLayer.path = path;
    CGPathRelease(path);
    _shapeLayer.backgroundColor = [[UIColor clearColor] CGColor];
    _shapeLayer.frame = frame;
    _shapeLayer.masksToBounds = NO;
    [_shapeLayer setValue:[NSNumber numberWithBool:NO] forKey:@"isCircle"];
    _shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    _shapeLayer.strokeColor = [lineColor CGColor];
    _shapeLayer.lineWidth = borderWidth;
    _shapeLayer.lineDashPattern = [NSArray arrayWithObjects:@1, @8, nil];
    _shapeLayer.lineCap = kCALineCapRound;
    //_shapeLayer is added as a sublayer of the view, the border is visible
    [v.layer addSublayer:_shapeLayer];
    v.layer.cornerRadius = cornerRadius;
}

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    @try {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
        imagePickerController.sourceType = sourceType;
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = TRUE;
        
        if (sourceType == UIImagePickerControllerSourceTypeCamera)
        {
            AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if (status == AVAuthorizationStatusAuthorized) { // authorized
                dispatch_async(dispatch_get_main_queue(), ^{
                    imagePickerController.showsCameraControls = YES;
                    self.imagePickerController = imagePickerController;
                    [self presentViewController:self.imagePickerController animated:YES completion:NULL];
                });
            }
            else if((status == AVAuthorizationStatusDenied) || (status == AVAuthorizationStatusRestricted)){ // denied
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Not authorized to use the camera.  Please give ClaimTracker access in Settings." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//                [alert show];
                [self showAlertControllerWithTitle:kError andMessage:@"Not authorized to use the camera.  Please give ClaimTracker access in Settings" andBtnTitle:kClose];
            }
            else if(status == AVAuthorizationStatusNotDetermined) { // not determined
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    if (granted) { // Access has been granted ..do something
                        dispatch_async(dispatch_get_main_queue(), ^{
                            imagePickerController.showsCameraControls = YES;
                            self.imagePickerController = imagePickerController;
                            [self presentViewController:self.imagePickerController animated:YES completion:NULL];
                        });
                    } else { // Access denied ..do something
                        dispatch_async (dispatch_get_main_queue(), ^{
//                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Not authorized to use the camera.  Please give ClaimTracker access in Settings." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//                            [alert show];
                            [self showAlertControllerWithTitle:kError andMessage:@"Not authorized to use the camera.  Please give ClaimTracker access in Settings" andBtnTitle:kClose];
                        });
                    }
                }];
            }
        }
        else {
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                if (status == PHAuthorizationStatusAuthorized){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.imagePickerController = imagePickerController;
                        [self presentViewController:self.imagePickerController animated:YES completion:NULL];
                    });
                }
                else {
                    dispatch_async (dispatch_get_main_queue(), ^{
//                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Not authorized to access photo albums.  Please give ClaimTracker access in Settings." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//                        [alert show];
                        [self showAlertControllerWithTitle:kError andMessage:@"Not authorized to access photo albums.  Please give ClaimTracker access in Settings" andBtnTitle:kClose];
                    });
                }
            }];
        }
    } @catch (NSException *exception) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//        [alert show];
        [self showAlertControllerWithTitle:kError andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
        UIView *spinner = [[UserData data] makeSpinner];
        
//        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
//        // Request to save the image to camera roll
//        [library writeImageToSavedPhotosAlbum:[takenImage CGImage] orientation:(ALAssetOrientation)[takenImage imageOrientation] completionBlock:^(NSURL *assetURL, NSError *error){
//            if (error) {
//                //NOT SAVED
//                //DISPLAY ERROR THE PICTURE CAN'T BE SAVED
//            } else {
//                //SAVED
//            }  
//        }];        

        [PHAsset addImageToCTAlbum:image ForClaim:_selectedClaim.claimName withCompletion:^{
            @try {
                PHAssetCollection *ctAlbum = [PHAssetCollection getCTAlbumForClaim:_selectedClaim.claimName];
                PHFetchResult *fetchResult = [PHAsset fetchAssetsInAssetCollection:ctAlbum options:nil];
                PHAsset *asset = fetchResult.lastObject;
                
                NSRange range = NSMakeRange(0, 36);
                NSString *url = [NSString stringWithFormat:@"assets-library://asset/asset.JPG?id=%@&ext=JPG",[asset.localIdentifier substringWithRange:range]];
                
                Photo *p = [[Photo alloc] init];
                p.imageName = [NSString stringWithFormat:@"Photo %@", _selectedInventory.ordinal];
                p.claimID = _selectedClaim.claimID;
                p.claimName = _selectedClaim.claimName;
                p.inventoryID = _selectedInventory.inventoryID;
                p.inventoryOrdinal = _selectedInventory.ordinal;
//                p.added = TRUE;
                if (_editMode) {
                    [photosArray addObject:p];
                } else {
                    [_selectedInventory.photoList addPhoto:p];
                }
                p.localURL = [NSURL URLWithString:url];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    @try {
                        if (self.imagePickerController)
                        {
                            [self dismissViewControllerAnimated:YES completion:NULL];
                        }
                        self.imagePickerController = nil;
                        [_photoCollectionView reloadData];
                    } @catch (NSException *exception) {
                        [self showAlertControllerWithTitle:kError andMessage:exception.reason andBtnTitle:kClose];
                    } @finally {
                        [[UserData data] removeSpinner:spinner];
                    }
                });
            } @catch (NSException *exception) {
                [self showAlertControllerWithTitle:kError andMessage:exception.reason andBtnTitle:kClose];
            } @finally {
                [[UserData data] removeSpinner:spinner];
            }
        }];
    }
    else {
        NSURL *url = [info valueForKey:UIImagePickerControllerReferenceURL];
        
        Photo *p = [[Photo alloc] init];
        p.localURL = url;
        p.imageName = [NSString stringWithFormat:@"Photo %@", _selectedInventory.ordinal];
        p.claimID = _selectedClaim.claimID;
        p.claimName = _selectedClaim.claimName;
        p.inventoryID = _selectedInventory.inventoryID;
        p.inventoryOrdinal = _selectedInventory.ordinal;
//        p.added = TRUE;
        if (_editMode) {
            [photosArray addObject:p];
        } else {
            [_selectedInventory.photoList addPhoto:p];
        }
        [_photoCollectionView reloadData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.imagePickerController)
            {
                [self dismissViewControllerAnimated:YES completion:NULL];
            }
            self.imagePickerController = nil;
        });
    }
//    [self layoutPhotoCollectionView];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:NULL];
    self.imagePickerController = nil;
}

//- (IBAction)btnPickerDoneAction:(id)sender {
//    NSInteger row = [_pickerView selectedRowInComponent:0];
//    if (selectedTextfield == _txtRoom) {
//        _txtRoom.text = [[_pickerView delegate] pickerView:_pickerView titleForRow:row forComponent:0];
//    }
//    else {
//        _txtCondition.text = [[_pickerView delegate] pickerView:_pickerView titleForRow:row forComponent:0];
//    }
//    [self hidePicker:YES];
//}

- (IBAction)btnPickerCancelAction:(id)sender {
    [self hidePicker:YES];
}

- (void)hidePicker:(BOOL)hideOrNot{
    _viewPicker.hidden = hideOrNot;
//    _pickerView.hidden = hideOrNot;
}

- (void)showAlertControllerWithTitle:(NSString*)alertTitle andMessage:(NSString*)alertMsg andBtnTitle:(NSString*)btnTitle {
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:alertTitle message:alertMsg preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *closeButton = [UIAlertAction actionWithTitle:btnTitle style:UIAlertActionStyleCancel handler: ^(UIAlertAction * action) {
        
    }];
    [closeButton setValue:kBCOrangeColor forKey:@"titleTextColor"];
    [alertView addAction:closeButton];
    [self presentViewController: alertView animated:YES completion:nil];
//    alertView.view.tintColor = kBCOrangeColor;
}

- (void)keyboardWillShow:(NSNotification*)notification {
    if (!_viewPicker.hidden) {
        _viewPicker.hidden = YES;
    }
//    btnReturn.hidden = YES;
    [btnReturn removeFromSuperview];
    if (selectedTextfield == _txtQuantity || selectedTextfield == _txtAgeM ||
        selectedTextfield == _txtAgeY /*|| selectedTextfield == _txtPurchasePrice*/) {
        dispatch_async(dispatch_get_main_queue(), ^{
            btnReturn.hidden = NO;
            UIView *keyboardView = [[[UIApplication sharedApplication]windows]lastObject];
            [btnReturn setFrame:CGRectMake(5, keyboardView.frame.size.height-53, 106, 53)];
            [keyboardView addSubview:btnReturn];
            [keyboardView bringSubviewToFront:btnReturn];
        });
    }

    if (selectedTextfield != _txtQuantity && selectedTextfield != _txtRoom && selectedTextfield != _txtDescription && selectedTextfield != _txtAgeY && selectedTextfield != _txtAgeM && selectedTextfield != _txtCondition) {
        NSDictionary *info = notification.userInfo;
        CGPoint kbOrigin = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue].origin;
        if (self.view.frame.origin.y < 0) {
            return;
        }
        float toMove = ((self.view.frame.origin.y+self.view.frame.size.height) - kbOrigin.y);
        [UIView animateWithDuration:0.35
                              delay:0.0
                            options: UIViewAnimationOptionTransitionCrossDissolve
                         animations:^{
                             self.view.frame = CGRectMake(0, -toMove, self.view.frame.size.width, self.view.frame.size.height);
                         } completion:nil];
    }
}

- (void)keyboardWillHide:(NSNotification*)notification {
    [btnReturn removeFromSuperview];
//    btnReturn.hidden = YES;
//    if (selectedTextfield == _txtQuantity || selectedTextfield == _txtAgeM ||
//        selectedTextfield == _txtAgeY || selectedTextfield == _txtPurchasePrice) {
//    }
//    if (selectedTextfield == _txtPurchasePrice || selectedTextfield == _txtPurchaseLocation) {
        [UIView animateWithDuration:0.35
                              delay:0.0
                            options: UIViewAnimationOptionTransitionCrossDissolve
                         animations:^{
                             self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                         }
                         completion:nil];
//    }
}

- (BOOL)validateData{
    if (self.txtQuantity.text.length == 0){
        [self showAlertControllerWithTitle:kError andMessage:@"Please enter a quantity" andBtnTitle:kClose];
        return NO;
    }
    
    if (self.txtRoom.text.length > 25){
        [self showAlertControllerWithTitle:kError andMessage:@"Room length exceeded" andBtnTitle:kClose];
        return NO;
    }

    if ([[self.txtRoom.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]length] == 0){
        [self showAlertControllerWithTitle:kError andMessage:@"Please enter a room" andBtnTitle:kClose];
        return NO;
    }

    if ([self.txtDescription.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        [self showAlertControllerWithTitle:kError andMessage:@"Please add a description" andBtnTitle:kOK];
        return NO;
    }

    if (([self.txtQuantity.text integerValue] > 99)){
        [self showAlertControllerWithTitle:kError andMessage:@"Quantity cannot exceed 99" andBtnTitle:kClose];
        return NO;
    }
    
    if ([self.txtQuantity.text integerValue] <= 0){
        [self showAlertControllerWithTitle:kError andMessage:@"Please enter a quantity greater than zero" andBtnTitle:kClose];
        return NO;
    }
    
    if ((self.txtAgeY.text.length > 0) && ([self.txtAgeY.text integerValue] > 99)){
        [self showAlertControllerWithTitle:kError andMessage:@"Age (Y) cannot exceed 99" andBtnTitle:kClose];
        return NO;
    }
    
    if ((self.txtAgeM.text.length > 0) && ([self.txtAgeM.text integerValue] > 11)){
        [self showAlertControllerWithTitle:kError andMessage:@"Age (M) cannot be more than 11" andBtnTitle:kClose];
        return NO;
    }
    
    if (self.txtPurchaseLocation.text.length > 255){
        [self showAlertControllerWithTitle:kError andMessage:@"Purchase location length exceeded" andBtnTitle:kClose];
        return NO;
    }
    
    if (self.txtPurchasePrice.text.length > 10){
        [self showAlertControllerWithTitle:kError andMessage:@"Purchase price limit exceeded" andBtnTitle:kClose];
        return NO;
    }
    
    if (self.txtPurchasePrice.text.length > 0){
        self.txtPurchasePrice.text = [NSString stringWithFormat:@"%.02f", [self.txtPurchasePrice.text floatValue]];
    }
    return YES;
}

- (IBAction)btnRoomPickerAction:(id)sender {
    selectedTextfield = _txtRoom;
    [self.view endEditing:YES];
    if (_rooms.count == 0){
        [self showAlertControllerWithTitle:kError andMessage:@"There are no rooms defined. Please type the room into the text box" andBtnTitle:kClose];
    }
    else {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }];
        [cancel setValue:kBCOrangeColor forKey:@"titleTextColor"];
        [actionSheet addAction:cancel];
        
        for (int j =0 ; j<_rooms.count; j++)
        {
            NSString *titleString = _rooms[j];
            UIAlertAction * action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                _txtRoom.text = _rooms[j];
            }];
            [action setValue:kBCPickerItemColor forKey:@"titleTextColor"];
            if ([_rooms[j] isEqualToString:_txtRoom.text]) {
                [action setValue:[UIColor blackColor] forKey:@"titleTextColor"];
            }
            [actionSheet addAction:action];
        }
        [self presentViewController:actionSheet animated:YES completion:nil];
//        [self hidePicker:NO];
//        [_tblPickerView reloadData];
    }
}

- (IBAction)btnConditionPickerAction:(id)sender {
    selectedTextfield = _txtCondition;
    [self.view endEditing:YES];
    if ([UserData data].conditions.count == 0){
        [self showAlertControllerWithTitle:kError andMessage:@"There are no conditions loaded" andBtnTitle:kClose];
    }
    else {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }];
        [cancel setValue:kBCOrangeColor forKey:@"titleTextColor"];
        [actionSheet addAction:cancel];
        
        for (int j =0 ; j<[UserData data].conditions.count; j++)
        {
            NSString *titleString = [[UserData data].conditions getConditionByID:j+1].conditionName;
            UIAlertAction * action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                _txtCondition.text = [[UserData data].conditions getConditionByID:j+1].conditionName;
            }];
            [action setValue:kBCPickerItemColor forKey:@"titleTextColor"];
            if ([[[UserData data].conditions getConditionByID:j+1].conditionName isEqualToString:_txtCondition.text]) {
                [action setValue:[UIColor blackColor] forKey:@"titleTextColor"];
            }
            [actionSheet addAction:action];
        }
        [self presentViewController:actionSheet animated:YES completion:nil];
//        [self hidePicker:NO];
//        [_tblPickerView reloadData];
    }
}

@end
