//
//  LoginViewController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 4/30/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "LoginController.h"
#import "UserData.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "ClaimsListViewController.h"
#import "DataSource.h"
#import "UIButton+Extended.h"

@interface LoginController (){
    UIView *spinner;
}

@property (weak, nonatomic) IBOutlet UITextField *txtEMail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
- (IBAction)cmdLogin:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;

@end

@implementation LoginController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.txtEMail.text = @"test.adjuster@nationalvendor.com";
    self.txtPassword.text = @"h3rtZc0GL0g";
    
    self.txtEMail.delegate = self;
    self.txtPassword.delegate = self;

    self.txtEMail.nextTextField = self.txtPassword;
    self.txtPassword.nextTextField = nil;

    if ([UserData data].lastLogin) self.txtEMail.text = [UserData data].lastLogin;
    
    _loginBtn.layer.cornerRadius = 2.0;
    self.navigationController.navigationBar.hidden = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissTheKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [_loginBtn setBackgroundImage:[_loginBtn imageForButtonWithColor:kBCOrangeSelectedColor] forState:UIControlStateHighlighted];
}

- (void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *info = notification.userInfo;
    CGPoint kbOrigin = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue].origin;
    if (self.view.frame.origin.y < 0) {
        return;
    }
    float toMove = ((self.view.frame.origin.y+self.view.frame.size.height) - kbOrigin.y);
    [UIView animateWithDuration:0.35
                          delay:0.0
                        options: UIViewAnimationOptionTransitionCrossDissolve
                     animations:^{
                         self.view.frame = CGRectMake(0, -toMove, self.view.frame.size.width, self.view.frame.size.height);
                     } completion:nil];
}

- (void)keyboardWillHide:(NSNotification*)notification {
    [UIView animateWithDuration:0.35
                          delay:0.0
                        options: UIViewAnimationOptionTransitionCrossDissolve
                     animations:^{
                         self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:nil];
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField
{
    UITextField *next = theTextField.nextTextField;
    if (next)
    {
        [next becomeFirstResponder];
    }
    else
    {
        [theTextField resignFirstResponder];
    }
    
    return NO;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    if (textField.tag == 1) {
        self.userNameLineView.backgroundColor = [UIColor orangeColor];
    }
    if (textField.tag == 2) {
        self.psswdLineView.backgroundColor = [UIColor orangeColor];
    }
}


-(void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 1) {
        self.userNameLineView.backgroundColor = [UIColor lightGrayColor/*colorWithRed:103.0 green:113.0 blue:135.0 alpha:1.0*/];
    }
    if (textField.tag == 2) {
        self.psswdLineView.backgroundColor = [UIColor lightGrayColor/*colorWithRed:103.0 green:113.0 blue:135.0 alpha:1.0*/];
    }
}

-(void)dismissTheKeyboard
{
    [_txtEMail resignFirstResponder];
    [_txtPassword resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//NEED_FOR_LAUNCH
- (IBAction)cmdLogin:(UIButton *)sender {
    //    dispatch_async(dispatch_get_main_queue(), ^{
    spinner = [[UserData data] makeSpinner];
    //    });
    
    [self performSelector:@selector(login) withObject:nil afterDelay:0.5];
    //    [self login];
}

- (void)login
{
//    spinner = [[UserData data] makeSpinner];

    if (self.txtEMail.text.length == 0)
    {
        [[UserData data] removeSpinner:spinner];
        [self showAlertControllerWithTitle:kError andMessage:kEnterLoginNameMsg andBtnTitle:kClose];
        return;
    }
    if (self.txtPassword.text.length == 0)
    {
        [[UserData data] removeSpinner:spinner];
        [self showAlertControllerWithTitle:kError andMessage:kEnterPasswordMsg andBtnTitle:kClose];
        return;
    }
    
    NSArray *arrIllegalChars = [NSArray arrayWithObjects:@"*", @"<", @">", @"%", @"&", @":", @"\\", @"?", nil];
    for (NSString *c in arrIllegalChars)
    {
        if ([self.txtEMail.text rangeOfString:c].location != NSNotFound)
        {
            [[UserData data] removeSpinner:spinner];
            [self showAlertControllerWithTitle:kError andMessage:[NSString stringWithFormat:kEmailValidationMsg, c] andBtnTitle:kClose];
            return;
        }
        if ([self.txtPassword.text rangeOfString:c].location != NSNotFound)
        {
            [[UserData data] removeSpinner:spinner];
            [self showAlertControllerWithTitle:kError andMessage:[NSString stringWithFormat:kPasswordValidationMsg, c] andBtnTitle:kClose];
            return;
        }
    }
    
    // If logging in as a different user, we need to clear out claims from last user
    if (![[UserData data].lastLogin isEqualToString: self.txtEMail.text]) {
        [[UserData data] clearClaimData];
    }
    
    DataSource * d = [DataSource new];
    if (!d.isConnected)
    {
        [[UserData data] removeSpinner:spinner];
        if ([[[UserData data]lastLogin] isEqualToString:_txtEMail.text]) {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:kClaimsStoryboard bundle: nil];
            ClaimsListViewController *claimsVC = (ClaimsListViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:kClaimsVC];
            claimsVC.isOfflineLogin = YES;
            [self.navigationController pushViewController:claimsVC animated:YES];
        } else {
            [self showAlertControllerWithTitle:kError andMessage:kNetworkConnectionErrorMsg andBtnTitle:kClose];
        }
        return;
    }
    else{
        if ([[UserData data] authenticateUser:self.txtEMail.text andPassword:self.txtPassword.text]){
            [[UserData data] refreshWithCompletion:^(void){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UserData data] removeSpinner:spinner];
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:kClaimsStoryboard bundle: nil];
                    ClaimsListViewController *claimsVC = (ClaimsListViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:kClaimsVC];
                    [self.navigationController pushViewController:claimsVC animated:YES];
                });
            }];
        }
        else {
            [[UserData data] removeSpinner:spinner];
        }
    }
}

- (void)showAlertControllerWithTitle:(NSString*)alertTitle andMessage:(NSString*)alertMsg andBtnTitle:(NSString*)btnTitle {
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:alertTitle message:alertMsg preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *closeButton = [UIAlertAction actionWithTitle:btnTitle style:UIAlertActionStyleCancel handler: ^(UIAlertAction * action) {
        
    }];
    [closeButton setValue:kBCOrangeColor forKey:@"titleTextColor"];
    [alertView addAction:closeButton];
    [self presentViewController: alertView animated:YES completion:nil];
}

@end
