//
//  LoginViewController.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 4/30/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITextField+Extended.h"

@interface LoginController : UIViewController<UITextFieldDelegate>

@property NSString *email;
@property NSString *password;
@property (weak, nonatomic) IBOutlet UIImageView *usernameImg;
@property (weak, nonatomic) IBOutlet UIImageView *passwordImg;
@property (weak, nonatomic) IBOutlet UIView *userNameLineView;
@property (weak, nonatomic) IBOutlet UIView *psswdLineView;


//+ (UIImage *)imageWithColor:(UIColor *)color;

@end
