//
//  ClaimInformationController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 6/2/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import "PHAssetCollection+Extended.h"
#import "ClaimInformationController.h"
#import "CTTabBarController.h"
#import "CTNavigationController.h"
#import "UserData.h"

@interface ClaimInformationController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *cmdDone;
@property (weak, nonatomic) IBOutlet UILabel *lblClaim;
@property (weak, nonatomic) IBOutlet UIButton *lblInsuredPhone;
@property (weak, nonatomic) IBOutlet UIButton *lblInsuredEMail;
@property (weak, nonatomic) IBOutlet UILabel *lblLossType;
@property (weak, nonatomic) IBOutlet UILabel *lblCoverageType;
@property (weak, nonatomic) IBOutlet UILabel *lblDeductible;
@property (weak, nonatomic) IBOutlet UILabel *lblOverallLimit;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress1;
@property (weak, nonatomic) IBOutlet UILabel *lblCityStateZip;
- (IBAction)cmdAddress:(UIButton *)sender;
- (IBAction)cmdInsuredPhone:(UIButton *)sender;
- (IBAction)cmdInsuredEMail:(UIButton *)sender;

@end

@implementation ClaimInformationController

- (void)doRefresh{
    @try {
        self.lblClaim.text = @"Claim:";
        self.lblAddress1.text = @"Address:";
        self.lblCityStateZip.text = @"City State Zip";
        [self.lblInsuredPhone setTitle:@"Phone:" forState:UIControlStateNormal];
        [self.lblInsuredEMail setTitle:@"E-Mail:" forState:UIControlStateNormal];
        self.lblLossType.text = @"Loss Type:";
        self.lblCoverageType.text = @"Coverage Type:";
        self.lblDeductible.text = @"Deductible:";
        self.lblOverallLimit.text = @"Overall Limit:";

        [self.claim refreshClaimWithCompletion:^(void){
            [self refreshData];
            if (self.refreshControl) [self.refreshControl endRefreshing];
        }];
        //[self refreshData];
        //if (self.refreshControl) [self.refreshControl endRefreshing];
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (void)refreshData {
    @try {
        NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        
        self.lblClaim.text = [NSString stringWithFormat:@"Claim: %@", self.claim.claimName];
        self.lblAddress1.text = [NSString stringWithFormat:@"%@ %@", self.claim.insuredAddress1, self.claim.insuredAddress2];
        self.lblCityStateZip.text = [NSString stringWithFormat:@"%@, %@ %@", self.claim.insuredCity, self.claim.insuredState, self.claim.insuredZip];
        [self.lblInsuredPhone setTitle:[NSString stringWithFormat:@"Phone: %@", self.claim.insuredPhone] forState:UIControlStateNormal];
        [self.lblInsuredEMail setTitle:[NSString stringWithFormat:@"E-Mail: %@", self.claim.insuredEMail] forState:UIControlStateNormal];
        self.lblLossType.text = [NSString stringWithFormat:@"Loss Type: %@", self.claim.lossType];
        self.lblCoverageType.text = [NSString stringWithFormat:@"Coverage: %@", self.claim.coverageType];
        self.lblDeductible.text = [NSString stringWithFormat:@"Deductible: %@", [formatter stringFromNumber:self.claim.deductible]];
        self.lblOverallLimit.text = [NSString stringWithFormat:@"Overall Limit: %@", [formatter stringFromNumber:self.claim.overallLimit]];
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (void)viewDidLoad {
    @try {
        [super viewDidLoad];

        [_cmdDone setAction:@selector(doneAction)];
        [_cmdDone setTarget:self];
        CTTabBarController *source = (CTTabBarController *)self.parentViewController.parentViewController;
        self.claim = source.selectedClaim;
        
        [self refreshData];
        
        self.refreshControl = [[UIRefreshControl alloc] init];
        self.refreshControl.backgroundColor = [UIColor blueColor];
        self.refreshControl.tintColor = [UIColor whiteColor];
        [self.refreshControl addTarget:self action:@selector(doRefresh) forControlEvents:UIControlEventValueChanged];

        [PHAssetCollection createCTAlbumForClaim:self.claim.claimName WithCompletion:nil];

        // Uncomment the following line to preserve selection between presentations.
        // self.clearsSelectionOnViewWillAppear = NO;
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"segueClaimInfoToQuickAddInventory"]){
        CTNavigationController *destination = segue.destinationViewController;
        destination.selectedClaim = self.claim;
    }
}

- (IBAction)cmdAddress:(UIButton *)sender {
    @try {
        NSString *mapAddress = [@"http://maps.apple.com/?q=" stringByAppendingString:[self.claim.insuredFullAddress stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        NSURL *url = [NSURL URLWithString:mapAddress];
        UIApplication *app = [UIApplication sharedApplication];
        [app openURL:url];
    }
    @catch (NSException *e){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:e.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)cmdInsuredPhone:(UIButton *)sender {
    @try {
        NSString *p = self.claim.insuredPhone;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", p]]];
    }
    @catch (NSException *e){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:e.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)cmdInsuredEMail:(UIButton *)sender {
    @try {
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            mailer.mailComposeDelegate = self;
            mailer.modalPresentationStyle = UIModalPresentationPageSheet;
            [mailer setToRecipients:[NSArray arrayWithObjects:self.claim.insuredEMail, nil]];
            [mailer setMessageBody:@"" isHTML:NO];
            [self presentViewController:mailer animated:YES completion:NULL];
        }
    }
    @catch (NSException *e){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:e.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Canceled");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Failed");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Savd");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Sent");
            break;
        default:
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)unwindToClaimInfo:(UIStoryboardSegue *)segue {
    [self doRefresh];
}

#pragma mark - Table view data source
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)doneAction {
    [self.tabBarController dismissViewControllerAnimated:YES completion:nil];
//    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popViewControllerAnimated:YES];
}
@end
