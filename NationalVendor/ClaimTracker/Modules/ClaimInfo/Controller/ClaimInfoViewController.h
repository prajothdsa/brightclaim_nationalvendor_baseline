//
//  ClaimInfoViewController.h
//  ClaimTracker
//
//  Created by Prajoth Antonio D on 11/08/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Claim.h"
#import <MessageUI/MessageUI.h>
#import "UserData.h"

@interface ClaimInfoViewController : UIViewController<MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblInventoryList;
@property (weak, nonatomic) IBOutlet UIButton *btnAddInventory;
@property (weak, nonatomic) IBOutlet UIView *addInventoryBtnView;

- (IBAction)actionAddInventory:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *topBar;
@property Claim *selectedClaim;
@property (weak, nonatomic) IBOutlet UILabel *lblOverallLimit;
@property (weak, nonatomic) IBOutlet UILabel *lblDeductible;
@property (weak, nonatomic) IBOutlet UILabel *lblCoverage;
@property (weak, nonatomic) IBOutlet UILabel *lblLossType;
@property (weak, nonatomic) IBOutlet UIButton *btnNotes;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnAddressIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnPhoneIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnEmailIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressLine1;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressLine2;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;

- (IBAction)btnAddressClicked:(id)sender;
- (IBAction)btnPhoneClicked:(id)sender;
- (IBAction)btnEmailClicked:(id)sender;
- (IBAction)backButtonAction:(id)sender;
- (IBAction)notesButtonAction:(id)sender;
@end
