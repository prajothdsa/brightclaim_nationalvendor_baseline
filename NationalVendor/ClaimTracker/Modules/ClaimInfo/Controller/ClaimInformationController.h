//
//  ClaimInformationController.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 6/2/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "Claim.h"

@interface ClaimInformationController : UITableViewController<MFMailComposeViewControllerDelegate, UINavigationControllerDelegate>

@property Claim *claim;
- (void)doneAction;
@end
