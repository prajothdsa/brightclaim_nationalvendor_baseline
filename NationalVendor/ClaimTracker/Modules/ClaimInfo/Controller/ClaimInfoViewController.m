//
//  ClaimInfoViewController.m
//  ClaimTracker
//
//  Created by Prajoth Antonio D on 11/08/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import "ClaimInfoViewController.h"
#import "Constants.h"
#import "InventoryListCustomCell.h"
#import "ClaimNoteController.h"
#import "InventoryViewController.h"
#import "ClaimNotesListViewController.h"
#import "InventoryInfoController.h"
#import "DataSource.h"
#import "PHAssetCollection+Extended.h"

@interface ClaimInfoViewController (){
    InventoryListCustomCell *cell;
    UIView *spinner;
    UIRefreshControl *refreshControl;
}
@property (weak, nonatomic) IBOutlet UIView *addInventoryView;
@property (weak, nonatomic) IBOutlet UIView *eMailInsuredView;
@end

@implementation ClaimInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    spinner = [[UserData data] makeSpinner];
    [self drawDashedBorderAroundView:_btnAddInventory];
    [PHAssetCollection createCTAlbumForClaim:_selectedClaim.claimName WithCompletion:nil];
    
    _addInventoryView.layer.shadowColor = [UIColor colorWithRed:211.0/255.0 green:211.0/255.0 blue:212.0/255.0 alpha:1].CGColor;
    _addInventoryView.layer.shadowOffset = CGSizeMake(2, 2);
    _addInventoryView.layer.shadowOpacity = 0.5;
    _addInventoryView.layer.shadowRadius = 10.0f;

    NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    NSString *addrLine1 = [NSString stringWithFormat:@"%@ %@", _selectedClaim.insuredAddress1,_selectedClaim.insuredAddress2];
    _lblAddressLine1.text = ([addrLine1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length>0) ? addrLine1 : @"NA";
    
    NSString *addrLine2 = [NSString stringWithFormat:@"%@ %@ %@",_selectedClaim.insuredCity,_selectedClaim.insuredState,_selectedClaim.insuredZip];
    _lblAddressLine2.text = ([addrLine2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length>0) ? addrLine2 : @"NA";

    if ([self checkForValidString:_selectedClaim.insuredFullAddress]) {
//        [_btnAddress setTitle:@"NA" forState:UIControlStateNormal];
        [_btnAddressIcon setUserInteractionEnabled:false];
    } else {
//        [_btnAddress setTitle:_selectedClaim.insuredFullAddress forState:UIControlStateNormal];
        [_btnAddressIcon setUserInteractionEnabled:true];
    }
    
    if ([self checkForValidString:_selectedClaim.insuredPhone]) {
        _lblPhone.text = @"NA";
        [_btnPhoneIcon setUserInteractionEnabled:false];
    } else {
        _lblPhone.text = _selectedClaim.insuredPhone;
        [_btnPhoneIcon setUserInteractionEnabled:true];
    }
    
    if ([self checkForValidString:_selectedClaim.insuredEMail]) {
        _lblEmail.text = @"NA";
        [_btnEmailIcon setUserInteractionEnabled:false];
    } else {
        _lblEmail.text = _selectedClaim.insuredEMail;
        [_btnEmailIcon setUserInteractionEnabled:true];
    }
    
    _eMailInsuredView.layer.shadowColor = [UIColor colorWithRed:211.0/255.0 green:211.0/255.0 blue:212.0/255.0 alpha:1].CGColor;
    _eMailInsuredView.layer.shadowOffset = CGSizeMake(2, 2);
    _eMailInsuredView.layer.shadowOpacity = 0.5;
    _eMailInsuredView.layer.shadowRadius = 0.1;
    
    _lblCoverage.text = _selectedClaim.coverageType;
    _lblLossType.text = _selectedClaim.lossType;
    _lblDeductible.text = [NSString stringWithFormat:@"%@",[formatter stringFromNumber:_selectedClaim.deductible]];
    _lblOverallLimit.text = [NSString stringWithFormat:@"%@",[formatter stringFromNumber:_selectedClaim.overallLimit]];
    self.lblTitle.text = [NSString stringWithFormat:@"%@", _selectedClaim.claimName];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(doRefresh) forControlEvents:UIControlEventValueChanged];
    refreshControl.backgroundColor = [UIColor colorWithRed:245/255.0f green:247/255.0f blue:248/255.0f alpha:1.0];
    [_tblInventoryList addSubview: refreshControl];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    if ([[UserData data]needToSyncCountForClaimID:_selectedClaim.claimID]>0) {
        [_tblInventoryList reloadData];
        [[UserData data] removeSpinner:spinner];
    }
    else {
        [self.selectedClaim refreshInventoryListWithActivityIndex:0 withCompletion:^(int i){
            [_tblInventoryList reloadData];
            [[UserData data] removeSpinner:spinner];
        }];
    }
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

- (void)doRefresh {
    NSUInteger syncCount = [[UserData data]needToSyncCountForClaimID:_selectedClaim.claimID];
    if (syncCount>0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:kWarning message:[NSString stringWithFormat:@"You have %ld item(s) to be uploaded. You would lose these changes if you refresh your data. Please sync first", (long)syncCount] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kOK style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            [refreshControl endRefreshing];
        }];
        [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:TRUE completion:nil];
    }
    else {
        DataSource * d = [DataSource new];
        if (!d.isConnected) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:kError message:kNetworkConnectionErrorMsg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kOK style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                [refreshControl endRefreshing];
                [self.tblInventoryList reloadData];
            }];
            [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:TRUE completion:nil];
        } else {
            [self.selectedClaim refreshInventoryListWithActivityIndex:0 withCompletion:^(int i){
                [_tblInventoryList reloadData];
                [refreshControl endRefreshing];
            }];
        }
        
    }
}

-(BOOL)checkForValidString:(NSString*)aString
{
    BOOL _isValidFlag = FALSE;
    if (([aString isEqual:[NSNull null]] ||
         [aString isKindOfClass:[NSNull class]] ||
         aString == nil ||
         aString == Nil) || [aString isEqual:@""]){
        _isValidFlag = TRUE;
    }
    return _isValidFlag;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_selectedClaim.inventoryList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    cell = [tableView dequeueReusableCellWithIdentifier:kInventoryListCellID];
    if (cell == nil) {
        cell = [[InventoryListCustomCell alloc] initWithStyle:UITableViewCellStyleDefault
                                              reuseIdentifier:kInventoryListCellID];
    }
    Inventory *inventory = [_selectedClaim.inventoryList getInventoryByIndex:indexPath.row];
    if(inventory.ordinal.integerValue <=9 /*indexPath.row <=8*/){
//        cell.lblQty.text = [NSString stringWithFormat:@"Item:#0%ld",(long)indexPath.row+1];
        cell.lblQty.text = [NSString stringWithFormat:@"Item:#0%ld",inventory.ordinal.integerValue];
    } else {
//        cell.lblQty.text = [NSString stringWithFormat:@"Item:#%ld",(long)indexPath.row+1];
        cell.lblQty.text = [NSString stringWithFormat:@"Item:#%ld",inventory.ordinal.integerValue];
    }
    cell.lblDesc.text  = inventory.originalDescription;
    cell.lblItemNo.text = [NSString stringWithFormat:@"%lu",(unsigned long)inventory.quantity];
    if (inventory.edited) {
        cell.lblEdited.hidden = NO;
    } else {
        cell.lblEdited.hidden = YES;
    }
    cell.QtyLabel.hidden = NO;
    cell.lblDesc.textAlignment = NSTextAlignmentLeft;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIStoryboard *inventoryStoryboard = [UIStoryboard storyboardWithName:kInventoryStoryboard bundle: nil];
    InventoryViewController *inventoryVC = (InventoryViewController*)[inventoryStoryboard instantiateViewControllerWithIdentifier:kInventoryVC];
    inventoryVC.selectedClaim = self.selectedClaim;
    Inventory *selectedInv = [self.selectedClaim.inventoryList getInventoryByIndex:indexPath.row];
    inventoryVC.selectedInventory = selectedInv;
    inventoryVC.editMode = YES;
    inventoryVC.itemNo = selectedInv.ordinal.intValue;
    [self.navigationController pushViewController:inventoryVC animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    Inventory *selectedInv = [self.selectedClaim.inventoryList getInventoryByIndex:indexPath.row];
    if (selectedInv.added) {
        return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Inventory *selectedInv = [self.selectedClaim.inventoryList getInventoryByIndex:indexPath.row];
        [self.selectedClaim.inventoryList removeInventory:selectedInv];
        [tableView reloadData];
    }
}

- (IBAction)actionAddInventory:(id)sender {
    UIStoryboard *inventoryStoryboard = [UIStoryboard storyboardWithName:kInventoryStoryboard bundle: nil];
    InventoryViewController *inventoryVC = (InventoryViewController*)[inventoryStoryboard instantiateViewControllerWithIdentifier:kInventoryVC];
    Inventory *inventory = [[Inventory alloc] init];
    inventory.claimID = _selectedClaim.claimID;
    inventory.claimName = _selectedClaim.claimName;
    inventory.quantity = 1;
    inventory.edited = true;
    inventory.added = true;
    if (_selectedClaim.inventoryList.count > 0) {
        Inventory * lastInv = [_selectedClaim.inventoryList getInventoryByIndex:_selectedClaim.inventoryList.count - 1];
        inventory.ordinal = [lastInv.ordinal decimalNumberByAdding:[NSDecimalNumber decimalNumberWithString:@"1"]];
        inventory.room = lastInv.room;
        inventory.conditionID = 1; // Average
        inventory.index = lastInv.index + 1;
    }
    else {
        inventory.ordinal = [NSDecimalNumber decimalNumberWithString:@"1"];
        inventory.conditionID = 1; // Average
        inventory.index = 0;
    }
    //NEED_FOR_LAUNCH
//    [_selectedClaim.inventoryList addInventory:inventory];
    inventoryVC.selectedClaim = _selectedClaim;
    inventoryVC.editMode = NO;
    inventoryVC.selectedInventory = inventory;
    Inventory *inv = [_selectedClaim.inventoryList getInventoryByIndex:_selectedClaim.inventoryList.count-1];
    inventoryVC.itemNo = inv.ordinal.intValue+1;
    [self.navigationController pushViewController:inventoryVC animated:YES];
}

- (void)drawDashedBorderAroundView:(UIView *)v {
    //border definitions
    CGFloat cornerRadius = 0.0;
    CGFloat borderWidth = 1.0;
    UIColor *lineColor = [UIColor darkGrayColor];
    //drawing
    CGRect frame = v.bounds;
    CAShapeLayer *_shapeLayer = [CAShapeLayer layer];
    //creating a path
    CGMutablePathRef path = CGPathCreateMutable();
    //drawing a border around a view
    CGPathMoveToPoint(path, NULL, 0, frame.size.height - cornerRadius);
    CGPathAddLineToPoint(path, NULL, 0, cornerRadius);
    
    CGPathAddArc(path, NULL, cornerRadius, cornerRadius, cornerRadius, M_PI, -M_PI_2, NO);
    CGPathAddLineToPoint(path, NULL, self.view.frame.size.width - 16, 0);
    
    CGPathAddArc(path, NULL, self.view.frame.size.width - 16, cornerRadius, cornerRadius, -M_PI_2, 0, NO);
    CGPathAddLineToPoint(path, NULL, self.view.frame.size.width - 16, frame.size.height - cornerRadius);
    
    CGPathAddArc(path, NULL, self.view.frame.size.width - 16, frame.size.height - cornerRadius, cornerRadius, 0, M_PI_2, NO);
    CGPathAddLineToPoint(path, NULL, cornerRadius, frame.size.height);
    
    CGPathAddArc(path, NULL, cornerRadius, frame.size.height - cornerRadius, cornerRadius, M_PI_2, M_PI, NO);
    //path is set as the _shapeLayer object's path
    _shapeLayer.path = path;
    CGPathRelease(path);
    _shapeLayer.backgroundColor = [[UIColor clearColor] CGColor];
    _shapeLayer.frame = frame;
    _shapeLayer.masksToBounds = NO;
    [_shapeLayer setValue:[NSNumber numberWithBool:NO] forKey:@"isCircle"];
    _shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    _shapeLayer.strokeColor = [lineColor CGColor];
    _shapeLayer.lineWidth = borderWidth;
    _shapeLayer.lineDashPattern = [NSArray arrayWithObjects:@1, @8, nil];
    _shapeLayer.lineCap = kCALineCapRound;
    //_shapeLayer is added as a sublayer of the view, the border is visible
    [v.layer addSublayer:_shapeLayer];
    v.layer.cornerRadius = cornerRadius;
}

- (IBAction)btnAddressClicked:(id)sender {
    NSString *mapAddress = [@"http://maps.apple.com/?q=" stringByAppendingString:[_selectedClaim.insuredFullAddress stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *url = [NSURL URLWithString:mapAddress];
    UIApplication *app = [UIApplication sharedApplication];
    [app openURL:url];
}

- (IBAction)btnPhoneClicked:(id)sender {
//    if ([self validatePhone:_selectedClaim.insuredPhone]) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_selectedClaim.insuredPhone]];
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
            [[UIApplication sharedApplication] openURL:phoneUrl];
        else {
            [self showAlertControllerWithTitle:@"Error" andMessage:@"Could not place a call" andBtnTitle:@"OK"];
        }
//    }
//    else {
//        [self showAlertControllerWithTitle:@"Error" andMessage:@"Invalid Phone Number" andBtnTitle:@"OK"];
//    }
}

//- (BOOL)validatePhone:(NSString *)phoneNumber
//{
//    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
//    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
//    return [phoneTest evaluateWithObject:phoneNumber];
//}


- (IBAction)btnEmailClicked:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        [mailer setToRecipients:[NSArray arrayWithObjects:_lblEmail.text, nil]];
        [mailer setMessageBody:@"" isHTML:NO];
        [self presentViewController:mailer animated:YES completion:NULL];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Canceled");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Failed");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Savd");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Sent");
            break;
        default:
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)eMailToInsuredButtonTapped:(id)sender {
    DataSource * d = [DataSource new];
    if (!d.isConnected) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:kError message:kNetworkConnectionErrorMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kOK style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            [refreshControl endRefreshing];
        }];
        [cancelAction setValue:kBCOrangeColor forKey:@"titleTextColor"];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:TRUE completion:nil];
//        alert.view.tintColor = kBCOrangeColor;
    }
    else {
        spinner = [[UserData data] makeSpinner];
        [d ctSendEmailToInsured:_selectedClaim async:YES addIt:nil withCompletion:^()
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [[UserData data] removeSpinner:spinner];
             });
         }];
    }
}


- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)notesButtonAction:(id)sender {
    UIStoryboard *notesStoryboard = [UIStoryboard storyboardWithName:kClaimNotesStoryboard bundle: nil];
    ClaimNotesListViewController *claimNotesListVC = (ClaimNotesListViewController*)[notesStoryboard instantiateViewControllerWithIdentifier:kClaimNotesListVC];
    claimNotesListVC.claim = _selectedClaim;
    [self.navigationController pushViewController:claimNotesListVC animated:YES];
    
}

- (void)showAlertControllerWithTitle:(NSString*)alertTitle andMessage:(NSString*)alertMsg andBtnTitle:(NSString*)btnTitle {
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:alertTitle message:alertMsg preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *closeButton = [UIAlertAction actionWithTitle:btnTitle style:UIAlertActionStyleCancel handler: ^(UIAlertAction * action) {
        
    }];
    [closeButton setValue:kBCOrangeColor forKey:@"titleTextColor"];
    [alertView addAction:closeButton];
    [self presentViewController: alertView animated:YES completion:nil];
//    alertView.view.tintColor = kBCOrangeColor;
}

@end
