//
//  InventoryListCustomCell.h
//  ClaimTracker
//
//  Created by Prajoth Antonio D on 11/08/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InventoryListCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblQty;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblItemNo;
@property (weak, nonatomic) IBOutlet UILabel *QtyLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblEdited;
@end
