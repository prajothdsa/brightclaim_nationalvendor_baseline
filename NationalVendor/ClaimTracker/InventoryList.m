//
//  InventoryList.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/18/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "InventoryList.h"
#import "DataSource.h"

@interface InventoryList()

@property NSMutableArray *inventoryArray;

@end

@implementation InventoryList

- (InventoryList *) init{
    self = [super init];
    self.inventoryArray = [[NSMutableArray alloc] init];
    return self;
}

- (NSUInteger)count{
    return [self.inventoryArray count];
}

- (Inventory *)getInventoryByIndex:(NSUInteger)index{
    return [self.inventoryArray objectAtIndex:index];
}

- (Inventory *)getInventoryByID:(NSUInteger)inventoryID{
    for (int i = 0; i < self.inventoryArray.count; i++){
        Inventory *inventory = [self getInventoryByIndex:i];
        if (inventory.inventoryID == inventoryID){
            return inventory;
        }
    }
    return nil;
}

- (void)addInventory:(Inventory *) inventory{
    [self.inventoryArray addObject:inventory];
}

- (void)clear{
    [self.inventoryArray removeAllObjects];
}

- (void)removeInventory:(Inventory *)inventory
{
    [inventory.photoList clear];
    [self.inventoryArray removeObject:(inventory)];

    // Renumber Inventory
//    for (NSUInteger i = 0; i  < self.inventoryArray.count; i++){
//        Inventory *currInventory = self.inventoryArray[i];
//        currInventory.index = i;
//
//        NSDecimalNumber *newOrdinal = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lu.0",(unsigned long)i+1]];
//        if (![currInventory.ordinal isEqualToValue:newOrdinal]){
//            currInventory.ordinal = newOrdinal;
//        }
//    }
}

- (void) encodeWithCoder:(NSCoder *)encoder{
    //[encoder encodeInt64:self.claimID forKey:@"claimID"];
    [encoder encodeObject:self.inventoryArray forKey:@"inventoryArray"];
}

- (id) initWithCoder:(NSCoder *)decoder{
    self = [super init];
    if (self) {
        //self.claimID = [decoder decodeInt64ForKey:@"claimID"];
        self.inventoryArray = [decoder decodeObjectForKey:@"inventoryArray"];
        if (!self.inventoryArray) {
            self.inventoryArray = [[NSMutableArray alloc] init];
        }
    }
    return self;
}

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id *)buffer count:(NSUInteger)len{
    return [self.inventoryArray countByEnumeratingWithState:state objects:buffer count:len];
}

- (NSInteger) needToSyncCount{
    NSInteger c = 0;
 
    for (Inventory *inventory in self.inventoryArray)
    {
        if (inventory.edited == TRUE) { c++; }
        
        for (Photo *photo in inventory.photoList)
        {
            if (photo.added == TRUE) { c++; }
        }
    }
    return c;
}

@end
