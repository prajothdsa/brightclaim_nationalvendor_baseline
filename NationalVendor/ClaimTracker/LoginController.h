//
//  LoginViewController.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 4/30/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITextField+Extended.h"

@interface LoginController : UIViewController<UITextFieldDelegate>

@property NSString *email;
@property NSString *password;

@end
