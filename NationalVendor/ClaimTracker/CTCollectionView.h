//
//  CTCollectionView.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 6/24/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Claim.h"
#import "Inventory.h"

@interface CTCollectionView : UICollectionView <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property Claim *claim;
@property Inventory *inventory;
@property UIViewController *presenterViewController;
- (IBAction)showImagePickerForCamera:(id)sender;
- (IBAction)showImagePickerForPhotoPicker:(id)sender;

@end
