//
//  Claim.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/7/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "Claim.h"
#import "DataSource.h"

@implementation Claim

- (void) encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.index] forKey:@"claimIndex"];
    [encoder encodeObject:[NSNumber numberWithUnsignedInteger:self.claimID] forKey:@"claimID"];
    [encoder encodeObject:self.claimName forKey:@"claimName"];
    [encoder encodeObject:self.insuredFullAddress forKey:@"insuredFullAddress"];
    [encoder encodeObject:self.insuredAddress1 forKey:@"insuredAddress1"];
    [encoder encodeObject:self.insuredAddress2 forKey:@"insuredAddress2"];
    [encoder encodeObject:self.insuredCity forKey:@"insuredCity"];
    [encoder encodeObject:self.insuredState forKey:@"insuredState"];
    [encoder encodeObject:self.insuredZip forKey:@"insuredZip"];
    [encoder encodeObject:self.insuredEMail forKey:@"insuredEMail"];
    [encoder encodeObject:self.insuredPhone forKey:@"insuredPhone"];
    [encoder encodeObject:self.lossType forKey:@"lossType"];
    [encoder encodeObject:self.coverageType forKey:@"coverageType"];
    [encoder encodeObject:self.deductible forKey:@"deductible"];
    [encoder encodeObject:self.overallLimit forKey:@"overallLimit"];
    [encoder encodeObject:self.claimNoteList forKey:@"claimNoteList"];
    [encoder encodeObject:self.inventoryList forKey:@"inventoryList"];
}

- (id) initWithCoder:(NSCoder *)decoder
{
    self = [self init];
    if (self){
        self.index = [[decoder decodeObjectForKey:@"claimIndex"] unsignedIntegerValue];
        self.claimID = [[decoder decodeObjectForKey:@"claimID"] unsignedIntegerValue];
        self.claimName = [decoder decodeObjectForKey:@"claimName"];
        self.insuredFullAddress = [decoder decodeObjectForKey:@"insuredFullAddress"];
        self.insuredAddress1 = [decoder decodeObjectForKey:@"insuredAddress1"];
        self.insuredAddress2 = [decoder decodeObjectForKey:@"insuredAddress2"];
        self.insuredCity = [decoder decodeObjectForKey:@"insuredCity"];
        self.insuredState = [decoder decodeObjectForKey:@"insuredState"];
        self.insuredZip = [decoder decodeObjectForKey:@"insuredZip"];
        self.insuredEMail = [decoder decodeObjectForKey:@"insuredEMail"];
        self.insuredPhone = [decoder decodeObjectForKey:@"insuredPhone"];
        self.lossType = [decoder decodeObjectForKey:@"lossType"];
        self.coverageType = [decoder decodeObjectForKey:@"coverageType"];
        self.deductible = [decoder decodeObjectForKey:@"deductible"];
        self.overallLimit = [decoder decodeObjectForKey:@"overallLimit"];
        self.claimNoteList = [decoder decodeObjectForKey:@"claimNoteList"];
        self.inventoryList = [decoder decodeObjectForKey:@"inventoryList"];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (!self.inventoryList) self.inventoryList = [[InventoryList alloc] init];
    if (!self.claimNoteList) self.claimNoteList = [[ClaimNoteList alloc] init];
    return self;
}

- (void)refreshClaimWithCompletion:(void(^)())handler
{
    DataSource * d = [DataSource new];
    [d ctGetClaim:(Claim *)self withCompletion:handler];
}

- (void)refreshClaimNotesWithCompletion:(void(^)())handler
{
    DataSource * d = [DataSource new];
    [d ctGetClaimNotesForClaim:(Claim *)self withCompletion:handler];
}

- (void)refreshInventoryListWithActivityIndex:(int)i withCompletion:(void(^)(int))handler
{
    DataSource * d = [DataSource new];
    [d ctGetInventoryForClaim:(Claim *)self withActivityIndex:0 withCompletion:handler];
}

- (NSInteger) needToSyncCount
{
    NSInteger c = 0;
    if (self.edited == TRUE) { c++; }
    
    for (Inventory *inventory in self.inventoryList)
    {
        if (inventory.edited == TRUE) { c++; }
        
        for (Photo *photo in inventory.photoList)
        {
            if (photo.added == TRUE) { c++; }
        }
    }
    return c;
}

- (NSArray *)getRoomList
{
    NSMutableArray * arr;
    arr = [[NSMutableArray alloc] init];
    
    for (Inventory *inv in self.inventoryList)
    {
        bool found = false;
        for (NSString *room in arr)
        {
            if ([inv.room isEqualToString:room])
            {
                found = true;
                break;
            }
        }
        if ((!found) && (inv.room))
        {
            [arr addObject:inv.room];
        }
    }
    return [arr sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

@end
