//
//  CTNavigationController.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/19/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Claim.h"
#import "Inventory.h"

@interface CTNavigationController : UINavigationController

@property Claim * selectedClaim;
@property Inventory * selectedInventory;
@property ClaimNote * selectedClaimNote;
@property NSString *selectedText;

@end
