//
//  UserData.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 4/30/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClaimList.h"
#import "ConditionList.h"

@interface UserData : NSObject

@property (nonatomic) BOOL isAuthenticated;
@property (nonatomic) NSString *lastLogin;
@property (nonatomic) NSString *password;
@property (nonatomic) NSString *fullName;
@property (nonatomic) NSString *repName;
@property (nonatomic) NSString *repPhone;
@property (nonatomic) NSString *repEMail;
@property ClaimList *claims;
@property ConditionList *conditions;
@property Claim *claim;

- (NSInteger) needToSyncCount;
- (NSMutableArray *) getSyncItems;
- (NSInteger) needToSyncCountForClaimID:(NSUInteger)claimID;
- (NSInteger) unsyncedNotesForClaimID:(NSUInteger)claimID;

- (void)refreshWithCompletion:(void(^)())handler;
- (void)clearUserData;
- (void)clearClaimData;

- (void)loadClaims;
- (void)saveClaims;
- (void)refreshClaimsWithCompletion:(void(^)())handler;
- (void)syncToServerWithTableView:(UITableView *)table andArray:(NSMutableArray *)array;

- (void)loadConditions;
- (void)saveConditions;
- (void)refreshConditions;

+ (UserData *)data;

- (UIView *)makeSpinner;
- (void)removeSpinner:(UIView *)activityView;

- (BOOL)authenticateUser:(NSString *)userName andPassword:(NSString *)password;

@end
