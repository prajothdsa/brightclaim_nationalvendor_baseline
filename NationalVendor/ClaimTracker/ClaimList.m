//
//  ClaimList.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/7/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "ClaimList.h"
#import "UserData.h"

@interface ClaimList()

@property NSMutableArray *claimArray;

@end

@implementation ClaimList

- (ClaimList *) init{
    self = [super init];
    self.claimArray = [[NSMutableArray alloc] init];
    return self;
}

- (NSUInteger)count{
    return [self.claimArray count];
}

- (Claim *)getClaimByIndex:(NSUInteger)index{
    return [self.claimArray objectAtIndex:index];
}

- (Claim *)getClaimByID:(NSUInteger)claimID{
    for (int i = 0; i < self.claimArray.count; i++){
        Claim *claim = [self getClaimByIndex:i];
        if (claim.claimID == claimID){
            return claim;
        }
    }
    return nil;
}

- (void)addClaim:(Claim *) claim{
    [self.claimArray addObject:claim];
}

- (void) clear{
    [self.claimArray removeAllObjects];
}

- (void) encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self.claimArray forKey:@"claimArray"];
    [encoder encodeBool:self.edited forKey:@"edited"];
}

- (id) initWithCoder:(NSCoder *)decoder{
    self = [super init];
    self.edited = [decoder decodeBoolForKey:@"edited"];
    if (self){
        self.claimArray = [decoder decodeObjectForKey:@"claimArray"];
    }
    return self;
}

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id *)buffer count:(NSUInteger)len{
    return [self.claimArray countByEnumeratingWithState:state objects:buffer count:len];
}

@end
