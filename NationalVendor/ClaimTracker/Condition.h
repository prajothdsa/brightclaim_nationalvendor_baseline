//
//  Condition.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 8/2/16.
//  Copyright © 2016 com.nationalvendor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Condition : NSObject

@property NSUInteger conditionID;
@property NSString *conditionName;
@property NSUInteger index;

@property BOOL added;

- (void) encodeWithCoder:(NSCoder *)encoder;
- (id) initWithCoder:(NSCoder *)decoder;

@end
