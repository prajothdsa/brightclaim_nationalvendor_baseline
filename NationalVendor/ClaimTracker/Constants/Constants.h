//
//  Constants.h
//  ClaimTracker
//
//  Created by Prajoth Antonio D on 8/9/17.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//


#import "AppDelegate.h"

#define APP_DELEGATE  (AppDelegate *)[[UIApplication sharedApplication] delegate]
#define IS_NOT_NIL_OR_NULL(value)   (value != nil && value != Nil && value != NULL && value != (id)[NSNull null] && ![value isKindOfClass: [NSNull class]])

/*TEST SERVER*/
//#define kServerPath             @"http://10.0.0.18:8080/telemedicine/services/rest/patient"

/*CLIENT NEW SERVER*/
#define kServerPath             @"http://www.knocknocdoc.com:8080/telemedicine-patientApp/services/rest/patient" /*New URL*/ /*75.114.64.51*/


//-------------------------- Web Service Names-------------------------------

//-------------------------- Color Constants -------------------------------
#define kBCOrangeColor              [UIColor colorWithRed:(210.0/255.0) green:(62.0/255.0) blue:(2.0/255.0) alpha:1]
#define kBCOrangeSelectedColor      [UIColor colorWithRed:(210.0/255.0) green:(62.0/255.0) blue:(2.0/255.0) alpha:1.0]
#define kBCWhiteSelectedColor       [UIColor colorWithRed:(243.0/255.0) green:(180.0/255.0) blue:(150.0/255.0) alpha:1.0]
#define kBCPickerItemColor          [UIColor colorWithRed:(145.0/255.0) green:(145.0/255.0) blue:(145.0/255.0) alpha:1.0]

// Fonts
#define kHelveticaRegularFontName                   @"Helvetica-Regular"
#define kSPECIAL_CHAR                               @"}"
#define HELVETICA_NAME                              @"Helvetica"
#define HELVETICA_BOLD_NAME                         @"Helvetica Bold"
#define HELVETICA_REGULAR                           @"Helvetica Regular"

#define VALID_STRING(val) \
((val == nil) || (val.length == 0) || [val isKindOfClass: [NSNull class]]) ? @"" : val

//RGB color macro
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define NAV_BG_COLOR                UIColorFromRGB(0x888888)
#define LIGHT_GRAY_COLOR            UIColorFromRGB(0x6E6E6E)


//------------------------ Storyboard Names ------------------------------------//
#define REGISTRATION_STORYBOARD_NAME                @"Registration"
#define HISTORY_STORYBOARD_NAME                     @"History"
#define CHAT_WINDOW_STORYBOARD_NAME                 @"ChatWindow"
#define CONTACT_USER_PROFILE_STORYBOARD_NAME        @"ContactUserProfile"

//------------------------ Storyboard Names ------------------------------------//
#define kClaimsStoryboard       @"Claims"
#define kLoginStoryboard        @"Login"
#define kClaimInfoStoryboard    @"ClaimInfo"
#define kClaimNotesStoryboard   @"ClaimNote"
#define kInventoryStoryboard    @"Inventory"

//------------------------ View Controllers ------------------------------------//
#define kClaimsVC           @"claimsVC"
#define kLoginVC            @"LoginScreen"
#define kClaimInfoVC        @"claimInfoVC"
#define kClaimNotesListVC   @"ClaimNotesListVC"
#define kInventoryVC        @"inventoryVC"
#define kClaimNotesDetailsVC @"ClaimNotesDetailsVC"

//------------------------ Custom Cell Identifiers ------------------------------------//
#define kClaimInfoCellID        @"claimInfoCell"
#define kInventoryListCellID    @"inventoryListCell"

//------------------------ Alert Messages ------------------------------------//
#define kError                  @"Error"
#define kCancel                 @"Cancel"
#define kWarning                @"Warning"
#define kClose                  @"Close"
#define kEnterLoginNameMsg      @"Please enter your Username"
#define kEnterPasswordMsg       @"Please enter your Password"
#define kEmailValidationMsg     @"E-Mail Address cannot contain the %@ character"
#define kPasswordValidationMsg  @"Password cannot contain the %@ character"
#define kNetworkConnectionErrorMsg @"You are not connected to the network"
#define kOK                         @"OK"
#define kNoDataMsg                  @"No data is currently available.  Please pull down to refresh."
#define kDateFormat                 @"MM/dd/yyyy"
#define kEnterNote                  @"Please enter a note"
#define kExceedLimit                @"Exceeded maximum limit of note length"

//------------------------ Claim Note Module  ------------------------------------//
#define kNewNote @"New Note"
#define kViewNote @"View Note"





