//
//  ConditionList.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 8/2/16.
//  Copyright © 2016 com.nationalvendor. All rights reserved.
//

#import "ConditionList.h"
#import "DataSource.h"

@interface ConditionList()

@property NSMutableArray *conditionArray;

@end

@implementation ConditionList

- (ConditionList *) init
{
    self = [super init];
    self.conditionArray = [[NSMutableArray alloc] init];
    return self;
}

- (NSUInteger)count
{
    return [self.conditionArray count];
}

- (Condition *)getConditionByIndex:(NSUInteger)index
{
    return [self.conditionArray objectAtIndex:index];
}

- (Condition *)getConditionByID:(NSUInteger) id{
    for (int i = 0; i < _conditionArray.count; i++){
        Condition* c = [self.conditionArray objectAtIndex:i];
        if (c.conditionID == id){
            return c;
            break;
        }
    }
    return nil;
}

- (Condition *)getConditionByName:(NSString*) name{
    for (int i = 0; i < _conditionArray.count; i++){
        Condition* c = [self.conditionArray objectAtIndex:i];
        if ([c.conditionName isEqualToString:name] == YES){
            return c;
            break;
        }
    }
    return nil;
}

- (void)addCondition:(Condition *) condition
{
    [self.conditionArray addObject:condition];
}

- (void)removeCondition:(Condition *) condition
{
    [self.conditionArray removeObject:condition];
}

- (void)clear
{
    [self.conditionArray removeAllObjects];
}

- (void) encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.conditionArray forKey:@"conditionArray"];
}

- (id) initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.conditionArray = [decoder decodeObjectForKey:@"conditionArray"];
        if (!self.conditionArray) {
            self.conditionArray = [[NSMutableArray alloc] init];
        }
    }
    return self;
}

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id *)buffer count:(NSUInteger)len
{
    return [self.conditionArray countByEnumeratingWithState:state objects:buffer count:len];
}
@end
