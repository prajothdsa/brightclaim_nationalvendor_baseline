//
//  UITextFieldExt.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 6/4/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Extended)

@property(retain, nonatomic)UITextField* nextTextField;

@end
