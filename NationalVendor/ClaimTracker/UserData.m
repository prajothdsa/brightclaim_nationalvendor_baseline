//
//  UserData.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 4/30/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UserData.h"
#import "SSKeyChain.h"
#import "DataSource.h"
#import "AppDelegate.h"
#import "Constants.h"

@interface UserData()

@property int syncCount;

@end

@implementation UserData

NSString * const KeyIsAuthenticated = @"com.ClaimTracker:isAuthenticated";
NSString * const KeyLastLogin = @"com.ClaimTracker:LastLogin";
NSString * const KeyFullName = @"com.ClaimTracker:FullName";
NSString * const KeyRepName = @"com.ClaimTracker:RepName";
NSString * const KeyRepPhone = @"com.ClaimTracker:RepPhone";
NSString * const KeyRepEMail = @"com.ClaimTracker:RepEMail";
NSString * const KeyClaims = @"com.ClaimTracker:Claims";
NSString * const KeyConditions = @"com.ClaimTracker:Conditions";

- (BOOL)authenticateUser:(NSString *)userName andPassword:(NSString *)password {
    @try {
        DataSource * d = [DataSource new];
        if ([d isConnected]) {
            if (![d ctAuthenticateUser:userName andPassword:password]){
                [UserData data].password = nil;
                return false;
            }
            else{
                [UserData data].lastLogin = userName;
                [UserData data].password = password;
                return true;
            }
        }
        else {
            if (![UserData data].password)
                return false;
            else if ([UserData data].password == password)
                return true;
            else
                return false;
        }
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)clearUserData{
    [SSKeychain deletePasswordForService:KCService account:KCAccount];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KeyIsAuthenticated];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KeyFullName];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KeyRepName];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KeyRepPhone];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KeyRepEMail];
}

- (void)clearClaimData{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KeyClaims];
    self.claims = nil;
}

- (void)saveClaims {
    @try {
        NSData *encodedClaims = [NSKeyedArchiver archivedDataWithRootObject:self.claims];
        [[NSUserDefaults standardUserDefaults] setObject:encodedClaims forKey:KeyClaims];
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)loadClaims {
    @try {
        NSData *encodedList = [[NSUserDefaults standardUserDefaults] objectForKey:KeyClaims];
        ClaimList *list = (ClaimList *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedList];
        if (!list) {
            list = [[ClaimList alloc] init];
        }
        self.claims = list;
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)refreshClaimsWithCompletion:(void(^)())handler {
    @try {
        DataSource * d = [DataSource new];
        [d ctGetClaimsWithCompletion:handler];
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)loadConditions {
    NSData *encodedList = [[NSUserDefaults standardUserDefaults] objectForKey:KeyConditions];
    ConditionList *list = (ConditionList *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedList];
    if (!list) {
        list = [[ConditionList alloc] init];
    }
    self.conditions = list;
}

- (void)refreshConditions {
    DataSource * d = [DataSource new];
    [d ctGetConditionsWithCompletion:^(void){
        [self saveConditions];
    }];
}

- (void)saveConditions {
    NSData *encodedConditions = [NSKeyedArchiver archivedDataWithRootObject:self.conditions];
    [[NSUserDefaults standardUserDefaults] setObject:encodedConditions forKey:KeyConditions];
}

- (NSInteger) needToSyncCount {
    @try {
        NSInteger c = 0;
        for (Claim *claim in self.claims)
        {
            if (claim.edited == TRUE) { c++; }

            for (Inventory *inventory in claim.inventoryList)
            {
                if (inventory.edited == TRUE) { c++; }

                for (Photo *photo in inventory.photoList)
                {
                    if (photo.added == TRUE) { c++; }
                }
            }
            
            for(ClaimNote *claimNote in claim.claimNoteList)
            {
                if (claimNote.edited == TRUE) { c++; }
            }
            
        }
        return c;
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (NSInteger) needToSyncCountForClaimID:(NSUInteger)claimID {
    NSInteger c = 0;
    Claim *claim = [self.claims getClaimByID:claimID];
    if (claim.edited == TRUE) { c++; }
    for (Inventory *inventory in claim.inventoryList)
    {
        if (inventory.edited == TRUE) { c++; }
        
        for (Photo *photo in inventory.photoList)
        {
            if (photo.added == TRUE) { c++; }
        }
    }
    return c;
}

- (NSInteger) unsyncedNotesForClaimID:(NSUInteger)claimID {
    NSInteger c = 0;
    Claim *claim = [self.claims getClaimByID:claimID];
    if (claim.edited == TRUE) { c++; }
    for (ClaimNote *claimNote in claim.claimNoteList) {
        if (claimNote.edited == TRUE) { c++; }
    }
    return c;
}

- (NSMutableArray *)getSyncItems {
    @try {
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        for (Claim *claim in self.claims)
        {
            if (claim.edited == TRUE)
            {
                [arr addObject:claim];
            }
            
            for (Inventory *inventory in claim.inventoryList)
            {
                if (inventory.edited == TRUE)
                {
                    [arr addObject:inventory];
                }

                for (Photo *photo in inventory.photoList)
                {
                    if (photo.added == TRUE)
                    {
                        [arr addObject:photo];
                    }
                }
            }
            for(ClaimNote *claimNote in claim.claimNoteList){
                
                if(claimNote.edited == TRUE){
                    [arr addObject:claimNote];
                }
            }
        }
        return arr;
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
    }
}

- (void)syncToServerWithTableView:(UITableView *)table andArray:(NSMutableArray *)array {
    DataSource * d = [DataSource new];
    NSMutableArray *arrInvSave = [[NSMutableArray alloc] init];
    NSMutableArray *arrInvAdd = [[NSMutableArray alloc] init];
    NSMutableArray *arrPhotoExistingInv = [[NSMutableArray alloc] init];
    NSMutableArray *arrNewNotes = [[NSMutableArray alloc] init];
    @try {
        [self saveClaims];
        
        for (int i = 0; i < array.count; i++) {
            if ([[array objectAtIndex:i] isKindOfClass:[Claim class]])
            {
                //Claim *claim = (Claim *)[array objectAtIndex:i];
            }
            else if ([[array objectAtIndex:i] isKindOfClass:[Inventory class]]) // Add Inventory to the array to update
            {
                Inventory *inventory = (Inventory *)[array objectAtIndex:i];

                if (inventory.inventoryID) { [arrInvSave addObject:inventory]; } else { [arrInvAdd addObject:inventory]; } // Either Add New or Edit Existing
            }
            else if ([[array objectAtIndex:i] isKindOfClass:[Photo class]])
            {
                Photo *photo = (Photo *)[array objectAtIndex:i];
                
                if (photo.inventoryID) { [arrPhotoExistingInv addObject:photo]; } // Only keeping track of Photos on Existing Inventory in an array - Photos on New Inventory will all be uploaded
            }
            else if ([[array objectAtIndex:i] isKindOfClass:[ClaimNote class]]){
                ClaimNote *claimNote = (ClaimNote *)[array objectAtIndex:i];
                if(claimNote.claimID)
                {
                    [arrNewNotes addObject:claimNote];
                }
            }
        }
        
        dispatch_queue_t ctQueue = dispatch_queue_create("ctUploadQueue", NULL);

        if ((arrInvSave.count > 0) || (arrInvAdd.count > 0) || (arrPhotoExistingInv.count > 0) || (arrNewNotes.count > 0))
        {
            UIView *spinner;
            for (Inventory *inventory in arrInvSave)
            {
                if (self.syncCount == 0) spinner = [self makeSpinner];
                self.syncCount++;
                dispatch_async(ctQueue, ^{
                    [d ctUploadInventory:inventory async:false addIt:FALSE withCompletion:^(int i)
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             self.syncCount--;
                             [table reloadData];
                             if (self.syncCount == 0 && i!=333) {
                                 [[NSNotificationCenter defaultCenter]postNotificationName:@"SyncToServerDone" object:nil];
                             }
                             if (self.syncCount == 0)
                                 [self removeSpinner:spinner];
                         });
                     }];
                });
            }
            
            for (Inventory *inventory in arrInvAdd)
            {
                if (self.syncCount == 0) spinner = [self makeSpinner];
                self.syncCount++;
                dispatch_async(ctQueue, ^{
                    [d ctUploadInventory:inventory async:false addIt:TRUE withCompletion:^(int i)
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             self.syncCount--;
                             [table reloadData];
                             if (self.syncCount == 0 && i!=333) {
                                 [[NSNotificationCenter defaultCenter]postNotificationName:@"SyncToServerDone" object:nil];
                             }
                             if (self.syncCount == 0) [self removeSpinner:spinner];
                         });
                     }];
                });
                
                for (Photo *photo in inventory.photoList)
                {
                    self.syncCount++;
                    photo.inventoryID = inventory.inventoryID;
                    dispatch_async(ctQueue, ^{
                        [d ctUploadPhoto:photo async:false withCompletion:^(int i)
                         {
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 self.syncCount--;
                                 [table reloadData];
                                 if (self.syncCount == 0 && i!=333) {
                                     [[NSNotificationCenter defaultCenter]postNotificationName:@"SyncToServerDone" object:nil];
                                 }
                                 if (self.syncCount == 0) [self removeSpinner:spinner];
                             });
                         }];
                    });
                }
            }
            
            for (Photo *photo in arrPhotoExistingInv)
            {
                if (self.syncCount == 0) spinner = [self makeSpinner];
                self.syncCount++;
                dispatch_async(ctQueue, ^{
                    [d ctUploadPhoto:photo async:false withCompletion:^(int i)
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             self.syncCount--;
                             [table reloadData];
                             if (self.syncCount == 0 && i!=333) {
                                 [[NSNotificationCenter defaultCenter]postNotificationName:@"SyncToServerDone" object:nil];
                             }
                             if (self.syncCount == 0) [self removeSpinner:spinner];
                         });
                     }];
                });
            }
            
            
            for (ClaimNote *claimNote in arrNewNotes)
            {
                if (self.syncCount == 0) spinner = [self makeSpinner];
                self.syncCount++;
                dispatch_async(ctQueue, ^{
                    [d ctUploadNotes:claimNote async:YES addIt:nil withCompletion:^(int i)
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             self.syncCount--;
                             [table reloadData];
                             if (self.syncCount == 0 && i!=333) {
                                 [[NSNotificationCenter defaultCenter]postNotificationName:@"SyncToServerDone" object:nil];
                             }
                             if (self.syncCount == 0) [self removeSpinner:spinner];
                         });
                     }];
                });
            }
            
            
        }
    } @catch (NSException *exception) {
        [self showAlertControllerWithTitle:@"Error" andMessage:exception.reason andBtnTitle:kClose];
    } @finally {
        [self saveClaims];
    }

    /*if ((arrInvSave.count > 0) || (arrInvAdd.count > 0) || (arrPhotoExistingInv.count > 0))
    {
        UIView *spinner;
        for (Inventory *inventory in arrInvSave)
        {
            if (self.syncCount == 0) spinner = [self makeSpinner];
            self.syncCount++;
            [d ctUploadInventory:inventory async:true addIt:FALSE withCompletion:^()
            {
                self.syncCount--;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [table reloadData];
                    if (self.syncCount == 0) [self removeSpinner:spinner];
                });
            }];
        }

        for (Inventory *inventory in arrInvAdd)
        {
            if (self.syncCount == 0) spinner = [self makeSpinner];
            self.syncCount++;
            [d ctUploadInventory:inventory async:true addIt:TRUE withCompletion:^()
            {
                self.syncCount--;
                [table reloadData];

                for (Photo *photo in inventory.photoList)
                {
                    self.syncCount++;
                    photo.inventoryID = inventory.inventoryID;
                    [d ctUploadPhoto:photo async:true withCompletion:^()
                    {
                        self.syncCount--;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [table reloadData];
                            if (self.syncCount == 0) [self removeSpinner:spinner];
                        });
                    }];
                }
                if (self.syncCount == 0) [self removeSpinner:spinner];
            }];
        }

        for (Photo *photo in arrPhotoExistingInv)
        {
            if (self.syncCount == 0) spinner = [self makeSpinner];
            self.syncCount++;
            [d ctUploadPhoto:photo async:true withCompletion:^()
            {
                self.syncCount--;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [table reloadData];
                    if (self.syncCount == 0) [self removeSpinner:spinner];
                });
            }];
        }
    }*/
}

- (UIView *)makeSpinner {
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    UIWindow *window = delegate.window;
    UIView *activityView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, window.bounds.size.width, window.bounds.size.height)];
    activityView.backgroundColor = [UIColor blackColor];
    activityView.alpha = 0.5;
    
    UIActivityIndicatorView *ac = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((activityView.bounds.size.width / 2) - 12.0, window.bounds.size.height / 2, 24, 24)];
    ac.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    ac.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    [activityView addSubview:ac];
    [window addSubview:activityView];
    [[[activityView subviews] objectAtIndex:0] startAnimating];
    return activityView;
}

- (void)removeSpinner:(UIView *)activityView{
    [[[activityView subviews] objectAtIndex:0] stopAnimating];
    [activityView removeFromSuperview];
}

+ (UserData *)data {
    static UserData *_data = nil;
    static dispatch_once_t onceSecurePredicate;
    dispatch_once(&onceSecurePredicate, ^{
        _data = [[self alloc] init];
    });
    return _data;
}

- (void)refreshWithCompletion:(void(^)())handler
{
    DataSource * d = [DataSource new];
    [d ctGetUserDataWithCompletion:handler];
}

@synthesize isAuthenticated = _isAuthenticated;
- (void)setIsAuthenticated:(BOOL)isAuthenticated
{
    [[NSUserDefaults standardUserDefaults] setBool:isAuthenticated forKey:KeyIsAuthenticated];
}
- (BOOL)isAuthenticated
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:KeyIsAuthenticated];
}

@synthesize lastLogin = _lastLogin;
- (void)setLastLogin:(NSString *)lastLogin
{
    [[NSUserDefaults standardUserDefaults] setObject:lastLogin forKey:KeyLastLogin];
}
- (NSString *)lastLogin
{
    return (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:KeyLastLogin];
}

@synthesize password = _password;
- (void)setPassword:(NSString *)password
{
    [SSKeychain setPassword:password forService:KCService account:KCAccount];
}
- (NSString *)password
{
    return [SSKeychain passwordForService:KCService account:KCAccount];
}

@synthesize fullName = _fullName;
- (void)setFullName:(NSString *)fullName
{
    [[NSUserDefaults standardUserDefaults] setObject:fullName forKey:KeyFullName];
}
- (NSString *)fullName
{
    return (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:KeyFullName];
}

@synthesize repName = _repName;
- (void)setRepName:(NSString *)repName
{
    [[NSUserDefaults standardUserDefaults] setObject:repName forKey:KeyRepName];
}
- (NSString *)repName
{
    return (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:KeyRepName];
}

@synthesize repPhone = _repPhone;
- (void)setRepPhone:(NSString *)repPhone
{
    [[NSUserDefaults standardUserDefaults] setObject:repPhone forKey:KeyRepPhone];
}
- (NSString *)repPhone
{
    return (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:KeyRepPhone];
}

@synthesize repEMail = _repEMail;
- (void)setRepEMail:(NSString *)repEMail
{
    [[NSUserDefaults standardUserDefaults] setObject:repEMail forKey:KeyRepEMail];
}
- (NSString *)repEMail
{
    return (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:KeyRepEMail];
}

- (void)showAlertControllerWithTitle:(NSString*)alertTitle andMessage:(NSString*)alertMsg andBtnTitle:(NSString*)btnTitle {
    UIWindow *keyWindow = [[UIApplication sharedApplication]keyWindow];
    UIViewController *mainController = [keyWindow rootViewController];
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:alertTitle message:alertMsg preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *closeButton = [UIAlertAction actionWithTitle:btnTitle style:UIAlertActionStyleCancel handler: ^(UIAlertAction * action) {
        
    }];
    [closeButton setValue:kBCOrangeColor forKey:@"titleTextColor"];
    [alertView addAction:closeButton];
    [mainController presentViewController: alertView animated:YES completion:nil];
    
}


@end
