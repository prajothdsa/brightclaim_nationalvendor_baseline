//
//  Claim.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/7/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClaimNoteList.h"
#import "InventoryList.h"

@interface Claim : NSObject

@property NSUInteger index;
@property NSUInteger claimID;
@property NSString *claimName;
@property NSString *insuredFullAddress;
@property NSString *insuredAddress1;
@property NSString *insuredAddress2;
@property NSString *insuredCity;
@property NSString *insuredState;
@property NSString *insuredZip;
@property NSString *insuredEMail;
@property NSString *insuredPhone;
@property NSString *lossType;
@property NSString *coverageType;
@property NSDecimalNumber *deductible;
@property NSDecimalNumber *overallLimit;
@property ClaimNoteList *claimNoteList;
@property InventoryList *inventoryList;

@property BOOL edited;
@property BOOL added;

- (void) encodeWithCoder:(NSCoder *)encoder;
- (id) initWithCoder:(NSCoder *)decoder;

- (void)refreshClaimWithCompletion:(void(^)())handler;
- (void)refreshClaimNotesWithCompletion:(void(^)())handler;
- (void)refreshInventoryListWithActivityIndex:(int)i withCompletion:(void(^)(int))handler;
- (NSArray *)getRoomList;

- (NSInteger) needToSyncCount;

@end
