//
//  UIButton+Extended.h
//  ClaimTracker
//
//  Created by Prajoth Antonio D on 04/09/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Extended)
- (UIImage *)imageForButtonWithColor:(UIColor *)color;
@end
