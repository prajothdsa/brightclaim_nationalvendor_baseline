//
//  PHAsset+Extended.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 6/22/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <Photos/Photos.h>

@interface PHAsset (Extended)

+ (PHAsset *)fetchAssetWithALAssetURL:(NSURL *)url;
+ (void)addImageToCTAlbum:(UIImage *)image ForClaim:(NSString *)claim withCompletion:(void(^)())handler;

@end
