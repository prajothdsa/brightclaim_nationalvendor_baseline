//
//  InventoryInfoController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/19/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import "InventoryInfoController.h"
#import "CTTabBarController.h"
#import "InventoryListController.h"
#import "CTNavigationController.h"
#import "PickARoomViewController.h"
#import "PickAConditionViewController.h"
#import "UserData.h"
#import "AppDelegate.h"

@interface InventoryInfoController()

@property (weak, nonatomic) IBOutlet UILabel *lblClaim;
@property (weak, nonatomic) IBOutlet UILabel *lblItem;
@property (weak, nonatomic) IBOutlet UITextField *txtQuantity;
@property (weak, nonatomic) IBOutlet UITextField *txtRoom;
@property (weak, nonatomic) IBOutlet UITextField *txtDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtAgeY;
@property (weak, nonatomic) IBOutlet UITextField *txtAgeM;
@property (weak, nonatomic) IBOutlet UITextField *txtPurchasePrice;
@property (weak, nonatomic) IBOutlet UITextField *txtPurchaseLocation;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cmdSave;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cmdCancel;
@property (weak, nonatomic) IBOutlet UITextField *txtCondition;
- (IBAction)cmdPickARoom:(UIButton *)sender;
- (IBAction)cmdPickACondition:(UIButton *)sender;

@end

@implementation InventoryInfoController

- (void)viewDidLoad {
    @try {
        [super viewDidLoad];
        
//        CTTabBarController *source = (CTTabBarController *)self.parentViewController.parentViewController;
//        self.claim = source.selectedClaim;
//        self.editMode = source.editMode;
        
//        self.inventory = source.selectedInventory;
        
        self.txtQuantity.delegate = self;
        self.txtRoom.delegate = self;
        self.txtDescription.delegate = self;
        self.txtAgeY.delegate = self;
        self.txtAgeM.delegate = self;
        self.txtPurchasePrice.delegate = self;
        self.txtPurchaseLocation.delegate = self;
        
        self.txtQuantity.nextTextField = self.txtRoom;
        self.txtRoom.nextTextField = self.txtDescription;
        self.txtDescription.nextTextField = self.txtAgeY;
        self.txtAgeY.nextTextField = self.txtAgeM;
        self.txtAgeM.nextTextField = self.txtCondition;
        self.txtCondition.nextTextField = self.txtPurchasePrice;
        self.txtPurchasePrice.nextTextField = self.txtPurchaseLocation;
        self.txtPurchaseLocation.nextTextField = nil;
        
        self.lblClaim.text = [NSString stringWithFormat:@"%@", self.claim.claimName];
        if (self.inventory.ordinal) self.lblItem.text = [NSString stringWithFormat:@"Item #%@", self.inventory.ordinal.stringValue];
        if (self.inventory.quantity) self.txtQuantity.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.inventory.quantity];
        if (self.inventory.room) self.txtRoom.text = self.inventory.room;
        if (self.inventory.originalDescription) self.txtDescription.text = self.inventory.originalDescription;
        if (self.inventory.ageY) self.txtAgeY.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.inventory.ageY];
        if (self.inventory.ageM) self.txtAgeM.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.inventory.ageM];
        if (self.inventory.conditionID) self.txtCondition.text = [[UserData data].conditions getConditionByID:self.inventory.conditionID].conditionName;
        if (self.inventory.purchasePrice) { self.txtPurchasePrice.text = self.inventory.purchasePrice.stringValue; }
        if (self.inventory.purchaseLocation) { self.txtPurchaseLocation.text = self.inventory.purchaseLocation; }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // So Pictures can get the correct name
    self.inventory.originalDescription = self.txtDescription.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField
{
    UITextField *next = theTextField.nextTextField;
    if (next)
    {
        [next becomeFirstResponder];
    }
    else
    {
        [theTextField resignFirstResponder];
    }
    return NO;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if (sender == self.cmdSave)
        return [self validateData];
    else
        return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    @try {
        if (sender == self.cmdSave)
        {
            NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:self.txtPurchasePrice.text];
            
            self.inventory.quantity = [self.txtQuantity.text integerValue];
            self.inventory.room = self.txtRoom.text;
            self.inventory.originalDescription = self.txtDescription.text;
            self.inventory.ageY = [self.txtAgeY.text integerValue];
            self.inventory.ageM = [self.txtAgeM.text integerValue];
            self.inventory.conditionID = [[UserData data].conditions getConditionByName:self.txtCondition.text].conditionID;
            self.inventory.purchasePrice = [price isEqualToNumber:[NSDecimalNumber notANumber]] ? [NSDecimalNumber decimalNumberWithString:@"0"] : price;
            self.inventory.purchaseLocation = self.txtPurchaseLocation.text;
            
            if (self.inventory.edited == FALSE){
                self.inventory.edited = TRUE;
            }
            [[UserData data] saveClaims];
        }
        else if ((sender == self.cmdCancel) && (!self.editMode)) // Canceling an Add
        {
            [self.claim.inventoryList removeInventory:self.inventory];
            self.inventory = nil;
        }
        //else if ([segue.identifier isEqualToString:@"segueInventoryInfoToRoomList"])
        //{
        //    CTNavigationController *destination = segue.destinationViewController;
        //    destination.selectedClaim = self.claim;
        //    destination.selectedText = self.txtRoom.text;
        //}
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

//- (IBAction)unwindToInventoryInfo:(UIStoryboardSegue *)segue
//{
   // PickARoomViewController *source = (PickARoomViewController *)segue.sourceViewController;
   // if (source.selectedRoom.length > 0)
   // {
    //    self.txtRoom.text = source.selectedRoom;
    //}
//}

- (IBAction)cmdPickARoom:(UIButton *)sender {
    @try {
        if ([self.claim getRoomList].count == 0){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"There are no rooms defined.  Please type the room into the text box" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [alert show];
        }
        else {
            PickARoomViewController *pickARoomViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"PickARoomViewController"];

            pickARoomViewController.claim = self.claim;
            pickARoomViewController.textField = self.txtRoom;
            [self presentViewController:pickARoomViewController animated:YES completion:NULL];
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (IBAction)cmdPickACondition:(UIButton *)sender {
    @try {
        if ([UserData data].conditions.count == 0){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"There are no conditions loaded." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [alert show];
        }
        else {
            PickAConditionViewController *pickAConditionViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"PickAConditionViewController"];
            
            pickAConditionViewController.textField = self.txtCondition;
            [self presentViewController:pickAConditionViewController animated:YES completion:NULL];
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (BOOL)validateData{
    @try {
        if (self.txtDescription.text.length > 0)
        {
            if (self.txtQuantity.text.length == 0){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter a quantity" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                [alert show];
                return NO;
            }
            
            if ([self.txtQuantity.text integerValue] <= 0){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter a quantity greater than zero" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                [alert show];
                return NO;
            }
            
            if ((self.txtAgeM.text.length > 0) && ([self.txtAgeM.text integerValue] > 11)){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Age (M) cannot be more than 11." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                [alert show];
                return NO;
            }
            
            if (self.txtPurchasePrice.text.length > 0){
                self.txtPurchasePrice.text = [NSString stringWithFormat:@"%.02f", [self.txtPurchasePrice.text floatValue]];
            }
        }
        return YES;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (IBAction)btnCancelAction:(id)sender {
    if(!self.editMode) // Canceling an Add
    {
        [self.claim.inventoryList removeInventory:self.inventory];
        self.inventory = nil;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSaveAction:(id)sender {
    if ([self validateData]) {
        NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:self.txtPurchasePrice.text];
        
        self.inventory.quantity = [self.txtQuantity.text integerValue];
        self.inventory.room = self.txtRoom.text;
        self.inventory.originalDescription = self.txtDescription.text;
        self.inventory.ageY = [self.txtAgeY.text integerValue];
        self.inventory.ageM = [self.txtAgeM.text integerValue];
        self.inventory.conditionID = [[UserData data].conditions getConditionByName:self.txtCondition.text].conditionID;
        self.inventory.purchasePrice = [price isEqualToNumber:[NSDecimalNumber notANumber]] ? [NSDecimalNumber decimalNumberWithString:@"0"] : price;
        self.inventory.purchaseLocation = self.txtPurchaseLocation.text;
        
        if (self.inventory.edited == FALSE){
            self.inventory.edited = TRUE;
        }
        [[UserData data] saveClaims];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
@end
