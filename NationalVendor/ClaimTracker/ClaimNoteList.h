//
//  ClaimNoteList.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/7/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClaimNote.h"

@interface ClaimNoteList : NSObject

- (NSUInteger)count;
- (ClaimNote *)getClaimNoteByID:(NSUInteger) claimNoteID;
- (ClaimNote *)getClaimNoteByIndex:(NSUInteger) index;
- (void)addClaimNote:(ClaimNote *) claimNote;
- (void)addNewClaimNote:(ClaimNote *) claimNote;
- (void) clear;

- (void) encodeWithCoder:(NSCoder *)encoder;
- (id) initWithCoder:(NSCoder *)decoder;
- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id *)buffer count:(NSUInteger)len;
- (NSInteger) needToSyncClaimNotesCount;
@end
