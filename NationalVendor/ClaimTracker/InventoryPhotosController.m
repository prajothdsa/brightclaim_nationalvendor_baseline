//
//  InventoryPhotosController.m
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/22/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <Photos/Photos.h>
#import "PHAsset+Extended.h"
#import "InventoryPhotosController.h"
#import "CTTabBarController.h"
#import "PhotoViewController.h"
#import "CTCollectionViewCell.h"

#import "CTCollectionView.h"

@interface InventoryPhotosController ()

@property CTCollectionViewCell *selectedCell;
@property bool editMode;

@end

@implementation InventoryPhotosController

- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        
        CTTabBarController *source = (CTTabBarController *)self.parentViewController.parentViewController;
        self.claim = source.selectedClaim;
        self.inventory = source.selectedInventory;
        self.editMode = source.editMode;
        CTCollectionView *collectionView = (CTCollectionView *)self.collectionView;
        collectionView.inventory = self.inventory;
        collectionView.claim = self.claim;
        collectionView.presenterViewController = self.tabBarController;
        collectionView.delegate = collectionView;
        collectionView.dataSource = collectionView;
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    @try {
        self.selectedCell = nil;

        if ([segue.identifier isEqualToString:@"seguePhotoCollectionToEditPhoto"])
        {
            self.selectedCell = (CTCollectionViewCell *)sender;
            UINavigationController *destination = segue.destinationViewController;
            PhotoViewController * photoViewController = destination.viewControllers[0];
            photoViewController.editMode = true;
            photoViewController.initialTitle = self.selectedCell.imageTitle.text;
            photoViewController.initialImage = self.selectedCell.imageView.image;
        }
        else if ([segue.identifier isEqualToString:@"seguePhotoCollectionToAddPhoto"])
        {
            UINavigationController *destination = segue.destinationViewController;
            PhotoViewController *photoViewController = destination.viewControllers[0];
            photoViewController.initialTitle = self.inventory.originalDescription;
            photoViewController.editMode = false;
        }
    } @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:exception.reason delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    } @finally {
    }
}

@end
