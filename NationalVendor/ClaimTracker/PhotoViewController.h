//
//  PhotoViewController.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/22/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property NSArray *images;
@property NSString *imageTitle;
@property bool editMode;
@property bool toDelete;
@property bool toSave;

@property NSString *initialTitle;
@property UIImage *initialImage;

@end
