//
//  UIButton+Extended.m
//  ClaimTracker
//
//  Created by Prajoth Antonio D on 04/09/17.
//  Copyright © 2017 com.nationalvendor. All rights reserved.
//

#import "UIButton+Extended.h"

@implementation UIButton (Extended)

- (UIImage *)imageForButtonWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
