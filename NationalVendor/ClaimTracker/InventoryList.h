//
//  InventoryList.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/18/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Inventory.h"
#import "PhotoList.h"

@interface InventoryList : NSObject

- (NSUInteger)count;
- (Inventory *)getInventoryByID:(NSUInteger) inventoryID;
- (Inventory *)getInventoryByIndex:(NSUInteger) index;
- (void)addInventory:(Inventory *) inventory;
- (void)clear;
- (void)removeInventory:(Inventory *)inventory;

- (void) encodeWithCoder:(NSCoder *)encoder;
- (id) initWithCoder:(NSCoder *)decoder;

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id *)buffer count:(NSUInteger)len;

@property PhotoList *photoList;

- (NSInteger) needToSyncCount;

@end
