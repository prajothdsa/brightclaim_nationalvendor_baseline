//
//  CollectionViewCell.h
//  ClaimTracker
//
//  Created by Andy Kratzert on 5/26/15.
//  Copyright (c) 2015 com.nationalvendor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Photo.h"
#import "Inventory.h"

@interface CTCollectionViewCell : UICollectionViewCell<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextField *imageTitle;
- (IBAction)cmdDelete:(UIButton *)sender;
@property UICollectionView *collectionView;
@property UIViewController *presenterViewController;
@property Photo *photo;
@property Inventory *inventory;

@end
